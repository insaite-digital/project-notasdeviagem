<?php 
    include DIR_INCLUDE . 'header.php'; 
?>

<div class="top_part">
    <!-- Start Header -->
    <header id="header" class="header_3 header">
        <div class="container">
            <div class="navigation">
                <?php include DIR_INCLUDE . 'menu.php'; ?>
            </div>
        </div>
    </header>
    
    
</div>
<div class="page-header">
    <div class="page-header-content">
        <div class="container">
            <h2 class="heading">Política de Privacidade</h2>
        </div>
    </div>
</div>

<div class="main-wrapper">
    <div class="container">                 
        <div class="">
            <div class="row">
                <div class="col-lg-12">
                    <div class="author_info">
                        <h3>Política de Privacidade do Website</h3>
                        <p>
                            Todas as suas informações pessoais recolhidas, serão usadas para o ajudar a tornar a sua visita no nosso site o mais produtiva e agradável possível. A garantia da confidencialidade dos dados pessoais dos utilizadores do nosso site é importante para o Notas de Viagem.
                            <br> 
                            Todas as informações pessoais relativas a membros, assinantes, clientes ou visitantes que usem o Notas de Viagem serão tratadas em concordância com a Lei da Proteção de Dados Pessoais de 26 de outubro de 1998 (Lei n.º 67/98).
                            <br>
                            A informação pessoal recolhida pode incluir o seu nome, e-mail, número de telefone e/ou smartphone, moradia, data de nascimento e/ou outros.
                            <br>
                            O uso do Notas de Viagem pressupõe a aceitação deste Acordo de privacidade. O Notas de Viagem reserva-se ao direito de alterar este acordo sem aviso prévio. Deste modo, recomendamos que consulte a nossa política de privacidade com regularidade de forma a estar sempre atualizado.
                        </p>
                        <h3>Cookies & Ligações</h3>
                        <p>
                            Utilizamos cookies para armazenar informação, tais como as suas preferências pessoas quando visita o nosso website. Em adição também utilizamos publicidade de terceiros no nosso website para suportar os custos de manutenção. Alguns destes publicitários, poderão utilizar tecnologias como os cookies e/ou web beacons quando publicitam no nosso website, o que fará com que esses publicitários (como o Google através do Google AdSense) também recebam a sua informação pessoal, como o endereço IP, o seu ISP, o seu browser, etc. 
                            <br>
                            Esta função é geralmente utilizada para geotargeting (mostrar publicidade de Chapecó/RS apenas aos leitores oriundos de Chapecó/RS por ex.) ou apresentar publicidade direcionada a um tipo de utilizador (como mostrar publicidade de restaurante a um utilizador que visita sites de culinária regularmente, por ex.).
                            <br>
                            Você detém o poder de desligar os seus cookies, nas opções do seu browser, ou efetuando alterações nas ferramentas de programas Anti-Virus. No entanto, isso poderá alterar a forma como interage com o nosso website, ou outros websites. Isso poderá afetar ou não permitir que faça logins em programas, sites ou fóruns da nossa e de outras redes.
                            <br>
                            O Notas de Viagem possui ligações para outros sites, os quais, a nosso ver, podem conter informações / ferramentas úteis para os nossos visitantes. A nossa política de privacidade não é aplicada a sites de terceiros, pelo que, caso visite outro site a partir do nosso deverá ler a política de privacidade do mesmo.
                            <br>
                            Não nos responsabilizamos pela política de privacidade ou conteúdo presente nesses mesmos sites.
                        </p>
                        <h3>Os Anúncios</h3>
                        <p>
                            Tal como outros websites, coletamos e utilizamos informação contida nos anúncios. A informação contida nos anúncios, inclui o seu endereço IP (Internet Protocol), o seu ISP (Internet Service Provider, como o Sapo, Clix, ou outro), o browser que utilizou ao visitar o nosso website (como o Internet Explorer ou o Firefox), o tempo da sua visita e que páginas visitou dentro do nosso website.
                        </p>
                        <p>
                            Presentemente estamos a usar Google Analytics para monitorizar a audiência no nosso website e para adequar o nosso conteúdo. Nenhuma informação pessoal é recolhida pelo Google Analytics.
                            Para mais informação sobre a política de privacidade do Google Analytics, por favor visite o seguinte link:
                            <a href="https://marketingplatform.google.com/about/analytics/terms/us/" target="blank">Acessar Termos do Google Analytics</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



            
<?php include DIR_INCLUDE . 'footer.php'; ?>
