<?php 
    include DIR_INCLUDE . 'header.php'; 
?>

<div class="top_part">
    <!-- Start Header -->
    <header id="header" class="header_3 header">
        <div class="container">
            <div class="navigation">
                <?php include DIR_INCLUDE . 'menu.php'; ?>
            </div>
        </div>
    </header>
    
    
</div>
<div class="page-header">
    <div class="page-header-content">
        <div class="container">
            <h2 class="heading">Autora</h2>
        </div>
    </div>
</div>

<div class="main-wrapper m_tp_0">
    <div class="container">                 
        <div class="author_wrapper">
            <div class="row">
                <div class="col-lg-6">
                    <div class="author_slider">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="slide_img" data-swiper="overlay-bottom">
                                        <img src="<?php echo INSAITE . 'images/author/author.jpg'; ?>" alt="img">
                                    </div>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="author_info">
                        <h3>Olá, eu sou a Jessica Edel</h3>
                        <p>
                            Sou autora do Notas de Viagem e colunista no Canal Ideal. Além de jornalista em formação, sou uma apaixonada por viagens, natureza, conhecimento e pela arte de escrever. 
                            <br> 
                            O Notas de Viagem teve início em 2019, quando eu decidi criar uma plataforma para compartilhar minhas experiências através de um blog focado na produção de conteúdo sobre lugares próximos (como foco na região Sul) e com valores acessíveis.
                        </p>
                        <h4>Um olhar sobre o que nos cerca</h4>
                        <p>
                            Nós temos riquezas ao nosso redor e nem sempre damos valor. Muitas pessoas associam turismo a lugares distantes e ‘badalados’. Essa não é a realidade, pois há muitas cidades menores que são ricas em cultura e história e estão investindo no setor turístico. São esses lugares que me interessam!  
                            <br> 
                            Desde que criei o blog eu busco me especializar para produzir conteúdo para a categoria de turismo, não somente na técnica de redação com jornalismo literário, mas também fotografia, edição de fotos, vídeos, etc. Me mantenho na busca constante por conhecimento. 
                            <br> 
                            Caso tenha alguma sugestão de destino, envio de press kits ou precise de alguém para escrever sobre o seu serviço turístico,  fique à vontade para me contatar.
                        </p>
                        <div class="author_bottom">
                            <!-- <div><img src="<?php echo INSAITE . 'images/author.jpg'; ?>" alt="img"></div> -->
                            <div class="social">
                                <ul>
                                    <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                                    <li><a href="#"><i class="ion-social-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="post_by_author">
            <div class="sec_title">
                <h1>Minhas Publicações</h1>
            </div>
            <div class="more_posts" data-aos="fade-up" data-aos-duration="900">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <article class="blog_post">
                                <div class="post_img">
                                    <a href="#"><img src="<?php echo TEMPLATE . 'images/blog/sm1.jpg'; ?>" alt="blog"></a>
                                    <div class="calendar">
                                        <a href="#"><span class="date">27</span><br>May</a>
                                    </div>
                                </div>
                                <div class="post_content">
                                    <div class="post_header">
                                        <div class="author"><a href="#"><i class="ion-android-create"></i> By Partricia Doe</a></div>
                                        <h2 class="post_title">
                                            <a href="#">With age, comes wisdom. With travel, comes understanding</a>
                                        </h2>
                                    </div>
                                </div>
                            </article>
                        </div>

                        <div class="swiper-slide">
                            <article class="blog_post">
                                <div class="post_img">
                                    <a href="#"><img src="<?php echo TEMPLATE . 'images/blog/sm2.jpg'; ?>" alt="blog"></a>
                                    <div class="calendar">
                                        <a href="#"><span class="date">27</span><br>May</a>
                                    </div>
                                </div>
                                <div class="post_content">
                                    <div class="post_header">
                                        <div class="author"><a href="#"><i class="ion-android-create"></i> By Partricia Doe</a></div>
                                        <h2 class="post_title">
                                            <a href="#">I am not the same having seen the moon in the sky</a>
                                        </h2>
                                    </div>
                                </div>
                            </article>                                      
                        </div>

                        <div class="swiper-slide">
                            <article class="blog_post">
                                <div class="post_img">
                                    <a href="#"><img src="<?php echo TEMPLATE . 'images/blog/sm3.jpg'; ?>" alt="blog"></a>
                                    <div class="calendar">
                                        <a href="#"><span class="date">24</span><br>May</a>
                                    </div>
                                </div>
                                <div class="post_content">
                                    <div class="post_header">
                                        <div class="author"><a href="#"><i class="ion-android-create"></i> By Partricia Doe</a></div>
                                        <h2 class="post_title">
                                            <a href="#">Travel and change of place impart new vigor to the mind</a>
                                        </h2>
                                    </div>
                                </div>
                            </article>                                          
                        </div>

                        <div class="swiper-slide">
                            <article class="blog_post">
                                <div class="post_img">
                                    <a href="#"><img src="<?php echo TEMPLATE . 'images/blog/sm4.jpg'; ?>" alt="blog"></a>
                                    <div class="calendar">
                                        <a href="#"><span class="date">22</span><br>May</a>
                                    </div>
                                </div>
                                <div class="post_content">
                                    <div class="post_header">
                                        <div class="author"><a href="#"><i class="ion-android-create"></i> By Partricia Doe</a></div>
                                        <h2 class="post_title">
                                            <a href="#">A journey is best measured in friends, rather than miles</a>
                                        </h2>
                                    </div>
                                </div>
                            </article>                                          
                        </div>
                    </div>
                    
                </div>
                <div class="swip_button">
                    <div class="swiper-button-prev"><i class="ion-ios-arrow-left"></i></div>
                    <div class="swiper-button-next"><i class="ion-ios-arrow-right"></i></div>
                </div>
            </div>
        </div> -->
    </div>
</div>



            
<?php include DIR_INCLUDE . 'footer.php'; ?>
