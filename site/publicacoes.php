<?php 
    include DIR_INCLUDE . 'header.php'; 
    $categoria = get_parameter_select("category", $web_parametros);
?>

<div class="top_part">
    <!-- Start Header -->
    <header id="header" class="header_3 header">
        <div class="container">
            <div class="navigation">
                <?php include DIR_INCLUDE . 'menu.php'; ?>
            </div>
        </div>
    </header>
    
    
</div>
<div class="page-header">
    <div class="page-header-content">
        <div class="container">
            <h2 class="heading">Publicações</h2>
        </div>
    </div>
</div>

<?php 
    if(!empty($categoria)){
        $database_categorys = newsql("SELECT * FROM tbl_publicacoes_categorias WHERE CAT_VISIBILIDADE = 'true' AND CAT_CHECK = 'true' AND CAT_CODIGO = '{$categoria}' ORDER BY CAT_TITULO DESC");
    } else {
        $database_categorys = newsql("SELECT * FROM tbl_publicacoes_categorias WHERE CAT_VISIBILIDADE = 'true' AND CAT_CHECK = 'true' ORDER BY CAT_TITULO DESC");
    }
    foreach ($database_categorys as $key => $value) {
        $category_codigo = $value['CAT_CODIGO'];
        $category_titulo = $value['CAT_TITULO'];
?>
<div class="main-wrapper m_tp_0">
    <div class="container">                 
        <div class="post_by_category1">
            <div class="sec_title">
                <h1><?php echo $category_titulo; ?></h1>
            </div>
            <div class="more_posts" data-aos="fade-up" data-aos-duration="900">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php 
                            $database_public = newsql("SELECT * FROM tbl_publicacoes as publicacao INNER JOIN tbl_publicacoes_categorias as categoria ON categoria.CAT_CODIGO = publicacao.PUB_CATEGORIA WHERE publicacao.PUB_CATEGORIA = '{$category_codigo}' AND publicacao.PUB_CHECK = 'true' AND publicacao.PUB_VISIBILIDADE = 'true' ORDER BY publicacao.PUB_DATA DESC");
                            foreach ($database_public as $chave => $vl) {
                                $database_codigo      = $vl['PUB_CODIGO'];
                                $database_time        = $vl['PUB_CADASTRO_TIME'];
                                $database_user        = $vl['PUB_CADASTRO_USUARIO'];
                                $database_categoria   = $vl['PUB_CATEGORIA'];
                                $database_titulo      = $vl['PUB_TITULO'];
                                $database_subtitulo   = limit($vl['PUB_SUBTITULO'], 250);
                                $database_descricao   = $vl['PUB_DESCRICAO'];
                                $database_legenda     = $vl['PUB_LEGENDA'];
                                $database_slug        = $vl['PUB_SLUG'];
                                $database_views       = $vl['PUB_VIEWS'];
                                $database_data        = details_data($vl['PUB_DATA']);
                                $database_category_titulo    = $vl['CAT_TITULO'];

                                if(empty($database_legenda)) $database_legenda = 'Imagem de ' . $database_titulo;
                                
                                $database_get_imagens = get_uploads("publicacoes_unico",$database_codigo,"");
                                $database_imagem = capture_uploads($database_get_imagens);

                                $database_search_ancor_category = SITE . 'publicacoes/' . Remove_caracter($database_category_titulo) . '?category=' . $database_categoria;
                                $database_search_ancor = SITE . 'publicacao/' . $database_slug;

                                if(empty(trim($database_subtitulo))){ $database_subtitulo = limit($database_descricao, 150); }
                        ?>
                        <div class="swiper-slide">
                            <article class="blog_post">
                                <div class="post_img">
                                    <a href="<?php echo $database_search_ancor; ?>"><img src="<?php echo $database_imagem; ?>" alt="<?php echo $database_legenda; ?>"></a>
                                    <div class="calendar">
                                        <a href="<?php echo $database_search_ancor; ?>"><span class="date"><?php echo $database_data[0]; ?></span><br><?php echo $database_data[1]; ?></a>
                                    </div>
                                </div>
                                <div class="post_content">
                                    <div class="post_header">
                                        <h2 class="post_title">
                                            <a href="<?php echo $database_search_ancor; ?>"><?php echo $database_titulo; ?></a>
                                        </h2>
                                    </div>
                                    <div class="post_intro">
                                        <p><?php echo strip_tags(htmlspecialchars_decode($database_subtitulo),"<span><strong><b><i>"); ?></p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <!-- Add Arrows -->
                <div class="swip_button">
                    <div class="swiper-button-prev-category1"><i class="ion-ios-arrow-left"></i></div>
                    <div class="swiper-button-next-category1"><i class="ion-ios-arrow-right"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
            
<?php include DIR_INCLUDE . 'footer.php'; ?>
