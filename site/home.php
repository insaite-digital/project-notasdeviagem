<?php 
    include DIR_INCLUDE . 'header.php'; 
?>

<div class="top_part">
    <!-- Start Header -->
    <header id="header" class="header_1 header">
        <div class="navigation">
            <?php include DIR_INCLUDE . 'menu.php'; ?>
        </div>
    </header>

    <?php include DIR_INCLUDE . 'social.php'; ?>

    <div class="theme_slider_1">
        <div class="slider_inner_content">
            <div class="slider_text">
                <div class="swiper-container slider_posts">
                    <div class="swiper-wrapper">
                        <?php 
                            $home_publicacoes_banner = newsql("SELECT * FROM tbl_publicacoes as publicacao INNER JOIN tbl_publicacoes_categorias as categoria ON categoria.CAT_CODIGO = publicacao.PUB_CATEGORIA WHERE publicacao.PUB_VISIBILIDADE = 'true' AND publicacao.PUB_CHECK = 'true' ORDER BY publicacao.PUB_VIEWS DESC LIMIT 0,4");
                            foreach ($home_publicacoes_banner as $key => $value) {
                                $public_codigo      = $value['PUB_CODIGO'];
                                $public_time        = $value['PUB_CADASTRO_TIME'];
                                $public_user        = $value['PUB_CADASTRO_USUARIO'];
                                $public_categoria   = $value['PUB_CATEGORIA'];
                                $public_titulo      = $value['PUB_TITULO'];
                                $public_subtitulo   = limit($value['PUB_SUBTITULO'], 250);
                                $public_descricao   = $value['PUB_DESCRICAO'];
                                $public_legenda     = $value['PUB_LEGENDA'];
                                $public_slug        = $value['PUB_SLUG'];
                                $public_views       = $value['PUB_VIEWS'];
                                $public_data        = details_data($value['PUB_DATA']);
                                $category_titulo    = $value['CAT_TITULO'];

                                if(empty($public_legenda)) $public_legenda = 'Imagem de ' . $public_titulo;
                                
                                $get_imagens = get_uploads("publicacoes_unico",$public_codigo,"");
                                $imagem = capture_uploads($get_imagens);

                                $search_ancor_category = SITE . 'publicacoes/' . Remove_caracter($category_titulo) . '?category=' . $public_categoria;
                                $search_ancor = SITE . 'publicacao/' . $public_slug;

                                if(empty(trim($public_subtitulo))){ $public_subtitulo = limit($public_descricao, 250); }
                        ?>
                        <div class="swiper-slide">
                            <article class="blog_post" style="background: url(<?php echo INSAITE . 'images/teste/texture.jpg'; ?>); background-position: center; background-size: cover; background-repeat: no-repeat;">
                                <div class="post_img">
                                    <div class="calendar">
                                        <a href="<?php echo $search_ancor; ?>"><span class="date"><?php echo $public_data[0]; ?></span><br><?php echo $public_data[1]; ?></a>
                                    </div>
                                </div>
                                <div class="post_content">
                                    <div class="post_header">
                                        <h2 class="post_title" data-swiper="overlay-left">
                                            <a href="<?php echo $search_ancor; ?>"><?php echo $public_titulo; ?></a>
                                        </h2>
                                    </div>
                                    <div class="post_intro">
                                        <p><?php echo strip_tags(htmlspecialchars_decode($public_subtitulo),"<span><strong><b><i>"); ?></p>
                                    </div>
                                    <div class="post_footer">
                                        <div class="read_more">
                                            <a href="<?php echo $search_ancor; ?>">Continue Lendo</a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <?php } ?>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        
            <div class="slider_images">
                <div class="swiper-container slider_posts">
                    <div class="swiper-wrapper">
                        <?php 
                            $home_publicacoes_banner = newsql("SELECT * FROM tbl_publicacoes as publicacao INNER JOIN tbl_publicacoes_categorias as categoria ON categoria.CAT_CODIGO = publicacao.PUB_CATEGORIA WHERE publicacao.PUB_VISIBILIDADE = 'true' AND publicacao.PUB_CHECK = 'true' ORDER BY publicacao.PUB_VIEWS DESC LIMIT 0,4");
                            foreach ($home_publicacoes_banner as $key => $value) {
                                $public_codigo      = $value['PUB_CODIGO'];
                                $public_time        = $value['PUB_CADASTRO_TIME'];
                                $public_user        = $value['PUB_CADASTRO_USUARIO'];
                                $public_categoria   = $value['PUB_CATEGORIA'];
                                $public_titulo      = $value['PUB_TITULO'];
                                $public_subtitulo   = limit($value['PUB_SUBTITULO'], 250);
                                $public_descricao   = $value['PUB_DESCRICAO'];
                                $public_legenda     = $value['PUB_LEGENDA'];
                                $public_slug        = $value['PUB_SLUG'];
                                $public_views       = $value['PUB_VIEWS'];
                                $public_data        = details_data($value['PUB_DATA']);
                                $category_titulo    = $value['CAT_TITULO'];

                                if(empty($public_legenda)) $public_legenda = 'Imagem de ' . $public_titulo;
                                
                                $get_imagens = get_uploads("publicacoes_unico",$public_codigo,"");
                                $imagem = capture_uploads($get_imagens);

                                $search_ancor_category = SITE . 'publicacoes/' . Remove_caracter($category_titulo) . '?category=' . $public_categoria;
                                $search_ancor = SITE . 'publicacao/' . $public_slug;

                                if(empty(trim($public_subtitulo))){ $public_subtitulo = limit($public_descricao, 250); }
                        ?>
                        <div class="swiper-slide">
                            <div class="slide_img" data-swiper="overlay-left">
                                <img src="<?php echo $imagem; ?>" alt="<?php echo $public_legenda; ?>">
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="featured_category bg_image_1 d-block d-sm-none d-md-block d-lg-block d-xl-block d-xxl-block" style="background: url(<?php echo INSAITE . 'images/teste/bg1.png'; ?>); margin-top: 30px;" data-aos="fade-up" data-aos-duration="700">
    <div class="container">
        <div class="row">
            <?php  
                $home_categorys = newsql("SELECT * FROM tbl_publicacoes_categorias WHERE CAT_VISIBILIDADE = 'true' AND CAT_CHECK = 'true' ORDER BY CAT_TITULO ASC LIMIT 3");
                foreach ($home_categorys as $key => $value) {
                    $category_codigo    = $value['CAT_CODIGO'];
                    $category_titulo    = $value['CAT_TITULO'];
                    $category_descricao = $value['CAT_DESCRICAO_TEXT'];
                    $category_legenda   = $value['CAT_LEGENDA'];
                    if(empty($category_legenda)) $category_legenda = 'Imagem de ' . $category_titulo;
                    $category_count = newsql("SELECT COUNT(*) as total FROM tbl_publicacoes WHERE PUB_CATEGORIA = '{$category_codigo}' AND PUB_VISIBILIDADE = 'true' AND PUB_CHECK = 'true'")[0]['total'];

                    $get_imagens = get_uploads("categoria_unico",$category_codigo,"");
                    $imagem = capture_uploads($get_imagens);

                    $search_ancor = SITE . 'publicacoes/' . Remove_caracter($category_titulo) . '?category=' . $category_codigo;
            ?>
            <div class="col-lg-3 col-sm-6">
                <div class="featured_category_item" data-aos="fade-up" data-aos-duration="600">
                    <img src="<?php echo $imagem; ?>" alt="<?php echo $category_legenda; ?>">
                    <div class="featured_category_info">
                        <h6 class="featured_category_heading"><a href="<?php echo $search_ancor; ?>"><?php echo $category_titulo; ?></a></h6>
                        <p class="featured_category_number"><span><?php echo $category_count; ?></span> Publicações</p>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="col-lg-3 col-sm-6">
                Bloco de Anuncio do Google Adsense
            </div>
        </div>
    </div>
</div>



<div class="main-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-7">
                <div class="blog_list" data-aos="fade-up" data-aos-duration="700">
                    <?php 
                        $home_publicacoes = newsql("SELECT * FROM tbl_publicacoes as publicacao INNER JOIN tbl_publicacoes_categorias as categoria ON categoria.CAT_CODIGO = publicacao.PUB_CATEGORIA WHERE publicacao.PUB_VISIBILIDADE = 'true' AND publicacao.PUB_CHECK = 'true' ORDER BY publicacao.PUB_DATA DESC LIMIT 0,2");
                        foreach ($home_publicacoes as $key => $value) {
                            $public_codigo      = $value['PUB_CODIGO'];
                            $public_time        = $value['PUB_CADASTRO_TIME'];
                            $public_user        = $value['PUB_CADASTRO_USUARIO'];
                            $public_categoria   = $value['PUB_CATEGORIA'];
                            $public_titulo      = $value['PUB_TITULO'];
                            $public_subtitulo   = limit($value['PUB_SUBTITULO'], 250);
                            $public_descricao   = $value['PUB_DESCRICAO'];
                            $public_legenda     = $value['PUB_LEGENDA'];
                            $public_slug        = $value['PUB_SLUG'];
                            $public_views       = $value['PUB_VIEWS'];
                            $public_data        = details_data($value['PUB_DATA']);
                            $category_titulo    = $value['CAT_TITULO'];

                            if(empty($public_legenda)) $public_legenda = 'Imagem de ' . $public_titulo;
                            
                            $get_imagens = get_uploads("publicacoes_unico",$public_codigo,"");
                            $imagem = capture_uploads($get_imagens);

                            $search_ancor_category = SITE . 'publicacoes/' . Remove_caracter($category_titulo) . '?category=' . $public_categoria;
                            $search_ancor = SITE . 'publicacao/' . $public_slug;

                            if(empty(trim($public_subtitulo))){ $public_subtitulo = limit($public_descricao, 250); }
                    ?>
                    <article class="blog_post">
                        <div class="post_img" data-aos="overlay-left">
                            <a href="<?php echo $search_ancor; ?>"><img src="<?php echo $imagem; ?>" alt="<?php echo $public_legenda; ?>"></a>
                            <div class="calendar">
                                <a href="<?php echo $search_ancor; ?>"><span class="date"><?php echo $public_data[0]; ?></span><br><?php echo $public_data[1]; ?></a>
                            </div>
                        </div>
                        <div class="post_content">
                            <div class="post_header">
                                <div class="author"><a href="<?php echo $search_ancor_category; ?>"><i class="ion-android-create"></i> <?php echo $category_titulo; ?></a></div>
                                <h2 class="post_title">
                                    <a href="<?php echo $search_ancor; ?>"><?php echo $public_titulo; ?></a>
                                </h2>
                            </div>
                            <div class="post_intro">
                                <p><?php echo strip_tags(htmlspecialchars_decode($public_subtitulo),"<span><strong><b><i>"); ?></p>
                            </div>
                            <div class="post_footer">
                                <div class="read_more" data-aos="fade-up" data-aos-duration="700">
                                    <a href="<?php echo $search_ancor; ?>">Continue Lendo</a>
                                </div>
                                <div class="post_share">
                                    <ul class="share_list">
                                        <li data-aos="fade-up" data-aos-duration="500"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $search_ancor; ?>" target="_blank"><i class="ion-social-facebook"></i></a></li>
                                        <li data-aos="fade-up" data-aos-duration="700"><a href="http://twitter.com/intent/tweet?text=<?php echo $public_titulo;?>&url=<?php echo $search_ancor;?>" target="_blank"><i class="ion-social-twitter"></i></a></li>
                                        <li data-aos="fade-up" data-aos-duration="900"><a href="https://api.whatsapp.com/send?text= Veja esta Publicação: <?php echo $public_titulo; ?> - Segue o Link: <?php echo $search_ancor; ?>" target="_blank"><i class="ion-social-whatsapp"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </article>
                    <?php } ?>
                </div>

                <div class="newsletter bg_image_2" style="background: url(<?php echo INSAITE . 'images/teste/bg2.png'; ?>);" data-aos="fade-up" data-aos-duration="700">
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">
                            <form id="FormNewsletter" class="newsletter_form" method="POST" onsubmit="return false">
                                <input type="hidden" name="email" value="">
                                <div class="newsletter_header">
                                    <p><i class="ion-email"></i>Assine nossa newsletter</p>
                                    <h3>Todo conteúdo novo postado aqui no blog, será enviado para você via e-mail totalmente de graça!</h3>
                                </div>
                                <div class="form_group">
                                    <input class="form-control" type="email" placeholder="Seu e-mail" name="newsletter_email1" required="">
                                    <input type="submit" class="button" value="Assinar">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
                <div class="more_posts">
                    <div class="row">
                        <?php 
                            $home_publicacoes_body = newsql("SELECT * FROM tbl_publicacoes as publicacao INNER JOIN tbl_publicacoes_categorias as categoria ON categoria.CAT_CODIGO = publicacao.PUB_CATEGORIA WHERE publicacao.PUB_VISIBILIDADE = 'true' AND publicacao.PUB_CHECK = 'true' ORDER BY publicacao.PUB_DATA DESC LIMIT 2,6");
                            foreach ($home_publicacoes_body as $key => $value) {
                                $public_codigo      = $value['PUB_CODIGO'];
                                $public_time        = $value['PUB_CADASTRO_TIME'];
                                $public_user        = $value['PUB_CADASTRO_USUARIO'];
                                $public_categoria   = $value['PUB_CATEGORIA'];
                                $public_titulo      = $value['PUB_TITULO'];
                                $public_subtitulo   = limit($value['PUB_SUBTITULO'], 250);
                                $public_descricao   = $value['PUB_DESCRICAO'];
                                $public_legenda     = $value['PUB_LEGENDA'];
                                $public_slug        = $value['PUB_SLUG'];
                                $public_views       = $value['PUB_VIEWS'];
                                $public_data        = details_data($value['PUB_DATA']);
                                $category_titulo    = $value['CAT_TITULO'];

                                if(empty($public_legenda)) $public_legenda = 'Imagem de ' . $public_titulo;
                                
                                $get_imagens = get_uploads("publicacoes_unico",$public_codigo,"");
                                $imagem = capture_uploads($get_imagens);

                                $search_ancor_category = SITE . 'publicacoes/' . Remove_caracter($category_titulo) . '?category=' . $public_categoria;
                                $search_ancor = SITE . 'publicacao/' . $public_slug;

                                if(empty(trim($public_subtitulo))){ $public_subtitulo = limit($public_descricao, 250); }

                        ?>
                        <div class="col-lg-6 col-md-6">
                            <article class="blog_post" data-aos="fade-up" data-aos-duration="700">
                                <div class="post_img" data-aos="overlay-left">
                                    <a href="<?php echo $search_ancor; ?>"><img src="<?php echo $imagem; ?>" alt="<?php echo $public_legenda; ?>"></a>
                                    <div class="calendar">
                                        <a href="<?php echo $search_ancor; ?>"><span class="date"><?php echo $public_data[0]; ?></span><br><?php echo $public_data[1]; ?></a>
                                    </div>
                                </div>
                                <div class="post_content">
                                    <div class="post_header">
                                        <div class="author"><a href="<?php echo $search_ancor; ?>"><i class="ion-android-create"></i> <?php echo $category_titulo; ?></a></div>
                                        <h2 class="post_title">
                                            <a href="<?php echo $search_ancor; ?>"><?php echo $public_titulo; ?></a>
                                        </h2>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="pagination-div">
                    <ul class="pagination">
                        <li><a href="<?php echo SITE . 'publicacoes'; ?>">Ver Todas Publicações</a></li>
                    </ul>
                </div>
            </div>
            
            <div class="col-lg-4 col-md-5">
                <div class="sidebar" data-aos="fade-up" data-aos-duration="900">
                    <div id="custom_1" class="widget widget_custom">
                        <h4 class="widget_title">Autoria</h4>
                        <div class="sidebar_author">
                            <img src="<?php echo INSAITE . 'images/teste/autor.jpg'; ?>" alt="img">                                 
                            <p class="intro">A paixão pela escrita e por viagens me fez criar o blog. Unindo o útil ao agradável, aqui eu exponho minhas experiências de Norte a Sul através do jornalismo literário.<br><br>Sou uma estudante de jornalismo apaixonada por histórias, viagens e pela vida!</p>

                            <div class="social_profile">
                                <div class="social_media">
                                    <a href="https://www.facebook.com/BlogNotasdeViagem" target="_blank">
                                        <i class="ion-social-facebook"></i>
                                        <div class="name">Facebook</div>
                                        <!-- <div class="off_text">45k Likes</div> -->
                                    </a>
                                </div>
                                <div class="social_media">
                                    <a href="https://www.instagram.com/notas_de_viagem/" target="_blank">
                                        <i class="ion-social-instagram"></i>
                                        <div class="name">Instagram</div>
                                        <!-- <div class="off_text">23k Follower</div> -->
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="custom_4" class="widget widget_custom">
                        <div class="sidebar_add">
                            <img src="<?php echo INSAITE . 'images/teste/anuncio.png'; ?>" alt="add">
                        </div>
                    </div>
                    <div id="recent-posts-1" class="widget widget_recent_posts">
                        <h4 class="widget_title">Mais Lidas</h4>
                        <div class="sidebar_recent_posts">
                            <ul>
                                <?php 
                                    $home_publicacoes_views = newsql("SELECT * FROM tbl_publicacoes as publicacao INNER JOIN tbl_publicacoes_categorias as categoria ON categoria.CAT_CODIGO = publicacao.PUB_CATEGORIA WHERE publicacao.PUB_VISIBILIDADE = 'true' AND publicacao.PUB_CHECK = 'true' ORDER BY publicacao.PUB_VIEWS DESC LIMIT 0,4");
                                    foreach ($home_publicacoes_views as $key => $value) {
                                        $public_codigo      = $value['PUB_CODIGO'];
                                        $public_titulo      = $value['PUB_TITULO'];
                                        $public_slug        = $value['PUB_SLUG'];
                                        $search_ancor       = SITE . 'publicacao/' . $public_slug;
                                ?>
                                <li><a href="<?php echo $search_ancor; ?>"><?php echo $public_titulo; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>

                    <div id="custom_2" class="widget widget_custom">
                        <div class="sidebar_add">
                            <img src="<?php echo INSAITE . 'images/teste/anuncio.png'; ?>" alt="add">
                        </div>
                    </div>

                    <div id="categories-1" class="widget widget_categories">
                        <h4 class="widget_title">Categorias</h4>
                        <div class="sidebar_categories">
                            <ul class="category_list">
                                <?php  
                                    $home_categorys_sidebar = newsql("SELECT * FROM tbl_publicacoes_categorias WHERE CAT_VISIBILIDADE = 'true' AND CAT_CHECK = 'true' ORDER BY CAT_TITULO ASC LIMIT 3");
                                    foreach ($home_categorys_sidebar as $key => $value) {
                                        $category_codigo    = $value['CAT_CODIGO'];
                                        $category_titulo    = $value['CAT_TITULO'];
                                        $category_count = newsql("SELECT COUNT(*) as total FROM tbl_publicacoes WHERE PUB_CATEGORIA = '{$category_codigo}' AND PUB_VISIBILIDADE = 'true' AND PUB_CHECK = 'true'")[0]['total'];
                                        $search_ancor = SITE . 'publicacoes/' . Remove_caracter($category_titulo) . '?category=' . $category_codigo;
                                ?>
                                <li><a href="<?php echo $search_ancor; ?>"><?php echo $category_titulo; ?></a> (<?php echo $category_count; ?>)</li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    
                    <div id="popular-1" class="widget widget_popular_posts">
                        <h4 class="widget_title">Veja Também</h4>
                        <div class="sidebar_popular_posts">
                            <?php 
                                $home_publicacoes_sidebar = newsql("SELECT * FROM tbl_publicacoes as publicacao INNER JOIN tbl_publicacoes_categorias as categoria ON categoria.CAT_CODIGO = publicacao.PUB_CATEGORIA WHERE publicacao.PUB_VISIBILIDADE = 'true' AND publicacao.PUB_CHECK = 'true' ORDER BY publicacao.PUB_DATA DESC LIMIT 8,2");
                                foreach ($home_publicacoes_sidebar as $key => $value) {
                                    $public_codigo      = $value['PUB_CODIGO'];
                                    $public_time        = $value['PUB_CADASTRO_TIME'];
                                    $public_user        = $value['PUB_CADASTRO_USUARIO'];
                                    $public_categoria   = $value['PUB_CATEGORIA'];
                                    $public_titulo      = $value['PUB_TITULO'];
                                    $public_subtitulo   = limit($value['PUB_SUBTITULO'], 250);
                                    $public_descricao   = $value['PUB_DESCRICAO'];
                                    $public_legenda     = $value['PUB_LEGENDA'];
                                    $public_slug        = $value['PUB_SLUG'];
                                    $public_views       = $value['PUB_VIEWS'];
                                    $public_data        = details_data($value['PUB_DATA']);
                                    $category_titulo    = $value['CAT_TITULO'];

                                    if(empty($public_legenda)) $public_legenda = 'Imagem de ' . $public_titulo;
                                    
                                    $get_imagens = get_uploads("publicacoes_unico",$public_codigo,"");
                                    $imagem = capture_uploads($get_imagens);

                                    $search_ancor_category = SITE . 'publicacoes/' . Remove_caracter($category_titulo) . '?category=' . $public_categoria;
                                    $search_ancor = SITE . 'publicacao/' . $public_slug;

                                    if(empty(trim($public_subtitulo))){ $public_subtitulo = limit($public_descricao, 250); }

                            ?>
                            <div class="popular_post_item">
                                <div class="popular_post_item_wrapper">
                                    <img src="<?php echo $imagem; ?>" alt="<?php echo $public_legenda; ?>">
                                    <div class="popular_post_item_content">
                                        <h4 class="popular_post_item_title"><a href="<?php echo $search_ancor; ?>"><?php echo $public_titulo; ?></a></h4>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div id="custom_3" class="widget widget_custom">
                        <div class="sidebar_add">
                            <img src="<?php echo INSAITE . 'images/teste/anuncio.png'; ?>" alt="add">
                        </div>
                    </div>

                    <!-- <div id="tags-1" class="widget widget_tag_cloud">
                        <h4 class="widget_title">Tags</h4>
                        <div class="tagcloud">
                            <a href="#">Travel</a>
                            <a href="#">PHOTOGRAPHY</a>
                            <a href="#">FASHION</a>
                            <a href="#">Travel</a>
                            <a href="#">HOBBY</a>
                            <a href="#">PHOTOGRAPHY</a>
                            <a href="#">NATURE</a>
                        </div>
                    </div> -->
                    
                </div>
            </div>
        </div>
    </div>
</div>
            
<?php include DIR_INCLUDE . 'footer.php'; ?>

                   