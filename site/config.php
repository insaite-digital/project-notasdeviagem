<?php
header('Access-Control-Allow-Origin: *');
/***** SET CONFIGURAÇÕES *****/
session_cache_expire(15400);
/* Inciiar SESSION */
session_start();
/* Definição de Tempo */
date_default_timezone_set('America/Sao_Paulo');
/* Definição de Erros */
error_reporting(0);

/***** DEFAULTS CONSTANTS RAIZ *****/
/***** Caminho Físico *****/
define('DS', DIRECTORY_SEPARATOR); 										/* Separador = \ */
define('ROOT', dirname(__FILE__)); 										/* E:\xampp\htdocs\base\site */
define('ROOTSIS', dirname(dirname(__FILE__))); 							/* E:\xampp\htdocs\base */
define('DIR_ACTION', ROOT . DS . '_actions' . DS); 						/* E:\xampp\htdocs\base\site\_actions\ */
define('DIR_CLASSE', ROOT . DS . '_classes' . DS); 						/* E:\xampp\htdocs\base\site\_classes\ */
define('DIR_ERROR', ROOT . DS . '_erros' . DS); 						/* E:\xampp\htdocs\base\site\_erros\ */
define('DIR_FONT', ROOT . DS . '_fonts' . DS); 							/* E:\xampp\htdocs\base\site\_fonts\ */
define('DIR_INCLUDE', ROOT . DS . '_includes' . DS); 					/* E:\xampp\htdocs\base\site\_includes\ */
define('DIR_INSAITE', ROOT . DS . '_insaite' . DS); 					/* E:\xampp\htdocs\base\site\_insaite\ */
define('DIR_LIB', ROOT . DS . '_librarys' . DS); 						/* E:\xampp\htdocs\base\site\_librarys\ */
define('DIR_MAIL', ROOT . DS . '_mails' . DS); 							/* E:\xampp\htdocs\base\site\_mails\ */
define('DIR_PLUGIN', ROOT . DS . '_plugins' . DS); 						/* E:\xampp\htdocs\base\site\_plugins\ */
define('DIR_TEMPLATE', ROOT . DS . '_templates' . DS); 					/* E:\xampp\htdocs\base\site\_templates\ */
define('HOST', 'https://' . $_SERVER['HTTP_HOST'] . "/"); 				/* https://localhost/ */

/***** CHAMADA DE CLASSES E BIBLIOTECAS *****/
//-- Inicia conexão com o banco
require_once('_classes/Conexao.class.php');
date_default_timezone_set('America/Sao_Paulo');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();

//-- Inicia arquivo de funções
require DIR_LIB . 'functions.php';  
require DIR_LIB . 'phpmailer/class.phpmailer.php';

$sql_insaite = newsql("SELECT * FROM config_insaite WHERE INS_CODIGO = 0")[0];
/** Váriaveis Importantes **/
$login_titulo_projeto 	= $sql_insaite['INS_TITULO'];
$login_dominio_projeto 	= $sql_insaite['INS_DOMINIO'];
$login_website_projeto 	= SITE;
$projeto_online 		= $sql_insaite['INS_ONLINE'];
$projeto_repositorio 	= $sql_insaite['INS_REPOSITORIO'];

if($projeto_online == 'true'){ 
    /***** Caminho Lógico *****/
    define('SITEROOT', HOST . ''); 			
    define('SYSTEM', HOST . 'system/'); 	
    define('ADMIN', HOST . 'proadmin/');
    define('SITE', HOST . 'site/'); 								
    define('UPLOAD', HOST . 'uploads/'); 					
} else { 
    /***** Caminho Lógico *****/
    define('SITEROOT', HOST . '' . $projeto_repositorio . '/'); 			
    define('SYSTEM', HOST . '' . $projeto_repositorio . '/system/'); 	
    define('ADMIN', HOST . '' . $projeto_repositorio . '/proadmin/');
    define('SITE', HOST . '' . $projeto_repositorio . '/site/'); 	
    define('UPLOAD', HOST . '' . $projeto_repositorio . '/uploads/');
}

/***** CONSTANTES DO WEBISTE *****/
define('AJAX', SITE . '_ajax/'); 										/* https://localhost/base/site/_ajax/ */
define('CLASS', SITE . '_classes/'); 									/* https://localhost/base/site/_classes/ */
define('ERROR', SITE . '_erros/');  									/* https://localhost/base/site/_erros/ */
define('FONT', SITE . '_fonts/');  										/* https://localhost/base/site/_fonts/ */
define('INCLUDE', SITE . '_includes/'); 								/* https://localhost/base/site/_includes/ */
define('INSAITE', SITE . '_insaite/'); 									/* https://localhost/base/site/_insaite/ */
define('LIBRARY', SITE . '_librarys/'); 					 			/* https://localhost/base/site/_librarys/ */
define('MAIL', SITE . '_mails/'); 										/* https://localhost/base/site/_mails/ */
define('PLUGIN', SITE . '_plugins/'); 									/* https://localhost/base/site/_plugins/ */
define('TEMPLATE', SITE . '_template/');								/* https://localhost/base/site/_template/ */