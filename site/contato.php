<?php 
    include DIR_INCLUDE . 'header.php'; 
?>

<div class="top_part">
    <!-- Start Header -->
    <header id="header" class="header_3 header">
        <div class="container">
            <div class="navigation">
                <?php include DIR_INCLUDE . 'menu.php'; ?>
            </div>
        </div>
    </header>
    
    
</div>
<div class="page-header">
    <div class="page-header-content">
        <div class="container">
            <h2 class="heading">Contato</h2>
        </div>
    </div>
</div>

<div class="main-wrapper m_tp_0">
    <div class="container">                 
        <div class="contact_info">
            <div class="infobox">
                <div class="infobox_icon">
                    <img class="primary_img" src="<?php echo TEMPLATE . 'images/phonebook_white.png'; ?>" alt="alt">
                </div>
                <div class="infobox_content">
                    <p>Telefone</p>
                    <h6>(49) 99906-1733</h6>
                </div>
            </div>

            <div class="infobox">
                <div class="infobox_icon">
                    <img class="primary_img" src="<?php echo TEMPLATE . 'images/mailbox_white.png'; ?>" alt="alt">
                </div>
                <div class="infobox_content">
                    <p>E-mail</p>
                    <h6>jessicagedel@gmail.com</h6>
                </div>
            </div>

            <div class="infobox">
                <div class="infobox_icon">
                    <img class="primary_img" src="<?php echo TEMPLATE . 'images/map_white.png'; ?>" alt="alt">
                </div>
                <div class="infobox_content">
                    <p>Endereço</p>
                    <h6>Chapecó (SC)</h6>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="contact" data-aos="fade-up" data-aos-duration="700">
                    <div class="contact_title">
                        <h3>Entre em Contato</h3>
                        <div class="title_line"></div>
                    </div>
                    <form id="FormContato" method="POST" onsubmit="return false">
                        <input type="hidden" name="email" value="">
                        <p class="form_note">Para enviar sua mensagem fique atento aos dados obrigatórios marcados com *</p>
                        <div class="form-container">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <textarea name="contato_mensagem" class="form-control" placeholder="Mensagem*" required></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <input type="text" name="contato_nome" class="form-control" placeholder="Nome*" required>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <input type="email" name="contato_email" class="form-control" placeholder="E-mail*" required>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <input type="text" name="contato_telefone" class="form-control" placeholder="Telefone">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <input class="button" type="submit" value="Enviar" name="submit">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include DIR_INCLUDE . 'footer.php'; ?>

