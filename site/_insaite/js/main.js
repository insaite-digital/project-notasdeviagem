$(document).ready(function() {
    $("#FormContato").submit(function(e) {
        e.preventDefault(); 
        var form = $(this);
        $.ajax({
            type: "POST",
            url         : '_ajax/ajax.cadastrarContato.php',
            data: form.serialize(), 
            dataType    : 'json', 
            beforeSend: function() {
                Swal.fire({
                    title: 'Aguarde...',
                    html: 'Verificando...',
                    showConfirmButton: false,
                    showCancelButton: false,
                });
            },
            success: function (data) {
                var resposta = data.resposta;
                var last_id = data.last_id;
                
                if(resposta == 'true'){ 
                    Swal.fire({
                        type: 'success',
                        title: 'Mensagem Enviada com Sucesso',
                        text: 'Aguarde você será direcionado...',
                        footer: 'Feito!',
                        showConfirmButton: false,
                        timer: 2500
                    });
                    setTimeout(function(){ 
                        // location.href = 'painel';
                        location.reload();
                    }, 2500);

                } else if(resposta == 'email'){
                    Swal.fire({
                        type: 'warning',
                        title: 'E-mail Incorreto',
                        text: 'O e-mail digitado parece não estar correto',
                        footer: 'Aviso!',
                        showConfirmButton: false,
                        timer: 5000
                    });

                } else if(resposta == 'dados'){
                    Swal.fire({
                        type: 'warning',
                        title: 'Informações Insuficientes',
                        text: 'Preencha todoas as informações requeridas',
                        footer: 'Aviso!',
                        showConfirmButton: false,
                        timer: 5000
                    });

                } else if(resposta == 'false'){
                    Swal.fire({
                        type: 'danger',
                        title: 'Erro ao enviar mensagem',
                        text: 'Tente novamente mais tarde, ou entre em contato com a nossa equipe de suporte!',
                        footer: 'Aviso!',
                        showConfirmButton: false,
                        timer: 5000
                    });
                
                } else {
                    Swal.fire({
                        type: 'danger',
                        title: 'Erro ao realizar cadastro',
                        text: 'Tente novamente mais tarde, ou entre em contato com a nossa equipe de suporte!',
                        footer: 'Aviso!',
                        showConfirmButton: false,
                        timer: 5000
                    });
                }
            }
        });
    });

    $("#FormNewsletter").submit(function(e) {
        e.preventDefault(); 
        var form = $(this);
        $.ajax({
            type: "POST",
            url         : '_ajax/ajax.cadastrarNewsletter.php',
            data: form.serialize(), 
            dataType    : 'json', 
            beforeSend: function() {
                Swal.fire({
                    title: 'Aguarde...',
                    html: 'Verificando...',
                    showConfirmButton: false,
                    showCancelButton: false,
                });
            },
            success: function (data) {
                var resposta = data.resposta;
                var last_id = data.last_id;
                
                if(resposta == 'true'){ 
                    Swal.fire({
                        type: 'success',
                        title: 'Assinatura Realizada com Sucesso',
                        text: 'Aguarde você será direcionado...',
                        footer: 'Feito!',
                        showConfirmButton: false,
                        timer: 2500
                    });
                    setTimeout(function(){ 
                        // location.href = 'painel';
                        location.reload();
                    }, 2500);

                } else if(resposta == 'email'){
                    Swal.fire({
                        type: 'warning',
                        title: 'E-mail Incorreto',
                        text: 'O e-mail digitado parece não estar correto',
                        footer: 'Aviso!',
                        showConfirmButton: false,
                        timer: 5000
                    });

                } else if(resposta == 'dados'){
                    Swal.fire({
                        type: 'warning',
                        title: 'Informações Insuficientes',
                        text: 'Preencha todoas as informações requeridas',
                        footer: 'Aviso!',
                        showConfirmButton: false,
                        timer: 5000
                    });

                } else if(resposta == 'false'){
                    Swal.fire({
                        type: 'danger',
                        title: 'Erro ao assinar',
                        text: 'Tente novamente mais tarde, ou entre em contato com a nossa equipe de suporte!',
                        footer: 'Aviso!',
                        showConfirmButton: false,
                        timer: 5000
                    });
                
                } else {
                    Swal.fire({
                        type: 'danger',
                        title: 'Erro ao assinar',
                        text: 'Tente novamente mais tarde, ou entre em contato com a nossa equipe de suporte!',
                        footer: 'Aviso!',
                        showConfirmButton: false,
                        timer: 5000
                    });
                }
            }
        });
    });
});