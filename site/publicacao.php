<?php 
    include DIR_INCLUDE . 'header.php'; 
    $slug = getCod();
    $consulta = newsql("SELECT * FROM tbl_publicacoes as publicacao INNER JOIN tbl_publicacoes_categorias as categoria ON categoria.CAT_CODIGO = publicacao.PUB_CATEGORIA WHERE publicacao.PUB_SLUG = '{$slug}' AND publicacao.PUB_CHECK = 'true' AND publicacao.PUB_VISIBILIDADE = 'true'")[0];
    $public_codigo      = $consulta['PUB_CODIGO'];
    $public_categoria   = $consulta['PUB_CATEGORIA'];
    $public_titulo      = $consulta['PUB_TITULO'];
    $public_subtitulo   = $consulta['PUB_SUBTITULO'];
    $public_descricao   = strip_tags(htmlspecialchars_decode($consulta['PUB_DESCRICAO']),"<blockquote><h1><h2><h3><h4><h5><h6><iframe><img><a><article><p><ul><li><table><td><tr><span><strong><b><i><br>");
    $public_legenda     = $consulta['PUB_LEGENDA'];
    $public_data        = details_data($consulta['PUB_DATA']);
    $category_titulo    = $consulta['CAT_TITULO'];
    
    $get_imagens = get_uploads("publicacoes_unico",$public_codigo,"");
    $imagem = capture_uploads($get_imagens);
?>

<div class="top_part">
    <!-- Start Header -->
    <header id="header" class="header_3 header">
        <div class="container">
            <div class="navigation">
                <?php include DIR_INCLUDE . 'menu.php'; ?>
            </div>
        </div>
    </header>    
</div>
<div class="page-header">
    <div class="page-header-content">
        <div class="container">
            <h2 class="heading"><?php echo $public_titulo; ?></h2>
        </div>
    </div>
</div>

<div class="main-wrapper m_tp_0">
    <div class="container">
        <div class="blog_details">
            <div class="post_img" data-aos="overlay-left">
                <img src="<?php echo $imagem; ?>" alt="blog">
                <div class="calendar">
                    <a href="javascript:void(0);"><span class="date"><?php echo $public_data[0]; ?></span><br><?php echo $public_data[1]; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-7 details_with_sidebar">
                    <article class="blog_post post_expand">
                        <div class="post_content">
                            <div class="post_header">
                                <h2 class="post_title">
                                    <a href="javascript:void();"><?php echo $public_titulo; ?></a>
                                </h2>
                                <h4><?php echo $public_subtitulo; ?></h4>
                            </div>
                            <div class="post_fulltext">
                                <div class="fulltext_section">
                                    <?php echo $public_descricao; ?>
                                </div>
                                <?php 
                                    $galeria_imagens = get_uploads("publicacoes", $public_codigo, 83);
                                    if(!empty($galeria_imagens)){
                                ?>
                                <h2 style="margin-top: 50px;">Galeria de Imagens</h2>
                                <div class="main-wrapper m_tp_0">
                                    <div class="container">                 
                                        <div class="post_by_category_galery" style="margin-top: 30px;">
                                            <div class="more_posts" data-aos="fade-up" data-aos-duration="900">
                                                <div class="swiper-container">
                                                    <div class="swiper-wrapper lightgallery">
                                                        <?php
                                                            foreach ($galeria_imagens as $key => $value) {
                                                                $img_nome = $value['UP_NOME'];      
                                                                $img_titulo = $value['UP_TITULO'];      
                                                                $img_descricao = $value['UP_DESCRICAO'];    
                                                                $img_img = capture_alluploads($value); 
                                                                if(empty($img_descricao)) $img_descricao = $img_titulo;  
                                                        ?>
                                                        <a href="<?php echo $img_img; ?>" class="swiper-slide" data-sub-html="<?php echo $img_descricao; ?>" data-src="<?php echo $img_img; ?>" style="padding: 0px!important; width: 100%; position: relative; float: left; overflow: hidden; cursor: pointer;">
                                                            <img src="<?php echo $img_img; ?>" alt="<?php echo $img_descricao; ?>" class="img-fluid"/>
                                                        </a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <!-- Add Arrows -->
                                                <div class="swip_button swiper-pagination-category">
                                                    <div class="swiper-button-prev-category_galery" style="margin-left: 10px!important; background: #cecece;"><i class="ion-ios-arrow-left"></i></div>
                                                    <div class="swiper-button-next-category_galery" style="margin-right: 10px!important; background: #cecece;"><i class="ion-ios-arrow-right"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="fulltext_section">
                                    <p><i>*** É expressamente proibida a reprodução de qualquer fotografia e texto aqui publicados sem a atribuição dos devidos créditos à autora. Dúvidas e sugestões podem ser enviadas ao e-mail jessicagedel@gmail.com</i></p>
                                </div>
                            </div>

                            <div class="post_footer">
                                <div class="post_share" data-aos="fade-up" data-aos-duration="700">
                                    <ul class="share_list">
                                        <li>Compartilhe:</li>
                                        <li data-aos="fade-up" data-aos-duration="500"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo UrlAtual(); ?>"><i class="ion-social-facebook"></i></a></li>
                                        <li data-aos="fade-up" data-aos-duration="700"><a href="http://twitter.com/intent/tweet?text=<?php echo $public_titulo;?>&url=<?php echo UrlAtual(); ?>"><i class="ion-social-twitter"></i></a></li>
                                        <li data-aos="fade-up" data-aos-duration="900"><a href="https://api.whatsapp.com/send?text= Veja esta Publicação: <?php echo $public_titulo; ?> - Segue o Link: <?php echo UrlAtual(); ?>"><i class="ion-social-whatsapp"></i></a></li>
                                    </ul>
                                </div>
                                <!-- <div class="post_tag" data-aos="fade-up" data-aos-duration="900">
                                    <ul class="tag_list">
                                        <li><a href="#">#Photography</a></li>
                                        <li><a href="#">#Travel</a></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                    </article>

                    <div class="author_div" data-aos="fade-up" data-aos-duration="700">
                        <div class="author">
                            <img alt="img" src="<?php echo INSAITE . 'images/author/125x125author.jpg'; ?>" class="avatar">
                        </div>
                        <div class="author-block">
                            <h5 class="author_name">Jessica Edel</h5>
                            <p class="author_intro">
                                A paixão pela escrita e por viagens me fez criar o blog. 
                                Unindo o útil ao agradável, aqui eu exponho minhas experiências de Norte a Sul através do jornalismo literário.
                                <br>
                                Sou uma estudante de jornalismo apaixonada por histórias, viagens e pela vida!
                            </p>
                            <div class="social_media">
                                <ul class="social_list">
                                    <li data-aos="fade-up" data-aos-duration="700"><a href="https://www.instagram.com/jessica_edel__/" target="_blank"><i class="ion-social-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="related_posts" data-aos="fade-up" data-aos-duration="900">
                        <div class="sec_title">
                            <h1>Publicações Relacionadas</h1>
                        </div>
                        <div class="more_posts">
                            <div class="row">
                                <?php 
                                    $home_publicacoes_related = newsql("SELECT * FROM tbl_publicacoes as publicacao INNER JOIN tbl_publicacoes_categorias as categoria ON categoria.CAT_CODIGO = publicacao.PUB_CATEGORIA WHERE publicacao.PUB_VISIBILIDADE = 'true' AND publicacao.PUB_CHECK = 'true' AND publicacao.PUB_CODIGO != '{$public_codigo}' AND publicacao.PUB_CATEGORIA = '{$public_categoria}' ORDER BY publicacao.PUB_DATA DESC LIMIT 0,2");
                                        foreach ($home_publicacoes_related as $key => $value) {
                                            $public_related_codigo      = $value['PUB_CODIGO'];
                                            $public_related_time        = $value['PUB_CADASTRO_TIME'];
                                            $public_related_user        = $value['PUB_CADASTRO_USUARIO'];
                                            $public_related_categoria   = $value['PUB_CATEGORIA'];
                                            $public_related_titulo      = $value['PUB_TITULO'];
                                            $public_related_subtitulo   = limit($value['PUB_SUBTITULO'], 250);
                                            $public_related_descricao   = $value['PUB_DESCRICAO'];
                                            $public_related_legenda     = $value['PUB_LEGENDA'];
                                            $public_related_slug        = $value['PUB_SLUG'];
                                            $public_related_views       = $value['PUB_VIEWS'];
                                            $public_related_data        = details_data($value['PUB_DATA']);
                                            $category_related_titulo    = $value['CAT_TITULO'];

                                            if(empty($public_related_legenda)) $public_related_legenda = 'Imagem de ' . $public_related_titulo;
                                            
                                            $get_imagens_related = get_uploads("publicacoes_unico",$public_related_codigo,"");
                                            $imagem_related = capture_uploads($get_imagens_related);

                                            $search_ancor_category_related = SITE . 'publicacoes/' . Remove_caracter($category_related_titulo) . '?category=' . $public_related_categoria;
                                            $search_ancor_related = SITE . 'publicacao/' . $public_related_slug;

                                            if(empty(trim($public_related_subtitulo))){ $public_related_subtitulo = limit($public_related_descricao, 250); }
                                ?>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <article class="blog_post" data-aos="fade-up" data-aos-duration="700">
                                        <div class="post_img" data-aos="overlay-left">
                                            <a href="<?php echo $search_ancor_related; ?>"><img src="<?php echo $imagem_related; ?>" alt="<?php echo $public_related_legenda; ?>"></a>
                                        </div>
                                        <div class="post_content">
                                            <div class="post_header">
                                                <h2 class="post_title">
                                                    <a href="<?php echo $search_ancor_related; ?>"><?php echo $public_related_titulo; ?></a>
                                                </h2>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                    
                    <!-- <div class="row">
                        <div class="col-lg-12">
                            <div class="comment_sec" data-aos="fade-up" data-aos-duration="700">
                                <div class="contact_title">
                                    <h3>Comentários</h3>
                                    <div class="title_line"></div>
                                </div>

                                <div class="fb-comments" data-href="<?php echo UrlAtual(); ?>" data-width="100%" data-numposts="5"></div>
                            </div>
                        </div>
                    </div> -->
                </div>
                
                <div class="col-lg-4 col-md-5">
                    <div class="sidebar" data-aos="fade-up">
                        <div id="popular-1" class="widget widget_popular_posts">
                            <h4 class="widget_title">Mais Lidos</h4>
                            <div class="sidebar_popular_posts">
                                <?php 
                                    $home_publicacoes_body1 = newsql("SELECT * FROM tbl_publicacoes as publicacao INNER JOIN tbl_publicacoes_categorias as categoria ON categoria.CAT_CODIGO = publicacao.PUB_CATEGORIA WHERE publicacao.PUB_VISIBILIDADE = 'true' AND publicacao.PUB_CHECK = 'true' AND publicacao.PUB_CODIGO != '{$public_codigo}' ORDER BY publicacao.PUB_VIEWS DESC LIMIT 2");
                                    foreach ($home_publicacoes_body1 as $key => $value) {
                                        $public2_codigo      = $value['PUB_CODIGO'];
                                        $public2_time        = $value['PUB_CADASTRO_TIME'];
                                        $public2_user        = $value['PUB_CADASTRO_USUARIO'];
                                        $public2_categoria   = $value['PUB_CATEGORIA'];
                                        $public2_titulo      = $value['PUB_TITULO'];
                                        $public2_subtitulo   = limit($value['PUB_SUBTITULO'], 250);
                                        $public2_descricao   = $value['PUB_DESCRICAO'];
                                        $public2_legenda     = $value['PUB_LEGENDA'];
                                        $public2_slug        = $value['PUB_SLUG'];
                                        $public2_views       = $value['PUB_VIEWS'];
                                        $public2_data        = details_data($value['PUB_DATA']);
                                        $category2_titulo    = $value['CAT_TITULO'];

                                        if(empty($public2_legenda)) $public2_legenda = 'Imagem de ' . $public2_titulo;
                                        
                                        $get_imagens2 = get_uploads("publicacoes_unico",$public2_codigo,"");
                                        $imagem2 = capture_uploads($get_imagens2);

                                        $search_ancor_category2 = SITE . 'publicacoes/' . Remove_caracter($category2_titulo) . '?category=' . $public2_categoria;
                                        $search_ancor2 = SITE . 'publicacao/' . $public2_slug;

                                        if(empty(trim($public2_subtitulo))){ $public2_subtitulo = limit($public2_descricao, 250); }

                                ?>
                                <div class="popular_post_item">
                                    <div class="popular_post_item_wrapper">
                                        <img src="<?php echo $imagem2; ?>" alt="<?php echo $public2_legenda; ?>">
                                        <div class="popular_post_item_content">
                                            <h4 class="popular_post_item_title"><a href="<?php echo $search_ancor2; ?>"><?php echo $public2_titulo; ?></a></h4>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div id="custom_2" class="widget widget_custom">
                            <div class="sidebar_add">
                                <img src="<?php echo TEMPLATE . 'images/add.png'; ?>" alt="add">
                            </div>
                        </div>
                        
                        <div id="categories-1" class="widget widget_categories">
                            <h4 class="widget_title">Categorias</h4>
                            <div class="sidebar_categories">
                                <ul class="category_list">
                                    <?php  
                                        $home_categorys_sidebar = newsql("SELECT * FROM tbl_publicacoes_categorias WHERE CAT_VISIBILIDADE = 'true' AND CAT_CHECK = 'true' ORDER BY CAT_TITULO ASC LIMIT 3");
                                        foreach ($home_categorys_sidebar as $key => $value) {
                                            $category_codigo    = $value['CAT_CODIGO'];
                                            $category_titulo    = $value['CAT_TITULO'];
                                            $category_count = newsql("SELECT COUNT(*) as total FROM tbl_publicacoes WHERE PUB_CATEGORIA = '{$category_codigo}' AND PUB_VISIBILIDADE = 'true' AND PUB_CHECK = 'true'")[0]['total'];
                                            $search_ancor = SITE . 'publicacoes/' . Remove_caracter($category_titulo) . '?category=' . $category_codigo;
                                    ?>
                                    <li><a href="<?php echo $search_ancor; ?>"><?php echo $category_titulo; ?></a> (<?php echo $category_count; ?>)</li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        

                        <div id="recent-posts-1" class="widget widget_recent_posts">
                            <h4 class="widget_title">Recentes</h4>
                            <div class="sidebar_recent_posts">
                                <ul>
                                    <?php 
                                    $home_publicacoes_views = newsql("SELECT * FROM tbl_publicacoes as publicacao INNER JOIN tbl_publicacoes_categorias as categoria ON categoria.CAT_CODIGO = publicacao.PUB_CATEGORIA WHERE publicacao.PUB_VISIBILIDADE = 'true' AND publicacao.PUB_CHECK = 'true' AND publicacao.PUB_CODIGO != '{$public_codigo}' ORDER BY publicacao.PUB_DATA DESC LIMIT 0,4");
                                        foreach ($home_publicacoes_views as $key => $value) {
                                            $public1_codigo      = $value['PUB_CODIGO'];
                                            $public1_titulo      = $value['PUB_TITULO'];
                                            $public1_slug        = $value['PUB_SLUG'];
                                            $search1_ancor       = SITE . 'publicacao/' . $public1_slug;
                                    ?>
                                    <li><a href="<?php echo $search1_ancor; ?>"><?php echo $public1_titulo; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        
                        <!-- <div id="tags-1" class="widget widget_tag_cloud">
                            <h4 class="widget_title">Tag Cloud</h4>
                            <div class="tagcloud">
                                <a href="#">Travel</a>
                                <a href="#">PHOTOGRAPHY</a>
                                <a href="#">FASHION</a>
                                <a href="#">Travel</a>
                                <a href="#">HOBBY</a>
                                <a href="#">PHOTOGRAPHY</a>
                                <a href="#">NATURE</a>
                            </div>
                        </div> -->
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

            
<?php include DIR_INCLUDE . 'footer.php'; ?>
