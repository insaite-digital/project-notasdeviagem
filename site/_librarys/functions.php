<?php 
/*=======================================================*/
/** FUNÇÕES - Básicas **/
/*=======================================================*/
    /* Formatação de Resultados */
    function print_($vl){
        echo '<pre>';
        print_r($vl);
        echo '</pre>';
    }

    /* Pegar Código Url site */
    function getCod($pos = NULL){
        if(empty($pos)) $pos =1;
        $URI = $_SERVER["REQUEST_URI"];
        $path = (explode('site/', $URI));
        $path = explode('/', $path[1]);
        if(empty($path[$pos]) || $path[$pos] == '') $path[$pos] = '';
        return $path[$pos];
    }

    /* Limitar tamanho de textos */
    function limit($texto, $qtd){
        if(strlen($texto) == $qtd) $frase = $texto;
        else {
            $ps = explode(' ', $texto);
            $frase = '';
            if($qtd >= 3) {
                foreach($ps as $p){
                    $c = strlen($frase);
                    if($c + strlen($p) + 3 >= $qtd) { $frase .= '...'; break; }
                    $frase .= ' ' . $p;
                }
            }
        }
        return $frase;
    }

    /* Padrão Palavras URL */
    function Remove_caracter($string) {
        $what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','/','=','?','~','^','>','<','ª','º' );
        $by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
        $string = str_replace($what, $by, $string);
        $string = strtolower($string);
        return $string;
    }

    function UrlAtual(){
        $dominio= $_SERVER['HTTP_HOST'];
        $url = "https://" . $dominio. $_SERVER['REQUEST_URI'];
        return $url;
    }

    /*Pegar Pagina Variavel declarda no header do site*/
    function getPagina(){
        $URI = $_SERVER["REQUEST_URI"];
        $path = (explode('site/', $URI));
        $path = explode('/', $path[1]);
        if(empty($path) || $path[0] == '') $path[0] = 'home';
        return $path[0];
    }

/*=======================================================*/
/** FUNÇÕES - BANCO DE DADOS **/
/*=======================================================*/
    /* Consultar Dados */
    function newsql($sql){
        $qry = mysqli_query($GLOBALS["conexao"], "$sql");
        if(empty($qry)) return false;
        $dados = array();
        while($dad = mysqli_fetch_array($qry)){ $dados[] = $dad; }
        return $dados;
    }

    /* Cadastrar Dados */
    function newinsert($tabela, $sql){
        $sql = "INSERT INTO " . $tabela . " " . $sql ;
        if(mysqli_query($GLOBALS["conexao"], "$sql")) return true;
        else return false; 
    }

    /* Alterar Dados */
    function newupdate($tabela, $sql){
        $sql = "UPDATE " . $tabela . " SET " . $sql ; 
        if(mysqli_query($GLOBALS["conexao"], "$sql")) return true;
        else return false; 
    }

    /* Deletar Dados */
    function newdelete($tabela, $sql){
        $sql = "DELETE FROM " . $tabela . ' ' . $sql ;
        if(mysqli_query($GLOBALS["conexao"], "{$sql}")) return true;  
        else return false; 
    }

/*=======================================================*/
/** Funções utilizadas configuração do site amigavel **/
/*=======================================================*/
    /* URl Amigavel */
    function request_url($url, $online){
        if($online == 'true'){ $on = 2; } 
        else { $on = 3; }
    	$dados =  array();
    	$URI = $_SERVER["REQUEST_URI"];
    	$path = (explode('/', $URI));
    	$dados['page'] = $path[$on]; //Setando a página ao subir o site trocar para numero 2
    	return $dados;
    }

    /* Validar Url  */
    function validaurl($url){
    	if(empty($url) || is_null($url)) $url = 'home';
    	return ROOT . DS  . $url . '.php';
    }

/*=======================================================*/
/** Funções utilizadas no projeto padrão **/
/*=======================================================*/
    /* Função enviar mensagem usando PHP Mailer */
    function sendMessage($to, $body, $subject, $from = NULL){
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        try {
            $mail->SMTPAuth   = true;
            $mail->Host       = 'smtp.siscomp.inf.br';
            $mail->Port       = '587';
            $mail->Username   = 'contato@siscomp.inf.br';
            $mail->Password   = 'Siscomp@2021*';
            $mail->AddAddress($to);
            $mail->SetFrom('contato@siscomp.inf.br');
            $mail->CharSet = 'UTF-8';
            $mail->AddReplyTo($from);
            $mail->Subject = $subject;
            $mail->MsgHTML($body);
            return $mail->Send();
        } catch (phpmailerException $e) {
            echo $e;
            return false;
        }
    }

    /* Abrir arquivo de email */
    function openFile ($file) {
        $fp = fopen($file,"r");	
        return fread($fp,filesize($file));
    }

    /* Validação de E-mail */
    function validateEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }


/*=======================================================*/
/** Funções utilizadas para validar a identificação de usuários **/
/*=======================================================*/
    /* Função validar CPF */
    function validaCPF($cpf) {
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );
        if (strlen($cpf) != 11) { return false; }
        if (preg_match('/(\d)\1{10}/', $cpf)) { return false; }
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) { $d += $cpf{$c} * (($t + 1) - $c); }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) { return false; }
        }
        return true;
    }

    /* Função validar CNPJ */
    function validar_cnpj($cnpj){
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
        if (strlen($cnpj) != 14) return false;
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto)) return false;
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }

    /* Formatar CPF & CNPJ para inserção em Banco de Dados */
    function limpaCPF_CNPJ($valor){
        $valor = trim($valor);
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", "", $valor);
        $valor = str_replace("-", "", $valor);
        $valor = str_replace("/", "", $valor);
        return $valor;
    }


/*=======================================================*/
/** Funções utilizadas para Buscar Imagens **/
/*=======================================================*/
    /* Consultar & Buscar Uploads */
    function get_uploads($tipo, $codigo, $type){
        if(empty($type)){ $sql = "SELECT * FROM config_uploads WHERE UP_TABELA = '{$tipo}' AND UP_COD_REG = '{$codigo}' ORDER BY UP_CODIGO ASC"; } 
        else { $sql = "SELECT * FROM config_uploads WHERE UP_TABELA = '{$tipo}' AND UP_COD_REG = '{$codigo}' AND UP_TIPO = '{$type}' ORDER BY UP_CODIGO ASC"; }
        $qry = mysqli_query($GLOBALS["conexao"], "$sql") or die ($sql);
        $dados = array();
        while($dad = mysqli_fetch_array($qry)){
            $dados[] = $dad;
        }
        return $dados;
    }
    /* Pegar Primeira Imagem */
    function capture_uploads($midia){
        $mid = SITEROOT . "uploads/".$midia[0]['UP_TABELA']."/".$midia[0]['UP_COD_REG']."/".$midia[0]['UP_NOME'];
        return $mid;
    }

    /* Pegar Todas as Imagens */
    function capture_alluploads($midia){
        $mid = SITEROOT . "uploads/".$midia['UP_TABELA']."/".$midia['UP_COD_REG']."/".$midia['UP_NOME'];
        return $mid;
    }

/*=======================================================*/
/** Funções utilizadas para Buscar Imagens **/
/*=======================================================*/
    /*Formatação de Data*/
    function details_data($data) {
        $explode = (explode('-', $data));
        $ano = $explode[0];
        $mes = $explode[1];
        $dia = $explode[2];

        $mes_abreviado = array('01' => 'Jan', '02' => 'Fev', '03' => 'Mar', '04' => 'Abr', '05' => 'Mai', '06' => 'Jun', '07' => 'Jul', '08' => 'Ago', '09' => 'Set', '10' => 'Out', '11' => 'Nov', '12' => 'Dez');
        $mes_extenso = array('01' => 'Janeiro', '02' => 'Fevereiro', '03' => 'Março', '04' => 'Abril', '05' => 'Maio', '06' => 'Junho', '07' => 'Julho', '08' => 'Agosto', '09' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro');

        $arraydata = array();
        $arraydata[0] = $dia;
        $arraydata[1] = $mes_abreviado["$mes"];
        $arraydata[2] = $ano;
        $arraydata[3] = $mes_extenso["$mes"];
        $arraydata[4] = $mes;
        return $arraydata;
    }
    function data_format($data) { return $data[0] . '/' . $data[4] . '/' . $data[2]; }
    function data_extensa($data) { return $data[0] . ', ' . $data[3] . ', ' . $data[2]; }
    function data_abreviada($data) { return $data[0] . ' de ' . $data[1] . ' de ' . $data[2]; }


/*=======================================================*/
/** Funções utilizadas para SEO **/
/*=======================================================*/
/*Titulo da Pagina*/
function get_title(){
    $URI = $_SERVER["REQUEST_URI"];
    $path = (explode('site/', $URI));
    $path = explode('/', $path[1]);
    if(!empty($path[0])){
        if($path[0] == "publicacao"){
            $slug = getCod();
            $sql = newsql("SELECT * FROM tbl_publicacoes WHERE PUB_SLUG = '{$slug}'")[0];
            $titulo = $sql['PUB_SEO_TITULO'];
            if(empty($titulo)){ $titulo = $sql['PUB_TITULO']; }
        } 
        else if($path[0] == 'publicacoes'){ $titulo = "Publicações de Viagens"; } 
        else if($path[0] == 'contato'){ $titulo = "Entre em Contato"; } 
        else if($path[0] == 'autoria'){ $titulo = "Um pouco sobre a Autora"; } 
        else { $titulo = "Notas de Viagem"; }
    } else { $titulo = "Notas de Viagem"; }
    return $titulo;
}

/*Descricão da Pagina*/
function get_descricao(){
    $URI = $_SERVER["REQUEST_URI"];
    $path = (explode('site/', $URI));
    $path = explode('/', $path[1]);

    if(!empty($path[0])){
        if($path[0] == "publicacao"){
            $slug = getCod();
            $sql = newsql("SELECT * FROM tbl_publicacoes WHERE PUB_SLUG = '{$slug}'")[0];
            $descricao = limit(strip_tags(htmlspecialchars_decode($sql['PUB_SEO_DESCRICAO']),""), 350);
            if(empty(trim($descricao))){ $descricao = limit(strip_tags(htmlspecialchars_decode($sql['PUB_DESCRICAO']),""), 350); }
        } 
        else if($path[0] == 'publicacoes'){ $descricao = "Confira nossa lista de viagens e conheça mais sobre estas experiências!"; } 
        else if($path[0] == 'contato'){ $descricao = "Caso tenha alguma dúvida, sugestão ou denúncia, por favor entre em contato através desta página."; } 
        else if($path[0] == 'autoria'){ $descricao = "Sou autora do Notas de Viagem e colunista no Canal Ideal. Além de jornalista em formação, sou uma apaixonada por viagens, natureza, conhecimento e pela arte de escrever. O Notas de Viagem teve início em 2019, quando eu decidi criar uma plataforma para compartilhar minhas experiências através de um blog focado na produção de conteúdo sobre lugares próximos (como foco na região Sul) e com valores acessíveis. "; } 
        else { $descricao = "Website destinado a viagens, cultura, turismo e fotografia, com o intuito de passar uma experiência única através da escrita, imagem e multimidia."; }
    } else { $descricao = "Website destinado a viagens, cultura, turismo e fotografia, com o intuito de passar uma experiência única através da escrita, imagem e multimidia.";  } 
    return $descricao;
}

function get_image(){
    $URI = $_SERVER["REQUEST_URI"];
    $path = (explode('site/', $URI));
    $path = explode('/', $path[1]);

    if(!empty($path[0])){
        if($path[0] == "publicacao"){
            $slug = getCod(1);
            $sql = newsql("SELECT * FROM tbl_publicacoes WHERE PUB_SLUG = '{$slug}'")[0];
            $pub_imagens = get_uploads("publicacoes_unico", $sql['PUB_CODIGO'], "");
            $imagem_destaque = capture_uploads($pub_imagens); 
        } 
        else if($path[0] == 'publicacoes'){ $imagem_destaque = INSAITE . 'images/publics.jpg'; } 
        else if($path[0] == 'autoria'){ $imagem_destaque = INSAITE . 'images/author/author.jpg'; } 
        else if($path[0] == 'contato'){ $imagem_destaque = INSAITE . 'images/contact.jpg'; } 
        else { $imagem_destaque = INSAITE . 'images/home.jpeg'; }
    } else { $imagem_destaque = INSAITE . 'images/home.jpeg'; }
    return $imagem_destaque;
}








function get_parameters_url(){
	$URI = $_SERVER["REQUEST_URI"];
	$path = (explode('site/', $URI));
	$path = (explode('/', $path[1]));
	if(!empty($path[1])) {
		$var = explode('?', $path[1]);
		$path[1] = $var[1];
		$parameters = explode('&', $path[1]);
		$url_params = array();
		foreach($parameters as $key=>$param){
			$params = explode('=', $param);
			$url_params[$key] = array('collun'=>$params[0], 'value'=>$params[1]);
		}
	}
	if(!empty($url_params)){
	   $path[1] = $url_params;
	} else {
	   $path[1] = array();
	}
	return $path;
}

function get_parameter_select($collun, $parameters){
	foreach($parameters[1] as $key=>$paramter){
		if($paramter['collun'] == $collun){
			return $paramter['value'];
			break;
		}
	}
	return '';
}

function get_parameters_url_unique(){
    $URI = $_SERVER["REQUEST_URI"];
    $path = (explode('site/', $URI));
    $path = (explode('/', $path[1]));

    if(!empty($path[2])) {
        $var = explode('?', $path[2]);
        $path[2] = $var[1];
        $parameters = explode('&', $path[2]);
        $url_params = array();
        foreach($parameters as $key=>$param){
            $params = explode('=', $param);
            $url_params[$key] = array('collun'=>$params[0], 'value'=>$params[1]);
        }
    }
    if(!empty($url_params)){
       $path[2] = $url_params;
    } else {
       $path[2] = array();
    }
    return $path;
}

function get_parameter_unique($collun, $parameters){
    foreach($parameters[2] as $key=>$paramter){
        // print_($paramter['collun']);
        // print_($collun);
        if($paramter['collun'] == $collun){
            return $paramter['value'];
            break;
        }
    }
    return '';
}

function format_phone($phone) {
  $explode1 = explode(')', $phone);
  $explode2 = explode('(', $explode1[0]);
  $first_code = $explode2[1];
  $second_number = $explode1[1];
  $phone = '(' . $first_code . ') 9' . $second_number;
  return $phone;
}

function senhaValida($senha) {
    return preg_match('/[a-z]/', $senha) // tem pelo menos uma letra minúscula
    //  && preg_match('/[A-Z]/', $senha) // tem pelo menos uma letra maiúscula
     && preg_match('/[0-9]/', $senha) // tem pelo menos um número
     && preg_match('/^[\w$@]{6,}$/', $senha); // tem 6 ou mais caracteres
}

function tempo_leitura($descricao) {
    $word = str_word_count(strip_tags($descricao));
    $m = floor($word / 200);
    $s = floor($word % 200 / (200 / 60));
    $est = $m . ' minuto' . ($m == 1 ? '' : 's') . ' e ' . $s . ' segundo' . ($s == 1 ? '' : 's');
    return $est;
}


function maiuscula($string) {
    $string = mb_strtolower(trim(preg_replace("/\s+/", " ", $string)));
    $palavras = explode(" ", $string);
    $t =  count($palavras);
    for ($i=0; $i <$t; $i++){ 
        $retorno[$i] = ucfirst($palavras[$i]);
            if($retorno[$i] == "Dos" || $retorno[$i] == "De" || $retorno[$i] == "Do" || $retorno[$i] == "Da" || $retorno[$i] == "E" || $retorno[$i] == "Das"):
                $retorno[$i] = mb_strtolower($retorno[$i]);
            endif;  
    }
    return implode(" ", $retorno);
}





