<nav id="flexmenu">
    <div class="logo">
        <a href="<?php echo SITE; ?>"><img src="<?php echo INSAITE . 'images/projeto/logo.png'; ?>" alt="logo"></a>
    </div>
    <div class="nav-inner">
        <div id="mobile-toggle" class="mobile-btn"></div>
        <ul class="main-menu">
            <li class="menu-item <?php if(empty($pag) OR $pag == 'home') echo 'active'; ?>"><a href="<?php echo SITE; ?>">Inicial</a>
                <!-- <ul class="sub-menu">
                    <li class="menu-item"><a href="index-2.html">Home 2</a></li>
                    <li class="menu-item"><a href="index-3.html">Home 3</a></li>
                </ul> -->
            </li>
            <li class="menu-item <?php if($pag == 'publicacoes') echo 'active'; ?>"><a href="<?php echo SITE . 'publicacoes'; ?>">Publicações</a>
                <!-- <ul class="sub-menu">
                    <li class="menu-item"><a href="blog-details.html">Blog Details</a></li>
                    <li class="menu-item"><a href="blog-details-fullwidth.html">Blog Details Fullwidth</a></li>
                </ul> -->
            </li>
            <li class="menu-item <?php if($pag == 'autoria') echo 'active'; ?>"><a href="<?php echo SITE . 'autoria'; ?>">Autoria</a></li>
            <li class="menu-item <?php if($pag == 'contato') echo 'active'; ?>"><a href="<?php echo SITE . 'contato'; ?>">Contato</a></li>
        </ul>
    </div>
</nav>