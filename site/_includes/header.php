<?php 
    $pag = getPagina();
    $web_title = get_title();
    $web_description = get_descricao();
    $web_images = get_image();
    $web_parametros = get_parameters_url();
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <!-- Meta Tags -->
        <title>Notas de Viagem - <?php echo $web_title; ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="description" content="<?php echo $web_description; ?>">
        <meta name="author" content="Jessica Edel">
        <meta property="og:title" content="<?php echo $web_title; ?>"/>
        <meta property="og:description" content="<?php echo $web_description; ?>"/>
        <meta property="og:image" content="<?php echo $web_images; ?>"/>
        <meta property="og:url" content="<?php echo UrlAtual(); ?>"/>
        <meta property="og:site_name" content="<?php echo $web_images; ?>"/>
        <meta name="twitter:url" content="<?php echo UrlAtual(); ?>">
        <meta name="twitter:title" content="<?php echo $web_title; ?>">
        <meta name="twitter:description" content="<?php echo $web_description; ?>">
        <meta name="twitter:image" content="<?php echo $web_images; ?>">
        <meta name="twitter:card" content="article">

        <!-- Favicon and touch Icons -->
        <link href="<?php echo INSAITE . 'images/projeto/favicon.png'; ?>" rel="shortcut icon" type="image/png">

        <!-- Lead Style -->
        <link href="<?php echo TEMPLATE . 'css/style.css'; ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo INSAITE . 'css/style.css'; ?>" rel="stylesheet">
        <link href="<?php echo INSAITE . 'css/sweetalert/sweetalert2.css'; ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo PLUGIN . 'lightGallery/dist/css/lightgallery.css'; ?>">
        
        <?php if($pag == 'publicacao'){ ?>
            <!-- Comentários Facebook Integração --->
            <div id="fb-root"></div>
            <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v12.0" nonce="iTtHeKYt"></script>
        <?php } ?>

    </head>

    <body>
        <div id="preloader" class="loader_show">
            <div class="loader-wrap">
            <div class="loader">
                <div class="loader-inner"></div>
            </div>
            </div>
        </div>
        
        <!-- pointer start -->
        <div class="pointer bnz-pointer" id="bnz-pointer"></div>
        <div class="content">
