            <footer class="footer" data-aos="fade-up" data-aos-duration="700">
                <div class="footer-above">
                    <div class="footer-logo">
                        <a href="<?php echo SITE; ?>"><img class="img-fluid" src="<?php echo INSAITE . 'images/projeto/logo_white.png'; ?>" alt="logo"></a>
                    </div>
                    <div class="footer-social">
                        <ul>
                            <li><a href="https://www.facebook.com/BlogNotasdeViagem" target="_blank"><i class="ion-social-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/notas_de_viagem/" target="_blank"><i class="ion-social-instagram"></i></a></li>
                            <!-- <li><a href="#"><i class="ion-social-twitter"></i></a></li> -->
                        </ul>
                    </div>
                </div>
                <div class="footer-middle">
                    <div class="container">
                        <div class="footer_widget_wrapper">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="footer_widget">
                                        <div class="widget-title" style="background: #f09433; background: -moz-linear-gradient(45deg, #f09433 0%, #e6683c 25%, #dc2743 50%, #cc2366 75%, #bc1888 100%); background: -webkit-linear-gradient(45deg, #f09433 0%,#e6683c 25%,#dc2743 50%,#cc2366 75%,#bc1888 100%); background: linear-gradient(45deg, #f09433 0%,#e6683c 25%,#dc2743 50%,#cc2366 75%,#bc1888 100%); ">
                                            <h4>Instagram</h4>
                                        </div>
                                        <div id="instafeed" class="instagram"></div>

                                        <!-- <div class="instagram">
                                            <ul>
                                                <li><a href="#"><img src="<?php echo INSAITE . 'images/teste/insta.jpg'; ?>" alt="insta"></a></li>
                                                <li><a href="#"><img src="<?php echo INSAITE . 'images/teste/insta.jpg'; ?>" alt="insta"></a></li>
                                                <li><a href="#"><img src="<?php echo INSAITE . 'images/teste/insta.jpg'; ?>" alt="insta"></a></li>
                                                <li><a href="#"><img src="<?php echo INSAITE . 'images/teste/insta.jpg'; ?>" alt="insta"></a></li>
                                                <li><a href="#"><img src="<?php echo INSAITE . 'images/teste/insta.jpg'; ?>" alt="insta"></a></li>
                                                <li><a href="#"><img src="<?php echo INSAITE . 'images/teste/insta.jpg'; ?>" alt="insta"></a></li>
                                            </ul>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="footer_widget pd_lt">
                                        <div class="blend-logo">
                                            <a href="#"><img src="<?php echo TEMPLATE . 'images/logo-big.png'; ?>" alt="logo"></a>
                                        </div>
                                        <div class="footer_nav">
                                            <ul>
                                                <li class="menu-item"><a href="<?php echo SITE; ?>">Inicial</a></li>
                                                <li class="menu-item"><a href="<?php echo SITE . 'autoria'; ?>">Autoria</a></li>
                                                <li class="menu-item"><a href="<?php echo SITE . 'politica-privacidade'; ?>">Política de Privacidade</a></li>
                                                <li class="menu-item"><a href="#">LGDP</a></li>
                                                <!-- <li class="menu-item"><a href="<?php echo SITE . 'lgpd'; ?>">LGDP</a></li> -->
                                                <li class="menu-item"><a href="<?php echo SITE . 'publicacoes'; ?>">Publicações</a></li>
                                                <li class="menu-item"><a href="<?php echo SITE . 'contato'; ?>">Contato</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="container">
                        <div class="copyright">
                            <p> &copy; <strong><?php echo date('Y'); ?></strong> - Todos os direitos reservados para <a href="javascript:void(0);">Notas de Viagem</a>. <br>Dev <a href="https://insaitedigital.com.br">Insaite Digital</a>.</p>
                        </div>
                    </div>
                </div>
            </footer>

            <?php 
                $access_token = 'IGQVJXdDlUMjhQcEUwM2NfZAUZAUNmJXT2oxLWlpOUJGUHJMc1pWT1UtRW53SmNBU2h6eU4ybU54Umg1ejJuYWdRemtMbEp5dGE0cmNzTXY0VGkyNDU5djA3ZAlFMbW9XMDJNV1VPcXVVaFpVa1ZAYaHN0TAZDZD';
            ?>

        <!-- All JavaScript Files
        ================================================== -->
        <script src="<?php echo TEMPLATE . 'js/jquery-3.2.1.min.js'; ?>"></script>
        <script src="<?php echo TEMPLATE . 'js/bootstrap.min.js'; ?>"></script>
        <script src="<?php echo TEMPLATE . 'plugins/swiper/swiper.min.js'; ?>"></script>
        <script src="<?php echo TEMPLATE . 'js/menu.js'; ?>"></script>
        <script src="<?php echo TEMPLATE . 'plugins/aos/aos.js'; ?>"></script>
        <script src="<?php echo TEMPLATE . 'js/custom.js'; ?>"></script>
        <script src="<?php echo INSAITE . 'js/mask/jquery.mask.min.js'; ?>"></script>
        <script src="<?php echo INSAITE . 'js/sweetalert/sweetalert2.js'; ?>"></script>
        <script src="<?php echo INSAITE . 'js/main.js'; ?>"></script>
        <script type="text/javascript">
        $(document).ready(function(){
            $('.lightgallery').lightGallery();
        });
        </script>
        <script src="<?php echo PLUGIN . 'lightGallery/dist/js/lightgallery-all.min.js'; ?>"></script>
        <script src="<?php echo PLUGIN . 'lightGallery/lib/jquery.mousewheel.min.js'; ?>"></script>
        <script src="<?php echo PLUGIN . 'instafeed/dist/instafeed.min.js'; ?>"></script>
        <script type="text/javascript">
            var feed = new Instafeed({
              accessToken: 'IGQVJXdDlUMjhQcEUwM2NfZAUZAUNmJXT2oxLWlpOUJGUHJMc1pWT1UtRW53SmNBU2h6eU4ybU54Umg1ejJuYWdRemtMbEp5dGE0cmNzTXY0VGkyNDU5djA3ZAlFMbW9XMDJNV1VPcXVVaFpVa1ZAYaHN0TAZDZD',
              limit : 6,
              template: '<a href="{{link}}" target="_blank"><img src="{{image}}" /></a>'
            });
            feed.run();
        </script>
    </body>
</html>
