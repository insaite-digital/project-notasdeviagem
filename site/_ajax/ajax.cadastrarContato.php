<?php
require_once('../config.php');

$email_teste 	= mysqli_real_escape_string($conexao, $_POST['email']);
$nome 			= mysqli_real_escape_string($conexao, $_POST['contato_nome']);
$email 			= mysqli_real_escape_string($conexao, $_POST['contato_email']);
$telefone 		= mysqli_real_escape_string($conexao, $_POST['contato_telefone']);
$mensagem 		= mysqli_real_escape_string($conexao, $_POST['contato_mensagem']);

$cadastro_data 	= date('Y-m-d');
$cadastro_hora 	= date('H:i:s');

if(empty($email_teste)){
	if(!empty($nome) && !empty($email) && !empty($mensagem)){
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			
				$body = openFile("../_mails/EnviaContato.html");
				$to = "jessicagedel@gmail.com";
			    $subject = "Contato Website - Notas de Viagem";
			    $subject = utf8_decode($subject);
				$from = $email;
				
				foreach($_POST as $key=>$value){ $body = str_replace("<".$key.">", utf8_decode(nl2br($value)), $body); }
				$body = str_replace("<nome>", utf8_decode(nl2br($nome)), $body);
				$body = str_replace("<email1>", utf8_decode(nl2br($email)), $body);
				$body = str_replace("<telefone>", utf8_decode(nl2br($telefone)), $body);
				$body = str_replace("<mensagem>", utf8_decode(nl2br($mensagem)), $body);
				$body = str_replace("\\", '',$body);

				if(sendMessage($to,$body,$subject,$from,$nome)){
					$resp = array('resposta' => 'true');	
				}

		} else { $resp = array('resposta' => 'email'); }
	} else { $resp = array('resposta' => 'dados'); }
} else { $resp = array('resposta' => 'spam'); }

echo json_encode($resp);
?>