<?php
require_once('../config.php');

$email_teste 	= mysqli_real_escape_string($conexao, $_POST['email']);
$email 			= mysqli_real_escape_string($conexao, $_POST['newsletter_email1']);

$cadastro_time 		= date("Y-m-d H:i:s");

if(empty($email_teste)){
	if(!empty($email)){
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				if(newinsert("tbl_newsletters","(NEW_CHECK, NEW_VISIBILIDADE, NEW_CADASTRO_TIME, NEW_EMAIL) VALUES ('true', 'true', '{$cadastro_time}', '{$email}')")){

					$body = openFile("../_mails/EnviaNewsletter.html");
					$to = "jessicagedel@gmail.com";
				    $subject = "Newsletter Website - Notas de Viagem";
				    $subject = utf8_decode($subject);
					$from = $email;
					
					foreach($_POST as $key=>$value){ $body = str_replace("<".$key.">", utf8_decode(nl2br($value)), $body); }
					$body = str_replace("<email1>", utf8_decode(nl2br($email)), $body);
					$body = str_replace("\\", '',$body);

					if(sendMessage($to,$body,$subject,$from,$nome)){
						$resp = array('resposta' => 'true');	
					}
				}

		} else { $resp = array('resposta' => 'email'); }
	} else { $resp = array('resposta' => 'dados'); }
} else { $resp = array('resposta' => 'spam'); }

echo json_encode($resp);
?>