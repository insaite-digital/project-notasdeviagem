<?php

//-- Carregando as configuração inicias
require 'config.php';

$url = isset($_GET['url']) ? $_GET['url'] : '';

if($url == "" || empty($url)) { 
	include 'home.php';
} else {
	$page = request_url($url, $projeto_online); //explode a url

	/** exceção para uploads **/
	if($page == 'uploads'){ Header("Location: ".SITE.$url);}
	/** fim exceção para uploads **/
    
	$pag = validaurl($page['page']); //valida a requisição de página
	if(file_exists($pag)) include $pag;
 	else include ROOT . DS . '_erros' . DS . '404.php';
}
