<?php

// Setando tempo de sessão para uma hora
ini_set('session.gc_maxlifetime', 3600);
session_set_cookie_params(3600);

session_start();
require_once('../funcoes/funcoes.php');
require_once('../classes/Conexao.class.php');
require_once('../classes/Login.class.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();

// $secretKey  = "6LeyZgMbAAAAAOX2kWIeHLW3CJx3K00rHpG22ROX";
// if(isset($_POST['captcha-response']) && !empty($_POST['captcha-response'])){

      // $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secretKey.'&response='.$_POST['captcha-response']);
      // $responseData = json_decode($verifyResponse);
      // if($responseData->success == '1'){
            $usuario = mysqli_real_escape_string($conexao, $_POST['usuario']);
            $senha = mysqli_real_escape_string($conexao, $_POST['senha']);
            
            $login = new Login;
            $login->tabela = 'config_usuarios';

            if ($login->ValidaUsuario($usuario, $senha) == 1) {
                  if ($login->UsuarioLogado()) {
                        
                        header('Location: ../index.php');   
                  } else {
                        if ($login->LogaUsuario($usuario, $senha)) {
                              
                              /** LOGS INSERT **/
                              $path_pagina = pathinfo( __FILE__ )['basename'];
                              insert_logs($path_pagina,"");
                              
                              header('Location: ../index.php');   
                        } else { header('Location: ../login.php?adv=true'); }
                  }
            } else { header('Location: ../login.php?adv=true'); }
//       } else { header('Location: ../login.php?adv=true'); }
// } else { header('Location: ../login.php?adv=true'); }






