<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
require_once('../classes/Conexao.class.php');
require_once('../classes/Login.class.php');
require_once('../funcoes/funcoes.php');

$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();

$usuario = $_SESSION['CODIGO_USUARIO'];


$data = date('Y-m-d');
$hora = date('H:i:s');
$tbl = "config_usuarios";
$upd = "USU_LOGADO = 'false',USU_DATA_LOGOUT = '".$data."',USU_HORA_LOGOUT = '".$hora."' WHERE USU_CODIGO = '".$usuario."'";
$sq = newupdate($tbl,$upd);

$login = new Login;
$login->tabela = 'config_usuarios';

/** LOGS INSERT **/
$path_pagina = pathinfo( __FILE__ )['basename'];
insert_logs($path_pagina,"");

$login->Logout();

header('Location: ../login.php');