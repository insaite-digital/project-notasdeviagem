<?php 
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        header('Location: error.php');
    } else {
        $path_pagina = pathinfo( __FILE__ )['basename'];
        $acesso->Pagina = $path_pagina;
        $acesso->verificaPermissao();
        $resposta = $acesso->getResposta();
        if(empty($resposta) || $resposta == 'false'){ header('Location: direcionamento.php'); } else {

            $paginas = newsql("SELECT * FROM config_paginas WHERE PAG_ARQUIVO = '{$path_pagina}'")[0];
            $pagina_codigo          = $paginas['PAG_CODIGO'];
            $pagina_titulo          = $paginas['PAG_TITULO'];
            $pagina_singular        = $paginas['PAG_SINGULAR'];
            $pagina_plural          = $paginas['PAG_PLURAL'];
            $pagina_icone           = $paginas['PAG_ICONE'];
            $pagina_text_singular   = $paginas['PAG_TEXT_SINGULAR'];
            $pagina_text_plural     = $paginas['PAG_TEXT_PLURAL'];
            
            // Variaveis da Página de Consulta
            // $pagina_coluna_id       = "USU_CODIGO";
            // $pagina_tabela          = "config_usuarios";
            // $url_ajax               = "ajax/proadmin/ajax.alterar".maiuscula($pagina_plural).".php";
            // $link_back              = "consultar" . maiuscula($pagina_plural). ".php?active=".$pagina_plural."";
            // $upload_name            = "usuario"; 
            
            $codigo = $_GET['usuario'];
            $sql_consulta = newsql("SELECT * FROM config_usuarios WHERE USU_CODIGO = '{$codigo}'")[0];
?>
<?php require_once("includes/header.php"); ?>

<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <!-- Página Título -->
                    <span class="text-semibold">
                        <a href="index.php">Dashboard</a>
                    </span> 
                    <i class="icon-arrow-right6"></i> 
                    <strong>Logs de Usuários</strong>
                    <br>
                    <i class="icon-arrow-right6"></i> 
                    <strong><?php echo $sql_consulta['USU_NOME']; ?> </strong>
                    <br>
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <!-- Página Links de Ações -->
                    <div class="heading-btn-group">
                        <!-- Página Links de Ações -->
                        
                        <a href="javascript: history.go(-1)" class="btn btn-link btn-float has-text">
                            <i class="icon-database-arrow text-default"></i> 
                            <span>Voltar Página Anterior</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <!-- Página Breadcrumbs -->
                <li>
                    <a href="index.php">
                        <i class="icon-home2 position-left"></i> 
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <i class="icon-stack3"></i> 
                    <span>Logs de Usuários</span>
                </li>
                <li>
                    <i class="icon-arrow-right6"></i> 
                    <span><?php echo $sql_consulta['USU_NOME']; ?></span>
                </li>
            </ul>
            <ul class="breadcrumb-elements">
                <!-- Modal Ajuda -->
                <?php require_once("includes/ajuda.php"); ?>
            </ul>
        </div>
    </div>
    <div class="content">
        <!-- Detached content -->
        <div class="container-detached">
            <div class="content-detached">

                <!-- Version 1.5 -->
                <div class="panel panel-flat" id="v_1_5">
                    <div class="panel-heading">
                        <h5 class="panel-title"><strong>Logs do Usuário:  <?php echo $sql_consulta['USU_NOME']; ?></strong></h5>
                        <div class="heading-elements">
                            <?php 
                                $data_extenso = details_data(date('Y-m-d'));
                                $data_extenso = data_extensa($data_extenso);
                            ?>
                            <span class="text-muted heading-text"><?php echo $data_extenso; ?></span>
                            <span class="label bg-blue heading-text">Atualizado</span>
                        </div>
                    </div>

                    <div class="panel-body">
                        <!-- <p class="content-group">Version 1.5 is a second part of version 1.4, which includes 22 new pages from 7 categories, various improvements and bug fixes. Please check documentation for a full list of changes and files that need to be updated.</p> -->

                        <?php 
                        ?>

                        <pre class="language-php"><code>// # Lista de Logs // ------------------------------
                        <?php 

                            $sql_logs = newsql("SELECT * FROM config_logs as log INNER JOIN config_modulos as modulo ON modulo.MOD_CODIGO = log.LOG_MODULO INNER JOIN config_modulos_permissoes as modperm ON modperm.MODPERM_CODIGO = log.LOG_MODULO_PERMISSAO INNER JOIN config_usuarios as usuario ON usuario.USU_CODIGO = log.LOG_USUARIO INNER JOIN config_permissoes as permissao ON permissao.PERM_CODIGO = log.LOG_PERMISSAO INNER JOIN config_paginas as pagina ON pagina.PAG_CODIGO = log.LOG_PAGINA WHERE log.LOG_USUARIO = '{$codigo}' ORDER BY log.LOG_CODIGO DESC");
                            
                            foreach ($sql_logs as $key => $value) {
                                // print_($value);
                                $log_codigo = $value['LOG_CODIGO'];
                                $log_usuario = $value['LOG_USUARIO'];
                                $log_permissao = $value['LOG_MODULO_PERMISSAO'];
                                $log_modulo = $value['LOG_MODULO'];
                                $log_permissao = $value['LOG_PERMISSAO'];
                                $log_pagina = $value['LOG_PAGINA'];
                                $log_descricao = $value['LOG_DESCRICAO'];
                                $log_data = $value['LOG_DATA'];
                                $log_hora = $value['LOG_HORA'];
                                $log_ip = $value['LOG_IP'];
                                $log_host = $value['LOG_HOST'];
                                $modulo_titulo = $value['MOD_TITULO'];
                                $modulo_permissao_titulo = $value['MODPERM_TITULO'];
                                $usuario_nome = $value['USU_NOME'];
                                $permissao_titulo = $value['PERM_TITULO'];
                                $page_titulo = $value['PAG_TITULO'];
                                $data_extenso_log = details_data($log_data);
                                $data_extenso_log = data_extensa($data_extenso_log);
echo '
[#'.$log_codigo.'] ' . $data_extenso_log . '-' . $log_hora . '
[Usuário] ' . $usuario_nome .  '
[Pagina] ' . $page_titulo . '
[Modulo] ' . $modulo_titulo . '
[Ação] ' . $log_descricao . '
';
                            }
                        ?>
                        
                        </code></pre>
                    </div>
                </div>
            </div>
        </div>
                    
        <!-- Modal Upload -->
        <?php require_once("includes/copyright.php"); ?>
    </div>
</div>

<script type="text/javascript">
$('.insert_permissao').on('click', function () {
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.insertProModulosPermissoes.php',
        data        : $("#FormInsert").serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })


});


$('.update_permissao').on('click', function () {
    var codigo = $(this).attr("data-codigo");
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.updateProModulosPermissoes.php?code=' + codigo,
        data        : $("#FormEdit"+codigo).serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })


});


$('#update_save').on('click', function () {
    
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.updateProModulos.php',
        data        : $("#FormDefault").serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })


});

</script>
<?php 
    require_once("includes/footer.php");
    } }
?>