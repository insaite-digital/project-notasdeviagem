<?php 
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        header('Location: error.php');
    } else {
        $path_pagina = pathinfo( __FILE__ )['basename'];
        $acesso->Pagina = $path_pagina;
        $acesso->verificaPermissao();
        $resposta = $acesso->getResposta();
        if(empty($resposta) || $resposta == 'false'){ header('Location: direcionamento.php'); } else {
            date_default_timezone_set('America/Sao_Paulo');
            $cadastro_time      = date("Y-m-d H:i:s");
            $cadastro_usuario   = $_SESSION['USUARIO_CODIGO'];
            insert_logs($path_pagina,"");

            /* Variaveis da Página Database */
            $paginas                    = newsql("SELECT * FROM config_paginas WHERE PAG_ARQUIVO = '{$path_pagina}'")[0];
            $pagina_codigo              = $paginas['PAG_CODIGO'];
            $pagina_titulo              = $paginas['PAG_TITULO'];
            $pagina_modulo_permissao    = $paginas['PAG_MODULO_PERMISSAO'];
            $pagina_modulo              = $paginas['PAG_MODULO'];
            $pagina_text_singular       = $paginas['PAG_TEXT_SINGULAR'];
            $pagina_text_plural         = $paginas['PAG_TEXT_PLURAL'];
            $pagina_icone               = $paginas['PAG_ICONE'];
            $pagina_singular            = $paginas['PAG_SINGULAR'];
            $pagina_plural              = $paginas['PAG_PLURAL'];

            /* Variaveis da Página Principal */
                $primary_database_page_check            = "true";
                $primary_database_page_tabela           = "tbl_publicacoes";
                $primary_database_page_collumn_prefix   = "PUB";
                $primary_database_page_collumn_id       = "PUB_CODIGO";
                $primary_database_page_upload           = $pagina_plural;
                $primary_database_page_ajax             = "ajax/ajax.alterar".maiuscula($pagina_plural).".php";
                $primary_database_page_backlink         = "consultar" . maiuscula($pagina_plural). ".php?active=".$pagina_plural."";

            /* Variaveis da Página Secundária */
                $secondary_database_page_check            = "false";
            
            /*** Configurações Básicas ***/
                /* Geração de Código Único para cadastro*/
                $database_codigo       = $_GET[$pagina_singular];

            /*** Consulta Padrão/Personalizadas ***/
                $database_consulta = "SELECT * FROM ".$primary_database_page_tabela." ";
                    /***
                     *  Insert Complements Busca Principal  
                    ***/
                    // $database_consulta .= " ";
                $database_consulta .= " WHERE ".$primary_database_page_collumn_id." = ".$database_codigo."";
                $database_consulta = newsql($database_consulta)[0];
?>

<?php require_once("includes/header.php"); ?>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <span class="text-semibold">
                        <a href="index.php">Dashboard</a>
                    </span> 
                    <i class="icon-arrow-right6"></i> 
                    <strong><?php echo $pagina_titulo; ?></strong><br>
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo $primary_database_page_backlink; ?>" class="btn btn-link btn-float has-text">
                        <i class="icon-database-arrow text-default"></i> 
                        <span>Consultar <?php echo maiuscula($pagina_text_plural); ?></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li>
                    <a href="index.php">
                        <i class="icon-home2 position-left"></i> 
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $primary_database_page_backlink; ?>">
                        <i class="icon-database-arrow position-left"></i> 
                        <span>Consultar <?php echo maiuscula($pagina_text_plural); ?></span>
                    </a>
                </li>
                <li>
                    <i class="<?php echo $pagina_icone; ?>"></i> 
                    <span><?php echo $pagina_titulo; ?></span>
                </li>
            </ul>
            <ul class="breadcrumb-elements">
                <!-- Modal Ajuda -->
                <?php require_once("includes/ajuda.php"); ?>
            </ul>
        </div>
    </div>
    <div class="content">

        <div class="row">
            <div calss="col-lg-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"><i class="icon-database-add"></i> <strong>Informações para Cadastro</strong></h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a id="clean_form" href="header('Refresh:0');" data-action="reload"></a></li>
                            </ul>
                        </div>
                        <p class="content-group" style="margin: 0!important">Siga as etapas e passos até o final.</p>
                    </div> 
                    <div class="panel-body"> 
                        <form id="FormDefault" class="stepy-default" method="post" onsubmit="return false">
                            <input 
                                type="hidden" 
                                name="form_page" 
                                value="<?php echo $path_pagina; ?>"
                            >
                            <input 
                                type="hidden" 
                                name="form_table" 
                                value="<?php echo $primary_database_page_tabela; ?>"
                            >
                            <input 
                                type="hidden" 
                                name="form_id" 
                                value="<?php echo $database_codigo; ?>"
                            >
                            <input 
                                type="hidden" 
                                name="form_prefix" 
                                value="<?php echo $primary_database_page_collumn_prefix; ?>"
                            >
                            
                            
                            <fieldset title="1">
                                <legend class="text-semibold">Campos Padrões</legend>
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Configurações Padrões</legend>
                                                    
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Visibilidade</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-database4"></i></span>
                                                                        <select 
                                                                            id="visibilidade<?php echo $database_codigo; ?>"
                                                                            class="select insert_field" 
                                                                            type="" 
                                                                            name="visibilidade<?php echo $database_codigo; ?>"
                                                                            title="Indique a visibilidade"
                                                                            placeholder="Selecione o tipo de visibilidade"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_VISIBILIDADE" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Selecione o tipo de visibilidade" 
                                                                            required="true"
                                                                        >
                                                                            <option value="0">Selecionar...</option> 
                                                                            <option <?php if($database_consulta["PUB_VISIBILIDADE"] == 'true') echo 'selected'; ?> value="true">Ativo</option> 
                                                                            <option <?php if($database_consulta["PUB_VISIBILIDADE"] == 'false') echo 'selected'; ?> value="false">Inativo</option> 
                                                                        </select>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Categoria</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-database4"></i></span>
                                                                        <select 
                                                                            id="categoria<?php echo $database_codigo; ?>"
                                                                            class="select insert_field" 
                                                                            type="" 
                                                                            name="categoria<?php echo $database_codigo; ?>"
                                                                            title="Indique a categoria"
                                                                            placeholder="Selecione a categoria"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_CATEGORIA" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-popup="tooltip"
                                                                            data-placeholder="Selecione a categoria" 
                                                                            required="true"
                                                                            value=""
                                                                        >
                                                                            <option data-popup="tooltip" selected title="Selecionar" value="">Selecione...</option> 
                                                                            <?php 
                                                                                $sql_categorias = newsql("SELECT * FROM tbl_publicacoes_categorias WHERE CAT_VISIBILIDADE = 'true' ORDER BY CAT_TITULO ASC");
                                                                                foreach ($sql_categorias as $key => $value) {
                                                                                    $categoria_codigo = $value['CAT_CODIGO'];
                                                                                    $categoria_titulo = $value['CAT_TITULO'];
                                                                            ?>
                                                                            <option <?php if($categoria_codigo == $database_consulta['PUB_CATEGORIA']) echo 'selected'; ?> data-popup="tooltip" title="<?php echo $categoria_titulo; ?>" value="<?php echo $categoria_codigo; ?>"><?php echo $categoria_titulo; ?></option> 
                                                                            <?php } ?>
                                                                        </select>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Slug (Endereço da Página)</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><?php echo SITE . 'publicacao/'; ?></span>
                                                                        <input 
                                                                            id="slug<?php echo $database_codigo; ?>"
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="slug<?php echo $database_codigo; ?>"
                                                                            title="Informar o slug"
                                                                            placeholder="Inserir o slug"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_SLUG" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o slug" 
                                                                            data-popup="tooltip"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['PUB_SLUG']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>    
                                                    
                                                    
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Imagem:</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="row">
                                                                        <div class="col-lg-6">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon"><i class="icon-image2"></i></span>
                                                                                <input 
                                                                                    id="file_upload<?php echo $database_codigo; ?>"
                                                                                    class="file-styled inputs file_upload"
                                                                                    style="cursor: pointer;" 
                                                                                    type="file" 
                                                                                    name="file_upload<?php echo $database_codigo; ?>" 
                                                                                    data-codigo="<?php echo $database_codigo; ?>" 
                                                                                    data-upload="<?php echo $primary_database_page_upload; ?>" 
                                                                                    data-show-preview="false" 
                                                                                    title="Indique a Imagem"
                                                                                    placeholder="Selecione a Imagem"
                                                                                    data-trigger="focus" 
                                                                                    data-placeholder="Selecione a imagem" 
                                                                                >
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon"><i class="icon-highlight"></i></span>
                                                                                <input 
                                                                                    id=""
                                                                                    class="form-control insert_field" 
                                                                                    type="varchar" 
                                                                                    name="legenda<?php echo $database_codigo; ?>" 
                                                                                    title="Insira a legenda" 
                                                                                    placeholder="Informar a legenda"
                                                                                    data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                                    data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                                    data-field="<?php echo $primary_database_page_collumn_prefix; ?>_LEGENDA" 
                                                                                    data-id="<?php echo $database_codigo; ?>" 
                                                                                    data-type="varchar" 
                                                                                    data-trigger="focus" 
                                                                                    data-placeholder="Insira a legenda"
                                                                                    required="true"
                                                                                    value="<?php echo $database_consulta['PUB_LEGENDA']; ?>"
                                                                                >
                                                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div id="action_image" style="margin-top: 10px;">
                                                                                <!-- Imagem Ajax -->
                                                                                <?php 
                                                                                    $imagens_unico = get_uploads($primary_database_page_upload . "_unico", $database_codigo, "");
                                                                                    $imagem_unico = capture_uploads($imagens_unico); 
                                                                                ?>
                                                                                <?php if(!empty($imagens_unico)){ ?>
                                                                                    <span id="imagem_check"><a target="_blank" href="<?php echo $imagem_unico; ?>">Visualizar Imagem (<?php echo $imagens_unico[0]['UP_NOME']; ?>)</a></span>
                                                                                    <span style="cursor: pointer; float: right;" onclick="removeImage(<?php echo $imagens_unico[0]['UP_CODIGO']; ?>)" class="text-danger"><i class="icon-folder-remove"></i> Remover Imagem</span>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Título</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-cube4"></i></span>
                                                                        <input 
                                                                            id=""
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="titulo<?php echo $database_codigo; ?>" 
                                                                            title="Insira o título obrigatório" 
                                                                            placeholder="Informar o título"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_TITULO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Insira o título obrigatório"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['PUB_TITULO']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Subtítulo</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-cube4"></i></span>
                                                                        <input 
                                                                            id=""
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="subtitulo<?php echo $database_codigo; ?>" 
                                                                            title="Insira o subtítulo" 
                                                                            placeholder="Informar o título"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_SUBTITULO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Informar o subtítulo" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['PUB_SUBTITULO']; ?>"
                                                                        >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Descrição/Texto</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <textarea 
                                                                            id="summernote_update<?php echo $database_codigo; ?>" 
                                                                            class="summernote_update insert_field" 
                                                                            type="" 
                                                                            name="descricao<?php echo $database_codigo; ?>" 
                                                                            title="Informar a descrição"
                                                                            placeholder="Inserir a descrição"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_DESCRICAO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="longtext" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir a descrição"
                                                                            required="true"
                                                                        ><?php echo $database_consulta['PUB_DESCRICAO']; ?></textarea>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset title="2">
                                <legend class="text-semibold">Anexos</legend>
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Galeria de Anexos</legend>
                                                    <div class="alert alert-info alert-styled-left">
                                                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                                        <span class="text-semibold">Aviso!</span> <br>
                                                        <strong>Uploads:</strong> Por favor, não insira documentos, imagens ou qualquer outro tipo de upload com o mesmo nome dos arquivos já anexados!
                                                    </div> 
                                                    <div class="panel panel-flat">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <fieldset>
                                                                        <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Upload de Anexos</legend>
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="<?php echo $primary_database_page_upload; ?>-uploads"></div>
                                                                                <div class="file-uploader"><p>Atualize seu browser para ter acesso ao upload de arquivos! </p></div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="row">
                                                                                    <table class='table table-bordered table-lg'>
                                                                                        <thead>
                                                                                            <tr class='active'>
                                                                                                <th colspan='3'>Anexados</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody class="arquivos-box">

                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            
                                                                            </div>
                                                                        </div>
                                                                    </fieldset>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset title="3">
                                <legend class="text-semibold">Tags Personalizadas</legend>
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Configuração de Tags</legend>
                                                    <div class="panel panel-flat">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <fieldset>
                                                                        <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Tags</legend>
                                                                    
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label>Título da Tag:</label>
                                                                                    <div class="input-group">
                                                                                        <input 
                                                                                            id="titulo_item<?php echo $database_codigo; ?>" 
                                                                                            class="form-control" 
                                                                                            type="varchar" 
                                                                                            name="titulo_item<?php echo $database_codigo; ?>" 
                                                                                            title="Informar a tag" 
                                                                                            placeholder="Inserir a tag"
                                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>"  
                                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                                            data-type="varchar"
                                                                                            data-trigger="focus" 
                                                                                            data-placeholder="Inserir a tag"
                                                                                            required="true"
                                                                                            value=""
                                                                                        >
                                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <a id="add_item" style="width: 100%; text-align: center;" href="javascript:void(0);" class="btn btn-info">Adicionar Tag <i class="icon-hour-glass2 position-right"></i></a>
                                                                        
                                                                    </fieldset>
                                                                    <legend style="margin-top: 15px;" class="text-semibold"><i class="icon-hour-glass2 position-left"></i> Tags Adicionados</legend>
                                                                    <div calss="col-lg-12">
                                                                        <table class="table table-bordered table-hover " id="table_itens">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Título</th>
                                                                                    <th class="text-center">Ações</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody id="itens">
                                                                                <?php 
                                                                                    $sql_itens = newsql("SELECT * FROM tbl_publicacoes_tags WHERE ITEM_REGISTRO = '{$database_codigo}'");
                                                                                    foreach ($sql_itens as $key => $value) {
                                                                                        $item_codigo        = $value['ITEM_CODIGO'];
                                                                                        $item_titulo        = $value['ITEM_TITULO'];
                                                                                ?>
                                                                                <tr class='line<?php echo $item_codigo; ?>'>
                                                                                    <td><?php echo $item_codigo; ?></td>
                                                                                    <td><?php echo $item_titulo; ?></td>
                                                                                    <td class='text-center'>
                                                                                        <a onclick="confirmDelItem(<?php echo $item_codigo; ?>)" class='remove_button' style='color:red;'><i class='icon-folder-remove'></i></a>
                                                                                    </td>
                                                                                </tr>

                                                                                <?php } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset title="4">
                                <legend class="text-semibold">SEO</legend>
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Campos Importantes</legend>
                                                    
                                                        
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>[SEO] - Palavras Chave <small>(separadas por virgula)</small></strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-cube4"></i></span>
                                                                        <input 
                                                                            id=""
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="seo_palavras<?php echo $database_codigo; ?>" 
                                                                            title="Insira palavras chave para SEo" 
                                                                            placeholder="Informar o palavras chave para SEO"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_SEO_PALAVRAS" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Informar palavras chave para SEO" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['PUB_SEO_PALAVRAS']; ?>"
                                                                        >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>[SEO] - Título </strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-cube4"></i></span>
                                                                        <input 
                                                                            id=""
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="seo_titulo<?php echo $database_codigo; ?>" 
                                                                            title="Insira o título para SEO" 
                                                                            placeholder="Informar o título para SEO"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_SEO_TITULO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Informar o título para SEO" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['PUB_SEO_TITULO']; ?>"
                                                                        >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>[SEO] - Descrição</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-file-text2"></i></span>
                                                                        <textarea 
                                                                            id=""
                                                                            class="form-control insert_field" 
                                                                            type=""
                                                                            name="seo_descricao<?php echo $database_codigo; ?>" 
                                                                            title="Informar a descrição para SEO"
                                                                            placeholder="Inserir a descrição para SEO"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_SEO_DESCRICAO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="text" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir a descrição para SEO"
                                                                            required="true"
                                                                        ><?php echo $database_consulta['PUB_SEO_DESCRICAO']; ?></textarea>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    

                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            
                            
                            <button type="button" class="btn btn-default stepy-finish">Atualização Automática <i class="fa fa-save position-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <script type="text/javascript">
            var ADMIN                   = "<?php echo ADMIN; ?>";
            var database_page_codigo    = "<?php echo $database_codigo; ?>";
            var database_page_upload    = "<?php echo $primary_database_page_upload; ?>";
            var database_page_usuario   = "<?php echo $cadastro_usuario; ?>";
            var database_page_tabela    = "<?php echo $primary_database_page_tabela; ?>";
        </script>
        <?php /* Incluir Upload Unico */ require_once("includes/upload-unico.php"); ?>
        <?php /* Incluir Uploads de Anexos Gerais */ require_once("includes/upload.php"); ?>
        <?php /* Incluir Campo Text Personalizado */ require_once("includes/field-text.php"); ?>
        <?php /* Incluir Notificação Padrão de Anexos Gerais */ require_once("includes/notificacao-padrao.php"); ?>
        <?php /* Incluir Credits */ require_once("includes/copyright.php"); ?>

    </div>
</div>

<script type="text/javascript">
$('#add_item').click(function () {
    var item_codigo = '<?php echo $database_codigo; ?>';
    var item_titulo = $('#titulo_item' + item_codigo).val();
    $.ajax({
        type: 'POST',
        url: 'ajax/ajax.searchPublicacoesitens.php',
        dataType: 'JSON',
        data: {'item_codigo':item_codigo, 'item_titulo':item_titulo },
        beforeSend: function() {},
        success: function (data) {
            var resposta            = data.resposta;
            var search_titulo       = data.search_titulo;
            var last_id             = data.last_id;

            if(resposta == 'true'){
                var line = "<tr class='line"+last_id+"'>"+
                    "<td>"+last_id+"</td>"+
                    "<td>"+search_titulo+"</td>"+
                    "<td class='text-center'>"+
                        "<a onclick='confirmDelItem("+last_id+")' class='remove_button' style='color:red;'><i class='icon-folder-remove'></i></a>"+
                    "</td>"+
                "</tr>";
                $('#titulo_item' +item_codigo).val("");
                $("#itens").append(line);

                new PNotify({
                    text: "Adicionado com sucesso.",
                    addclass: 'bg-success',
                    type: 'warning',
                    icon: 'icon-database',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    opacity: .9,
                    width: "250px"
                });

                setTimeout(function(){ PNotify.removeAll(); }, 3000);

            } else {
                new PNotify({
                    text: "Erro ao procurar informações, tente novamente...",
                    addclass: 'bg-warning',
                    type: 'warning',
                    icon: 'icon-checkmark3',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    opacity: .9,
                    width: "250px"
                });
                setTimeout(function(){ PNotify.removeAll(); }, 3000);
            }
        }
    });
});


function confirmDelItem(codigo){

    $.ajax({
        type: 'POST',
        url: 'ajax/ajax.removerPublicacoesitens.php',
        dataType: 'JSON',
        data: {'item':codigo },
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento...",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        }, 
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta         = data.resposta;
            
            if(resposta == 'true'){
                new PNotify({
                    text: "Removido com sucesso.",
                    addclass: 'bg-success',
                    type: 'warning',
                    icon: 'icon-database',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    opacity: .9,
                    width: "250px"
                });
                $(".line"+codigo).remove();
                setTimeout(function(){ PNotify.removeAll(); }, 3000);

            } else {
                new PNotify({
                    text: "Erro ao remover item, tente novamente mais tarde...",
                    addclass: 'bg-warning',
                    type: 'warning',
                    icon: 'icon-checkmark3',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    opacity: .9,
                    width: "250px"
                });
                setTimeout(function(){ PNotify.removeAll(); }, 3000);
            }
        }
    });
}

// Clean Data Form
$('#FormDefault')[0].reset();
$('#clean_form').on('click', function () { $('#FormDefault')[0].reset(); });

$(".stepy-default").stepy({
    next: function(index) { 
        if(index == 2){
            getUploads('<?php echo $primary_database_page_upload; ?>', '<?php echo $database_codigo; ?>', '<?php echo $primary_database_page_upload; ?>-uploads', '.arquivos-box');
        }
    },
    back: function(index) { 
        if(index == 2){
            getUploads('<?php echo $primary_database_page_upload; ?>', '<?php echo $database_codigo; ?>', '<?php echo $primary_database_page_upload; ?>-uploads', '.arquivos-box');
        }
    },
    finish: function() {
        return false;
    }
});



</script>
<?php 
    /* Registrar Informações Individualmente */ require_once("includes/form-individual.php");
    /* Incluir Footer */ require_once("includes/footer.php");
    } }
?>