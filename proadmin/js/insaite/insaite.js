// $(document).ready(function() {
//     $('.summernote1').summernote({
//         popover: {
//             image: [
//                 ['custom', ['imageAttributes']],
//                 ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
//                 ['float', ['floatLeft', 'floatRight', 'floatNone']],
//                 ['remove', ['removeMedia']]
//             ],
//         },
//         lang: 'pt-BR', // Change to your chosen language
//         imageAttributes:{
//             icon:'<i class="note-icon-pencil"/>',
//             removeEmpty:false, // true = remove attributes | false = leave empty if present
//             disableUpload: false // true = don't display Upload Options | Display Upload Options
//         }
//     });

// });

// function deleteAll(tabela, campo){
//     $.blockUI({ 
//         message: '<i class="icon-spinner4 spinner"></i>',
//         overlayCSS: {
//             backgroundColor: '#1b2024',
//             opacity: 0.8,
//             cursor: 'wait'
//         },
//         css: {
//             border: 0,
//             color: '#fff',
//             padding: 0,
//             backgroundColor: 'transparent'
//         }
//     });
//     var notice = new PNotify({
//         title: 'Precisamos de sua confirmação',
//         text: '<p>Você tem certeza que deseja deletar todos estes itens (logs não serão salvos)? </p>',
//         hide: false,
//         type: 'warning',
//         confirm: {
//             confirm: true,
//             buttons: [
//                 {
//                     text: 'Sim, desejo deletar',
//                     addClass: 'btn btn-sm btn-danger'
//                 },
//                 {
//                     text: 'Melhor não',
//                     addClass: 'btn btn-sm btn-link'
//                 }
//             ]
//         },
//         buttons: {
//             closer: false,
//             sticker: false
//         },
//         history: {
//             history: false
//         }
//     })

//     // On confirm
//     notice.get().on('pnotify.confirm', function() {
//         $.unblockUI();
//         PNotify.removeAll();
//         var array_delete = [];
//         $(".input_delete").each(function(){
//             if ($(this).is(':checked')) {
//                 array_delete.push($(this).val());
//             }
//         }); 
        
//         var formData = {
//             'tabela'        : tabela,
//             'campo'         : campo,
//             'array_delete'  : array_delete,
//         };

//         $.ajax({
//             type        : 'POST', 
//             url         : 'ajax/ajax.removerProRegistros.php',
//             data        : formData,
//             dataType    : 'json', 
//             beforeSend: function() {
//                 $.blockUI({ 
//                     message: '<i class="icon-spinner4 spinner"></i>',
//                     overlayCSS: {
//                         backgroundColor: '#1b2024',
//                         opacity: 0.8,
//                         cursor: 'wait'
//                     },
//                     css: {
//                         border: 0,
//                         color: '#fff',
//                         padding: 0,
//                         backgroundColor: 'transparent'
//                     }
//                 });
//                 new PNotify({
//                     text: "Aguarde um momento",
//                     addclass: 'bg-primary',
//                     type: 'info',
//                     icon: 'icon-spinner4 spinner',
//                     hide: false,
//                     buttons: {
//                         closer: false,
//                         sticker: false
//                     },
//                     opacity: .9,
//                     width: "250px"
//                 });
//             },
//             success: function (data) {
//                 $.unblockUI();
//                 PNotify.removeAll();
//                 var resposta = data.resposta;
//                 var count = data.count;
//                 if(resposta == 'true'){
//                     new PNotify({   
//                         text: count + " itens deletados com sucesso!",
//                         addclass: 'bg-success',
//                         type: 'success',
//                         icon: 'icon-checkmark3',
//                         hide: true,
//                         buttons: {
//                             closer: true,
//                             sticker: false
//                         },
//                         opacity: 1,
//                         width: "200px"
//                     });
//                     location.reload();
//                 }           
//             }
//         })
//     })

//     // On cancel
//     notice.get().on('pnotify.cancel', function() {
//         $.unblockUI();
//         PNotify.removeAll();
//     });
// }


// function getUploads(type, cod, wrapElm, box){

//     $.blockUI({ 
//         message: '<i class="icon-spinner4 spinner"></i>',
//         timeout: 2000,
//         overlayCSS: {
//             backgroundColor: '#1b2024',
//             opacity: 0.8,
//             cursor: 'wait'
//         },
//         css: {
//             border: 0,
//             color: '#fff',
//             padding: 0,
//             backgroundColor: 'transparent'
//         }
//     });

//     $.ajax({
//         type        : 'POST', 
//         url         : 'ajax/ajax.searchProUploads.php',
//         data        : {"type" :type,"id" :cod},
//         dataType    : 'json', 
//         success: function (data) {
//             $(box).empty();
//             if(data.uploads.length > 0){
//                 for (var i in data.uploads){
//                     if(data.uploads[i].UP_DESTAQUE == 'true'){
//                         var destaque = 'Destaque';
//                     } else {
//                         var destaque = '';
//                     }
//                     if(data.uploads[i].UP_TIPO == 0){
//                         var titulo_tipo = '';
//                         var codigo_tipo = '';
//                     } else {
//                         var titulo_tipo = data.uploads[i].TIPO_TITULO;
//                         var codigo_tipo = '(' + data.uploads[i].TIPO_CODIGO + ')';
//                     }
//                     var up = "<tr id='upreg"+data.uploads[i].UP_CODIGO+"'>"+
//                                 "<td class='col-md-4 col-sm-3'><a target='_blank' href='"+data.uploads[i].UP_CAMINHO+"'>" + data.uploads[i].UP_NOME + "</a></td>"+
//                                 "<td class='col-md-3 col-sm-2'>"+
//                                     "<ul class='icons-list'>"+
//                                         "<li><a class='alterar_upload' data-tipo_proj='" + data.uploads[i].UP_TIPO + "' data-destaque='" + data.uploads[i].UP_DESTAQUE + "' data-codigo='" + data.uploads[i].UP_CODIGO + "' data-nome='" + data.uploads[i].UP_NOME + "' data-titulo='" + data.uploads[i].UP_TITULO + "' data-descricao='" + data.uploads[i].UP_DESCRICAO + "'data-tipo='" + type.trim() + "' href='javascript:void(0);'><i class='icon-cog7'></i></a></li>"+
//                                         "<li><a class='delaction' href='javascript:void(0);' data-tipo=" + type.trim() + " data-codigo='" + data.uploads[i].UP_CODIGO + "'><i class='icon-trash'></i></a></li>"+
//                                     "</ul>"+
//                                 "</td>"+
//                                 "<td><span style='margin-bottom: 5px;' class='label bg-success'>"+destaque+"</span>&nbsp;&nbsp;<span style='margin-bottom: 5px;' class='label bg-teal'>" + titulo_tipo + "" + codigo_tipo + "</span>&nbsp;&nbsp;" + data.uploads[i].UP_TITULO + "</td>"+
//                             "</tr>";
//                             console.log(up);
//                     $(box).append(up);
//                 }
//             }
//         }
//     });
// }

// function alteraUpload(id, type, elm, idReg, box){
//     var titulo      = $('#altera_titulo' + id).val();
//     var legenda     = $('#altera_legenda' + id).val();
//     var destaque    = $('#altera_destaque' + id).val();
//     $.ajax({
//         type        : 'POST', 
//         url         : 'ajax/ajax.updateProUploads.php',
//         data        : {"id" : id, "titulo" : titulo, "legenda" : legenda, "destaque" : destaque},
//         dataType    : 'json', 
//         beforeSend: function() {
//             $.blockUI({ 
//                 message: '<i class="icon-spinner4 spinner"></i>',
//                 timeout: 8000,
//                 overlayCSS: {
//                     backgroundColor: '#1b2024',
//                     opacity: 0.8,
//                     cursor: 'wait'
//                 },
//                 css: {
//                     border: 0,
//                     color: '#fff',
//                     padding: 0,
//                     backgroundColor: 'transparent'
//                 }
//             });
//         },
//         success: function (data) {
//             $.unblockUI();
//             PNotify.removeAll();
//             $(".modal-backdrop").remove();
//             $('body').removeClass( "modal-open" );
//             resposta = data.resposta;
//             if(resposta == 'true'){
//                 new PNotify({
//                     text: "Registro Alterado!",
//                     addclass: 'bg-success',
//                     type: 'success',
//                     icon: 'icon-checkmark3',
//                     hide: true,
//                     buttons: {
//                         closer: true,
//                         sticker: false
//                     },
//                     opacity: .9,
//                     width: "250px"
//                 });

//                 setTimeout(function(){
//                     getUploads(type, idReg, elm, box);
//                 }, 200);
//             } else {
//                 new PNotify({
//                     text: "Erro ao Alterar!",
//                     addclass: 'bg-warning',
//                     type: 'warning',
//                     icon: 'icon-checkmark3',
//                     hide: true,
//                     buttons: {
//                         closer: true,
//                         sticker: false
//                     },
//                     opacity: .9,
//                     width: "250px"
//                 });                
//             }
//         }
//     });
// };