<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../classes/Conexao.class.php');
require_once('../funcoes/funcoes.php');
require_once('../funcoes/phpmailer/class.phpmailer.php');
define('HOST', 'https://' . $_SERVER['HTTP_HOST']); 
define('SITEROOT', HOST . '/'); 

$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();

$projeto 		= intval($_POST['projeto']);
$item 		= intval($_POST['item']);
$titulo 		= mysqli_real_escape_string($conexao, $_POST['titulo']);
$documento 		= mysqli_real_escape_string($conexao, $_POST['documento']);
$grupo 		= mysqli_real_escape_string($conexao, $_POST['grupo']);

$documentos = get_uploads_documentos('documento_default',$item,$projeto);
$up_nome = $documentos[0]['UP_NOME'];

$capture = capture_uploads_documentos($documentos);

if(!empty($projeto) && !empty($titulo) && !empty($documento)){
	$sql_documentos = newsql("SELECT * FROM tbl_projetos_documentos WHERE DOC_CODIGO = '{$documento}'");
	$titulo_documento = $sql_documentos[0]['DOC_TITULO'];
	$sql_grupos = newsql("SELECT * FROM tbl_usuarios_grupos WHERE GRUP_CODIGO = '{$grupo}'");
	if(!empty($sql_grupos)){
		$grupo_titulo = $sql_grupos[0]['GRUP_TITULO'] . " + Admin";
	} else {
		$grupo_titulo = "Somente Administradores";
	}

	$resp = array('resposta' => 'true', 'documento_titulo' => $titulo_documento, 'nome' => $up_nome, 'link' => $capture, 'grupo' => $grupo_titulo);
	
} else { $resp = array('resposta' => 'dados'); }

echo json_encode($resp);
