<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../classes/Conexao.class.php');
require_once('../funcoes/funcoes.php');
require_once('../funcoes/phpmailer/class.phpmailer.php');

$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();

$database_form_page    	= mysqli_real_escape_string($conexao, $_POST['form_page']);
$database_form_table   	= mysqli_real_escape_string($conexao, $_POST['form_table']);
$database_form_id    	= mysqli_real_escape_string($conexao, $_POST['form_id']);
$database_form_prefix 	= mysqli_real_escape_string($conexao, $_POST['form_prefix']);

if(!empty($database_form_id) && !empty($database_form_page)){
	if(newdelete("config_usuarios_permissoes","WHERE USUPERM_USUARIO = '{$database_form_id}'")){
		foreach ($_POST['permissao'] as $key => $value) {
		    if($value == 'on'){
		    	newinsert("config_usuarios_permissoes","(USUPERM_USUARIO, USUPERM_MODULO_PERMISSAO) VALUES ('{$database_form_id}', '{$key}')");	    	
		    }
		}

		insert_logs($path_pagina,"");
		$resp = array('resposta' => 'true');
	
	} else { $resp = array('resposta' => 'false'); }
} else { $resp = array('resposta' => 'dados'); }

echo json_encode($resp);
