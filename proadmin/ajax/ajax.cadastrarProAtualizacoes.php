<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../classes/Conexao.class.php');
require_once('../funcoes/funcoes.php');
require_once('../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];

$database_form_page    	= mysqli_real_escape_string($conexao, $_POST['form_page']);
$database_form_table   	= mysqli_real_escape_string($conexao, $_POST['form_table']);
$database_form_id    	= mysqli_real_escape_string($conexao, $_POST['form_id']);
$database_form_prefix 	= mysqli_real_escape_string($conexao, $_POST['form_prefix']);

if(!empty($database_form_page) && !empty($database_form_table) && !empty($database_form_id) && !empty($database_form_prefix)){
	
	// Campos de Validação
	$visibilidade 	= mysqli_real_escape_string($conexao, $_POST['visibilidade' . $database_form_id]);
	$tipo 			= mysqli_real_escape_string($conexao, $_POST['tipo' . $database_form_id]);
	$versao 		= mysqli_real_escape_string($conexao, $_POST['versao' . $database_form_id]);
	$descricao 		= mysqli_real_escape_string($conexao, $_POST['descricao_text' . $database_form_id]);
	
	if(!empty($visibilidade) && !empty($descricao) && !empty($tipo) && !empty($versao)){
		if(newupdate($database_form_table, "".$database_form_prefix."_CHECK = 'true' WHERE ".$database_form_prefix."_CODIGO = '{$database_form_id}'")){
			
			insert_logs($database_form_page,"");
			
			$resp = array('resposta' => 'true');
		} else { $resp = array('resposta' => 'false'); }
	} else { $resp = array('resposta' => 'dados'); }
} else { $resp = array('resposta' => 'dados'); }
echo json_encode($resp);