<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../../classes/Conexao.class.php');
require_once('../../funcoes/funcoes.php');
require_once('../../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];

$codigo				= mysqli_real_escape_string($conexao, $_POST['in_codigo']);
$permissao			= mysqli_real_escape_string($conexao, $_POST['in_permissao']);
$titulo     		= mysqli_real_escape_string($conexao, $_POST['in_titulo']);
$page 	    		= mysqli_real_escape_string($conexao, $_POST['in_page']);
$arquivo     		= mysqli_real_escape_string($conexao, $_POST['in_arquivo']);
$icone 	    		= mysqli_real_escape_string($conexao, $_POST['in_icone']);
$singular    		= mysqli_real_escape_string($conexao, $_POST['in_singular']);
$plural     		= mysqli_real_escape_string($conexao, $_POST['in_plural']);

// print_($_POST);
// exit();
$modulo_titulo = newsql("SELECT * FROM config_modulos WHERE MOD_CODIGO = '{$codigo}'")[0]['MOD_TITULO'];

if(!empty($codigo) && !empty($permissao) && !empty($titulo)){
	$arquivo = $arquivo . '.php';
	if(newinsert("config_modulos_permissoes","(MODPERM_MODULO, MODPERM_PERMISSAO, MODPERM_TITULO) VALUES ('{$codigo}', '{$permissao}', '{$titulo}')")){
		$last_id = mysqli_insert_id($conexao);
		
		$format_singular 	= Remove_caracter($modulo_titulo);
		$format_plural 		= Remove_caracter($modulo_titulo) . 's';

		if(newinsert("config_paginas","(PAG_TITULO, PAG_ARQUIVO, PAG_MODULO_PERMISSAO, PAG_TEXT_SINGULAR, PAG_TEXT_PLURAL, PAG_ICONE, PAG_SINGULAR, PAG_PLURAL, PAG_MODULO) VALUES ('{$titulo}', '{$arquivo}', '{$last_id}', '{$singular}', '{$plural}', '{$icone}', '{$format_singular}', '{$format_plural}', '{$codigo}')")){

			$resp = array('resposta' => 'true');
		} else { $resp = array('resposta' => 'error'); }
	} else { $resp = array('resposta' => 'false'); }
} else { $resp = array('resposta' => 'dados'); }
echo json_encode($resp);
