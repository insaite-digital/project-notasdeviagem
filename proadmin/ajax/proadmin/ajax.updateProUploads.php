<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../../classes/Conexao.class.php');
require_once('../../funcoes/funcoes.php');
require_once('../../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];

$codigo    = mysqli_real_escape_string($conexao, $_POST['upload_codigo']);
$titulo		    	= mysqli_real_escape_string($conexao, $_POST['upload_titulo']);
$descricao         		= mysqli_real_escape_string($conexao, $_POST['upload_descricao']);
$tipo         = mysqli_real_escape_string($conexao, $_POST['upload_tipo_select']);
$destaque         		= mysqli_real_escape_string($conexao, $_POST['upload_destaque_select']);
// print_($_POST);
// exit();
if(!empty($codigo)){
	if(newupdate("config_uploads", "UP_TITULO = '{$titulo}', UP_DESCRICAO = '{$descricao}', UP_TIPO = '{$tipo}', UP_DESTAQUE = '{$destaque}' WHERE UP_CODIGO = '{$codigo}'")){
		
		$resp = array('resposta' => 'true');
	
	} else { $resp = array('resposta' => 'false'); }
} else { $resp = array('resposta' => 'dados'); }
echo json_encode($resp);
