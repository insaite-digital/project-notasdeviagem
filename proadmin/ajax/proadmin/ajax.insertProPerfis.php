<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../../classes/Conexao.class.php');
require_once('../../funcoes/funcoes.php');
require_once('../../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];


$path_pagina    	= "cadastrarProPerfis.php";
// $path_pagina    	= mysqli_real_escape_string($conexao, $_POST['pagina']);
$tabela         	= mysqli_real_escape_string($conexao, $_POST['tabela']);
$codigo		    	= mysqli_real_escape_string($conexao, $_POST['codigo']);
$titulo     		= mysqli_real_escape_string($conexao, $_POST['in_titulo']);
$descricao			= mysqli_real_escape_string($conexao, $_POST['in_descricao']);

if(!empty($titulo)){
	if(newinsert("config_perfis","(PER_TITULO, PER_DESCRICAO, PER_MASTER) VALUES ('{$titulo}', '{$descricao}', 'false')")){
		// Logs
        insert_logs($path_pagina);
		$resp = array('resposta' => 'true');
	} else { $resp = array('resposta' => 'false'); }
} else { $resp = array('resposta' => 'dados'); }
echo json_encode($resp);
