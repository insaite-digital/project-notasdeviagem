<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../../classes/Conexao.class.php');
require_once('../../funcoes/funcoes.php');
require_once('../../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];

$code 		        = $_GET['code'];
$path_pagina    	= mysqli_real_escape_string($conexao, $_POST['upsenha_pagina' . $code]);
$tabela         	= mysqli_real_escape_string($conexao, $_POST['upsenha_tabela' . $code]);
$codigo		    	= mysqli_real_escape_string($conexao, $_POST['upsenha_codigo' . $code]);
$senha         		= mysqli_real_escape_string($conexao, $_POST['upsenha_senha' . $code]);

$c = '94';
$salt = 'DiGiTalInsaiTe';
$hash = md5($salt.$senha.$c);

$forca = senhaValida($senha);

if($forca == 1){
	if(!empty($senha) && !empty($codigo)){
		if(newupdate("config_usuarios", "USU_SENHA = '{$hash}' WHERE USU_CODIGO = '{$codigo}'")){
			// Logs
	        insert_logs($path_pagina);
			
			$resp = array('resposta' => 'true');
		
		} else { $resp = array('resposta' => 'false'); }
	} else { $resp = array('resposta' => 'dados'); }
} else { $resp = array('resposta' => 'senha'); }
echo json_encode($resp);
