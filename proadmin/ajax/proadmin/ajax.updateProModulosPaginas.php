<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../../classes/Conexao.class.php');
require_once('../../funcoes/funcoes.php');
require_once('../../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];

// $path_pagina    = mysqli_real_escape_string($conexao, $_POST['pagina']);
// $tabela         = mysqli_real_escape_string($conexao, $_POST['tabela']);
$code			= mysqli_real_escape_string($conexao, $_GET['code']);
$in_codigo		= mysqli_real_escape_string($conexao, $_POST['in_codigo' . $code]);
$in_modperm     = mysqli_real_escape_string($conexao, $_POST['in_modperm' . $code]);
$in_icone   	= mysqli_real_escape_string($conexao, $_POST['in_icone' . $code]);
$in_page     	= mysqli_real_escape_string($conexao, $_POST['in_page' . $code]);
$in_singular    = mysqli_real_escape_string($conexao, $_POST['in_singular' . $code]);
$in_plural      = mysqli_real_escape_string($conexao, $_POST['in_plural' . $code]);
$in_arquivo     = mysqli_real_escape_string($conexao, $_POST['in_arquivo' . $code]);
$page         	= mysqli_real_escape_string($conexao, $_POST['page' . $code]);

if(!empty($code) && !empty($page)){
	// $page = $page . '.php';
	if(newupdate("config_paginas", "PAG_TITULO = '{$in_page}', PAG_ARQUIVO = '{$in_arquivo}', PAG_MODULO_PERMISSAO = '{$in_modperm}', PAG_TEXT_SINGULAR = '{$in_singular}', PAG_TEXT_PLURAL = '{$in_plural}', PAG_ICONE = '{$in_icone}', PAG_MODULO = '{$in_codigo}' WHERE PAG_CODIGO = '{$page}'")){
		// Logs
        // insert_logs($path_pagina);
		
		$resp = array('resposta' => 'true');
	
	} else { $resp = array('resposta' => 'false'); }
} else { $resp = array('resposta' => 'dados'); }
echo json_encode($resp);
