<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../../classes/Conexao.class.php');
require_once('../../funcoes/funcoes.php');
require_once('../../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];

$code 		        = $_GET['code'];
$path_pagina    	= mysqli_real_escape_string($conexao, $_POST['upnome_pagina' . $code]);
$tabela         	= mysqli_real_escape_string($conexao, $_POST['upnome_tabela' . $code]);
$codigo		    	= mysqli_real_escape_string($conexao, $_POST['upnome_codigo' . $code]);
$nome         		= mysqli_real_escape_string($conexao, $_POST['upnome_nome' . $code]);
$email         		= mysqli_real_escape_string($conexao, $_POST['upnome_email' . $code]);

// print_($nome);
// print_($email);
// exit();
if(!empty($nome) && !empty($email)){
	if(newupdate("config_usuarios", "USU_NOME = '{$nome}', USU_EMAIL = '{$email}' WHERE USU_CODIGO = '{$codigo}'")){
		// Logs
        insert_logs($path_pagina);
		
		$resp = array('resposta' => 'true');
	
	} else { $resp = array('resposta' => 'false'); }
} else { $resp = array('resposta' => 'dados'); }
echo json_encode($resp);
