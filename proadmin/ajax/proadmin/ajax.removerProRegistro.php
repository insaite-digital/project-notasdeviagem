<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../../classes/Conexao.class.php');
require_once('../../funcoes/funcoes.php');
require_once('../../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time      = date("Y-m-d H:i:s");
$cadastro_usuario   = $_SESSION['USUARIO_CODIGO'];

$pagina         = $_POST['remove_pagina'];
$tabela 	    = $_POST['remove_tabela'];
$codigo   	    = $_POST['remove_codigo'];
$motivo   	    = $_POST['remove_motivo'];
$coluna         = $_POST['remove_coluna'];
$path_pagina    = "#" . $pagina;

if(newdelete($tabela,"WHERE " . $coluna . " = '{$codigo}'")){
    insert_logs($path_pagina, $motivo);
    $resp = array('resposta' => 'true');
} else { $resp = array('resposta' => 'false'); }

echo json_encode($resp);