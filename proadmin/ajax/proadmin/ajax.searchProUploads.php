<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../../classes/Conexao.class.php');
require_once('../../funcoes/funcoes.php');
require_once('../../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];

$type = false;
$cod = intval($_POST['id']);

$allowedTypes = [];
$upload_projeto = newsql("SELECT * FROM config_uploads_projetos");
foreach ($upload_projeto as $key => $value) {
	array_push($allowedTypes, $value['PROJ_TITULO']);
}
if(isset($_POST['type']) && in_array($_POST['type'], $allowedTypes)){$type = $_POST['type']; }

if($type){
    $uploads = newsql("SELECT * FROM config_uploads as upload LEFT JOIN config_uploads_projetos_tipos as tipo ON tipo.TIPO_CODIGO = upload.UP_TIPO WHERE UP_TABELA = '{$type}' AND UP_COD_REG = '{$cod}'");
}

$resp = array('uploads' => $uploads);


echo json_encode($resp);