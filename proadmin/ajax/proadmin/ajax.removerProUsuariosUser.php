<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../../classes/Conexao.class.php');
require_once('../../funcoes/funcoes.php');
require_once('../../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];

$code 		        = $_GET['code'];
$path_pagina    	= mysqli_real_escape_string($conexao, $_POST['removeuser_pagina' . $code]);
$tabela         	= mysqli_real_escape_string($conexao, $_POST['removeuser_tabela' . $code]);
$codigo		    	= mysqli_real_escape_string($conexao, $_POST['removeuser_codigo' . $code]);
$coluna         	= mysqli_real_escape_string($conexao, $_POST['remove_coluna' . $code]);
$motivo         	= mysqli_real_escape_string($conexao, $_POST['remove_motivo' . $code]);

if(!empty($coluna) && !empty($motivo)){
	if(newdelete($tabela,"WHERE " . $coluna . " = '{$codigo}'")){
		// Logs
        insert_logs($path_pagina, $motivo);
		
		$resp = array('resposta' => 'true');
	
	} else { $resp = array('resposta' => 'false'); }
} else { $resp = array('resposta' => 'dados'); }
echo json_encode($resp);
