
<div class="sidebar sidebar-main">
    <div class="sidebar-content">
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left"><img src="_template/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        <span class="media-heading text-semibold"><?php echo $sql_user[0]['USU_NOME']; ?></span>
                        <div class="text-size-mini text-muted">
                            <i class="icon-user-check text-size-small"></i> <strong>Pefil:</strong>&nbsp;<?php echo $sql_user[0]['PER_TITULO']; ?><br>
                            <i class="icon-laptop text-size-small"></i> &nbsp;<?php echo gethostbyaddr($_SERVER['REMOTE_ADDR']); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
        <?php $active = $_GET['active']; ?>
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <li class="active"><a href="index.php" style="background-color: #2196F3!important;"><i class="icon-mouse-left"></i> <span>Dashboard</span></a></li>
                    <?php if($_SESSION['USUARIO_MASTER'] == 'true'){ ?>
                        <li class="navigation-header"><span>PRO ADMIN</span> <i class="icon-menu" title="PRO ADMIN"></i></li>
                        <li class="<?php if($_GET['active'] == 'promodulos') echo 'active'; ?>">
                            <a href="consultarProModulos.php?active=promodulos"><i class="icon-stack3"></i> <span>Módulos </span></a>
                        </li>
                        <li class="<?php if($_GET['active'] == 'properfis') echo 'active'; ?>">
                            <a href="consultarProPerfis.php?active=properfis"><i class="icon-dice"></i> <span>Perfis </span></a>
                        </li>
                        
                        
                        <li class="">
                            <a href="#"><i class="icon-droplet2"></i> <span>Padrões da Plataforma </span></a>
                            <ul>
                                <li class="<?php if($_GET['active'] == 'cores') echo 'active'; ?>"><a href="consultarProCores.php?active=cores">Paleta de Cores</a></li>
                                <li class="<?php if($_GET['active'] == 'icones') echo 'active'; ?>"><a href="consultarProModulosIcones.php?active=icones" target="_blank">Grade de Ícones </a></li>
                            </ul>
                        </li>

                        <li class="<?php if($_GET['active'] == 'atualizacoes') echo 'active'; ?>">
                            <a href="#"><i class="icon-earth"></i> <span>Base de Atualizações </span></a>
                            <ul>
                                <li class="<?php if($_GET['active'] == 'atualizacoes') echo 'active'; ?>"><a href="consultarProAtualizacoes.php?active=atualizacoes">Histórico de Atualizações </a></li>
                                <li class="<?php if($_GET['active'] == 'atualizacoes-tipos') echo 'active'; ?>"><a href="consultarProAtualizacoestipos.php?active=atualizacoes-tipos">Tipos de Atualizaões </a></li>
                            </ul>
                        </li>
                        <li class="<?php if($_GET['active'] == 'uploads') echo 'active'; ?>">
                            <a href="#"><i class="icon-upload"></i> <span>Uploads </span></a>
                            <ul>
                                <li class="<?php if($_GET['active'] == 'uploads') echo 'active'; ?>"><a href="consultarProUploads.php?active=uploads">Tipos de Uploads </a></li>
                                <li class="<?php if($_GET['active'] == 'modelo-uploads') echo 'active'; ?>"><a href="consultarProUploadsmodelos.php?active=modelo-uploads">Modelos/Pastas </a></li>
                            </ul>
                        </li>
                        
                        <li class="<?php if($_GET['active'] == 'defaults') echo 'active'; ?>">
                            <a href="consultarDefaults.php?active=defaults"><i class="icon-flip-horizontal2"></i> <span>Defaults </span></a>
                        </li>
                    <?php } ?>
                    
                    <li class="navigation-header"><span>GESTÃO</span> <i class="icon-menu" title="GESTÃO"></i></li>
                    <li class="<?php if($_GET['active'] == 'publicacoes') echo 'active'; ?>">
                        <a href="#"><i class="icon-newspaper"></i> <span>Publicações </span></a>
                        <ul>
                            <li class="<?php if($_GET['active'] == 'publicacoes') echo 'active'; ?>"><a href="consultarPublicacoes.php?active=publicacoes">Listar Publicações </a></li>
                            <li class="<?php if($_GET['active'] == 'categorias-de-publicacoes') echo 'active'; ?>"><a href="consultarCategorias.php?active=categorias-de-publicacoes">Categorias de Publicações </a></li>
                        </ul>
                    </li>
                    <li class="navigation-header"><span>ADMINISTRAÇÃO</span> <i class="icon-menu" title="ADMINISTRAÇÃO"></i></li>
                    <li class="navigation-header"><span>EXTRA</span> <i class="icon-menu" title="EXTRA"></i></li>
                    
                    <!-- <li class="navigation-header"><span>Gerenciamento</span> <i class="icon-menu" title="Gerenciamento"></i></li>

                    <li>
                        <a href="#"><i class="icon-office"></i> <span>Institucional/Município  </span></a>
                        <ul>
                            <li class="<?php if($_GET['active'] == 'lai') echo 'active'; ?>"><a href="alterarTextos.php?active=lai&texto=5">Lei de Acesso à Informação </a></li>
                            <li class="<?php if($_GET['active'] == 'lgpd') echo 'active'; ?>"><a href="alterarTextos.php?active=lgpd&texto=6">Lei Geral de Proteção de Dados  </a></li>
                            <li class="<?php if($_GET['active'] == 'historia') echo 'active'; ?>"><a href="alterarTextos.php?active=historia&texto=3">História </a></li>
                            <li class="<?php if($_GET['active'] == 'hino') echo 'active'; ?>"><a href="alterarTextos.php?active=hino&texto=1">Hino</a></li>
                            <li class="<?php if($_GET['active'] == 'brasao') echo 'active'; ?>"><a href="alterarTextos.php?active=brasao&texto=2">Brasão </a></li>
                            <li>
                                <a href="#"><span>Dados Gerais </span></a>
                                <ul>
                                    <li class="<?php if($_GET['active'] == 'clima') echo 'active'; ?>"><a href="alterarTextos.php?active=clima&texto=13">Clima </a></li>
                                    <li class="<?php if($_GET['active'] == 'geografia') echo 'active'; ?>"><a href="alterarTextos.php?active=geografia&texto=14">Geografia </a></li>
                                    <li class="<?php if($_GET['active'] == 'demografia') echo 'active'; ?>"><a href="alterarTextos.php?active=demografia&texto=15">Demografia </a></li>
                                    <li class="<?php if($_GET['active'] == 'administracao') echo 'active'; ?>"><a href="alterarTextos.php?active=administracao&texto=16">Administração </a></li>
                                    <li class="<?php if($_GET['active'] == 'economia') echo 'active'; ?>"><a href="alterarTextos.php?active=economia&texto=17">Economia </a></li>
                                </ul>
                            </li>
                            <li class="<?php if($_GET['active'] == 'mapas') echo 'active'; ?>"><a href="consultarMapas.php?active=mapas">Mapas </a></li>
                            <li class="<?php if($_GET['active'] == 'galeriasprefeitos') echo 'active'; ?>"><a href="consultarGaleriasprefeitos.php?active=galeriasprefeitos">Galeria de Prefeitos </a></li>
                        </ul>
                    </li>
                    
                    <li>
                        <a href="#"><i class="icon-city"></i> <span>Governo  </span></a>
                        <ul>
                            <li class="<?php if($_GET['active'] == 'estrutura') echo 'active'; ?>"><a href="alterarTextos.php?active=estrutura&texto=4">Estrutura Organizacional </a></li>
                            <li class="<?php if($_GET['active'] == 'gabinetes') echo 'active'; ?>"><a href="consultarGabinetes.php?active=gabinetes">Gabinetes  </a></li>
                            <li class="<?php if($_GET['active'] == 'secretarias') echo 'active'; ?>"><a href="consultarSecretarias.php?active=secretarias">Secretarias </a></li>
                            <li>
                                <a href="#"><span>Conselhos </span></a>
                                <ul>
                                    <?php $count_conselhos_documentos = newsql("SELECT COUNT(*) AS total FROM tbl_conselhos_documentos")[0]['total']; ?>
                                    <li class="<?php if($_GET['active'] == 'conselhosdocumentos') echo 'active'; ?>"><a href="consultarConselhosdocumentos.php?active=conselhosdocumentos">Documentos & Anexos <span class="label bg-blue-400"><?php echo $count_conselhos_documentos; ?></span></a></li>
                                    <?php $count_conselhos_categorias = newsql("SELECT COUNT(*) AS total FROM tbl_conselhos_categorias")[0]['total']; ?>
                                    <li class="<?php if($_GET['active'] == 'conselhoscategorias') echo 'active'; ?>"><a href="consultarConselhoscategorias.php?active=conselhoscategorias">Categorias <span class="label bg-blue-400"><?php echo $count_conselhos_categorias; ?></span></a></li>
                                    <li class="navigation-divider"></li>
                                    <?php $count_conselhos = newsql("SELECT COUNT(*) AS total FROM tbl_conselhos")[0]['total']; ?>
                                    <li class="<?php if($_GET['active'] == 'conselhos') echo 'active'; ?>"><a href="consultarConselhos.php?active=conselhos">Visualizar Conselhos <span class="label bg-blue-400"><?php echo $count_conselhos; ?></span></a></li>
                                    
                                </ul>
                            </li>
                            <li>
                                <a href="#"><span>Publicações Oficiais </span></a>
                                <ul>
                                    <li class="<?php if($_GET['active'] == 'publicacoes') echo 'active'; ?>"><a href="consultarPublicacoes.php?active=publicacoes">Listar Publicações </a></li>
                                    
                                    <li class="navigation-divider"></li>
                                    <li class="<?php if($_GET['active'] == 'publicacoescategorias') echo 'active'; ?>"><a href="consultarPublicacoescategorias.php?active=publicacoescategorias">Categorias </a></li>
                                    
                                </ul>
                            </li>
                            <li>
                                <a href="#"><span>Concursos </span></a>
                                <ul>
                                    <li class="<?php if($_GET['active'] == 'concursos') echo 'active'; ?>"><a href="consultarConcursos.php?active=concursos">Listar Concursos </a></li>
                                    
                                    <li class="navigation-divider"></li>
                                    <li class="<?php if($_GET['active'] == 'concursoscategorias') echo 'active'; ?>"><a href="consultarConcursoscategorias.php?active=concursoscategorias">Categorias </a></li>
                                    <li class="<?php if($_GET['active'] == 'concursosstatus') echo 'active'; ?>"><a href="consultarConcursosstatus.php?active=concursosstatus">Status </a></li>
                                    
                                </ul>
                            </li>
                            <li>
                                <a href="#"><span>Licitações </span></a>
                                <ul>
                                    <li class="<?php if($_GET['active'] == 'licitacoes') echo 'active'; ?>"><a href="consultarLicitacoes.php?active=licitacoes">Listar Licitações </a></li>
                                    
                                    <li class="navigation-divider"></li>
                                    <li class="<?php if($_GET['active'] == 'licitacoescategorias') echo 'active'; ?>"><a href="consultarLicitacoescategorias.php?active=licitacoescategorias">Modalidades </a></li>
                                    <li class="<?php if($_GET['active'] == 'licitacoesstatus') echo 'active'; ?>"><a href="consultarLicitacoesstatus.php?active=licitacoesstatus">Situações </a></li>
                                    
                                </ul>
                            </li>
                            <li>
                                <a href="#"><span>Contratos </span></a>
                                <ul>
                                    <li class="<?php if($_GET['active'] == 'contratos') echo 'active'; ?>"><a href="consultarContratos.php?active=contratos">Listar Contratos </a></li>
                                    
                                    <li class="navigation-divider"></li>
                                    <li class="<?php if($_GET['active'] == 'contratosstatus') echo 'active'; ?>"><a href="consultarContratosstatus.php?active=contratosstatus">Situações/Status </a></li>
                                    
                                </ul>
                            </li>
                            <li class="<?php if($_GET['active'] == 'coordenadorias') echo 'active'; ?>"><a href="consultarCoordenadorias.php?active=coordenadorias">Coordenadorias </a></li>
                            <li class="<?php if($_GET['active'] == 'diretorias') echo 'active'; ?>"><a href="consultarDiretorias.php?active=diretorias">Diretorias </a></li>
                            <li class="<?php if($_GET['active'] == 'fundacoes') echo 'active'; ?>"><a href="consultarFundacoes.php?active=fundacoes">Fundações </a></li>
                            
                            <li class="<?php if($_GET['active'] == 'departamentos') echo 'active'; ?>"><a href="consultarDepartamentos.php?active=departamentos">Departamentos </a></li>
                            <li class="<?php if($_GET['active'] == 'planos') echo 'active'; ?>"><a href="consultarPlanos.php?active=planos">Planos e Políticas Municipais  </a></li>
                        </ul>
                    </li>


                    <li>
                        <a href="#"><i class="icon-users2"></i> <span>Cidadão  </span></a>
                        <ul>
                            <li class="<?php if($_GET['active'] == 'regimes') echo 'active'; ?>"><a href="consultarRegimes.php?active=regimes">Regimes & Documentos </a></li>
                            <li class="<?php if($_GET['active'] == 'projetos') echo 'active'; ?>"><a href="consultarProjetos.php?active=projetos">Projetos </a></li>
                            <li class="<?php if($_GET['active'] == 'servicos') echo 'active'; ?>"><a href="consultarServicos.php?active=servicos">Serviços </a></li>
                            <li class="<?php if($_GET['active'] == 'empresas') echo 'active'; ?>"><a href="consultarEmpresas.php?active=empresas">Empresas </a></li>
                        </ul>
                    </li>


                    <li>
                        <a href="#"><i class="icon-newspaper"></i> <span>Comunicação  </span></a>
                        <ul>
                            <li>
                                <a href="#"><span>Notícias/Conteúdo </span></a>
                                <ul>
                                    <?php $count_blogs = newsql("SELECT COUNT(*) AS total FROM tbl_blogs")[0]['total']; ?>
                                    <li class="<?php if($_GET['active'] == 'blogs') echo 'active'; ?>"><a href="consultarBlogs.php?active=blogs">Listar Publicações <span class="label bg-blue-400"><?php echo $count_blogs; ?></span></a></li>
                                    
                                    <li class="navigation-divider"></li>
                                    <?php $count_blogs_categorias = newsql("SELECT COUNT(*) AS total FROM tbl_blogs_categorias")[0]['total']; ?>
                                    <li class="<?php if($_GET['active'] == 'blogs_categorias') echo 'active'; ?>"><a href="consultarBlogscategorias.php?active=blogs_categorias">Categoria <span class="label bg-blue-400"><?php echo $count_blogs_categorias; ?></span></a></li>
                                    
                                </ul>
                            </li>
                            <li class="<?php if($_GET['active'] == 'eventos') echo 'active'; ?>"><a href="consultarEventos.php?active=eventos">Eventos  </a></li>
                            <li class="<?php if($_GET['active'] == 'comunicados') echo 'active'; ?>"><a href="consultarComunicados.php?active=comunicados">Comunicados  </a></li>
                            <li class="<?php if($_GET['active'] == 'galeriasimagens') echo 'active'; ?>"><a href="consultarGaleriasimagens.php?active=galeriasimagens">Galerias de Imagens  </a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class="icon-bus"></i> <span>Turismo  </span></a>
                        <ul>
                            <li class="<?php if($_GET['active'] == 'pontos') echo 'active'; ?>"><a href="consultarPontos.php?active=pontos">Pontos Turísticos </a></li>
                            
                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class="icon-drawer"></i> <span>Contato  </span></a>
                        <ul>
                            <li>
                                <a href="#"><span>Ouvidoria </span></a>
                                <ul>
                                    <?php $count_ouvidoria = newsql("SELECT COUNT(*) AS total FROM tbl_ouvidoria")[0]['total']; ?>
                                    <li class="<?php if($_GET['active'] == 'ouvidorias') echo 'active'; ?>"><a href="consultarOuvidorias.php?active=ouvidorias">Listar Manifestações <span class="label bg-blue-400"><?php echo $count_ouvidoria; ?></span></a></li>
                                    <li class="navigation-divider"></li>
                                    <?php $count_ouvidoria_manifestacoes = newsql("SELECT COUNT(*) AS total FROM tbl_ouvidoria_manifestacoes")[0]['total']; ?>
                                    <li class="<?php if($_GET['active'] == 'ouvidoriamanifestacoes') echo 'active'; ?>"><a href="consultarOuvidoriamanifestacoes.php?active=ouvidoriamanifestacoes">Tipos de Manifestações <span class="label bg-blue-400"><?php echo $count_ouvidoria_manifestacoes; ?></span></a></li>
                                    <?php $count_ouvidoria_status = newsql("SELECT COUNT(*) AS total FROM tbl_ouvidoria_status")[0]['total']; ?>
                                    <li class="<?php if($_GET['active'] == 'ouvidoriastatus') echo 'active'; ?>"><a href="consultarOuvidoriastatus.php?active=ouvidoriastatus">Status <span class="label bg-blue-400"><?php echo $count_ouvidoria_status; ?></span></a></li>
                                    <li class="<?php if($_GET['active'] == 'ouvidoriasobre') echo 'active'; ?>"><a href="alterarTextos.php?active=ouvidoriasobre&texto=7">Sobre Ouvidoria </a></li>
                                    <li class="<?php if($_GET['active'] == 'ouvidoriaatuacao') echo 'active'; ?>"><a href="alterarTextos.php?active=ouvidoriaatuacao&texto=8">Atuação da Ouvidoria </a></li>
                                    <li class="<?php if($_GET['active'] == 'ouvidoriaclassificacao') echo 'active'; ?>"><a href="alterarTextos.php?active=ouvidoriaclassificacao&texto=9">Classificação de Manifestações </a></li>
                                    <li class="<?php if($_GET['active'] == 'ouvidoriaatendimento') echo 'active'; ?>"><a href="alterarTextos.php?active=ouvidoriaatendimento&texto=10">Atendimento da Ouvidoria </a></li>
                                    <li class="<?php if($_GET['active'] == 'ouvidoriaalternativa') echo 'active'; ?>"><a href="alterarTextos.php?active=ouvidoriaalternativa&texto=11">Alternativas de Acesso da Ouvidoria </a></li>
                                </ul>
                            </li>
                            <li class="<?php if($_GET['active'] == 'sic') echo 'active'; ?>"><a href="alterarTextos.php?active=sic&texto=10">SIC  </a></li>
                            <li class="<?php if($_GET['active'] == 'telefones') echo 'active'; ?>"><a href="consultarTelefones.php?active=telefones">Telefones & Links Úteis</a></li>
                        </ul>
                    </li> -->                    
                </ul>
            </div>
        </div>
    </div>
</div>
