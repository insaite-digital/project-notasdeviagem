<script type="text/javascript">
    
// Basic Initialization
$('.daterange-insaite').daterangepicker({
    singleDatePicker: true,
    autoUpdateInput: true,
    emptyInitUpdate: false,
    applyClass: 'bg-slate-600',
    cancelClass: 'btn-default',
    "locale": {
        "format": "DD/MM/YYYY",
        "separator": " - ",
        "applyLabel": "Aplicar",
        "cancelLabel": "Cancelar",
        "fromLabel": "De",
        "toLabel": "Para",
        "customRangeLabel": "Customizar",
        "daysOfWeek": [
            "Dom",
            "Seg",
            "Ter",
            "Qua",
            "Qui",
            "Sex",
            "Sab"
        ],
        "monthNames": [
            "Janeiro",
            "Fevereiro",
            "Março",
            "Abril",
            "Maio",
            "Junho",
            "Julho",
            "Agosto",
            "Setembro",
            "Outubro",
            "Novembro",
            "Dezembro"
        ]
    }
});
// Resetar Data de Calendario (Empty);
$(".daterange-insaite-cadastro").val(null);
</script>