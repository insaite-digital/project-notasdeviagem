<script type="text/javascript">
    $('.summernote_update').summernote({
        popover: {
            image: [
                ['custom', ['imageAttributes', 'captionIt']],
                ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize33', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']]
            ],
        },
        toolbar:[
            ['style',['style', 'inline']], // The "Inline" Button
            ['font',['bold','italic','underline','clear']],
            ['fontname',['fontname']],
            ['color',['color']],
            ['para',['ul','ol','paragraph']],
            ['height',['height']],
            ['table',['table']],
            ['insert',['picture','media','link','hr', 'resizedDataImage']],
            ['view',['fullscreen','codeview']],
            ['help',['help']]
        ],

        lang: 'pt-BR', // Change to your chosen language
        imageAttributes:{
            icon:'<i class="note-icon-pencil"/>',
            removeEmpty:false, // true = remove attributes | false = leave empty if present
            disableUpload: false // true = don't display Upload Options | Display Upload Options
        },
        captionIt:{
            figureClass:'figure-class',
            // figureClass:'{figure-class/es}',
            figcaptionClass:'figcapture-class',
            // figcaptionClass:'{figcapture-class/es}',
            captionText:'Digite sua legenda aqui...'
        }
    });
</script>