<div class="text-center content-group">
    <h5>Não encontramos nenhuma informação...</h5>
</div>
<div class="row">
    <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
        <div class="main-search">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                    <a href="index.php" class="btn btn-primary btn-block content-group"><i class="icon-circle-left2 position-left"></i> Ir para Dashboard</a>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>
    </div>
</div>