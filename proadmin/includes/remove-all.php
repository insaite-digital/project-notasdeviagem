<script type="text/javascript">
function deleteAll(tabela, campo){
    $.blockUI({ 
        message: '<i class="icon-spinner4 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#1b2024',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            color: '#fff',
            padding: 0,
            backgroundColor: 'transparent'
        }
    });
    var notice = new PNotify({
        title: 'Precisamos de sua confirmação',
        text: '<p>Você tem certeza que deseja deletar todos estes itens (logs não serão salvos)? </p>',
        hide: false,
        type: 'warning',
        confirm: {
            confirm: true,
            buttons: [
                {
                    text: 'Sim, desejo deletar',
                    addClass: 'btn btn-sm btn-danger'
                },
                {
                    text: 'Melhor não',
                    addClass: 'btn btn-sm btn-link'
                }
            ]
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    })

    // On confirm
    notice.get().on('pnotify.confirm', function() {
        $.unblockUI();
        PNotify.removeAll();
        var array_delete = [];
        $(".input_delete").each(function(){
            if ($(this).is(':checked')) {
                array_delete.push($(this).val());
            }
        }); 
        
        var formData = {
            'tabela'        : tabela,
            'campo'         : campo,
            'array_delete'  : array_delete,
        };

        $.ajax({
            type        : 'POST', 
            url         : 'ajax/proadmin/ajax.removerProRegistros.php',
            data        : formData,
            dataType    : 'json', 
            beforeSend: function() {
                $.blockUI({ 
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
                new PNotify({
                    text: "Aguarde um momento",
                    addclass: 'bg-primary',
                    type: 'info',
                    icon: 'icon-spinner4 spinner',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    opacity: .9,
                    width: "250px"
                });
            },
            success: function (data) {
                $.unblockUI();
                PNotify.removeAll();
                var resposta = data.resposta;
                var count = data.count;
                if(resposta == 'true'){
                    new PNotify({   
                        text: count + " itens deletados com sucesso!",
                        addclass: 'bg-success',
                        type: 'success',
                        icon: 'icon-checkmark3',
                        hide: true,
                        buttons: {
                            closer: true,
                            sticker: false
                        },
                        opacity: 1,
                        width: "200px"
                    });
                    location.reload();
                }           
            }
        })
    })

    // On cancel
    notice.get().on('pnotify.cancel', function() {
        $.unblockUI();
        PNotify.removeAll();
    });
}
</script>