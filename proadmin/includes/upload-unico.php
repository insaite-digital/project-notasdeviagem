<script type="text/javascript">
$(document).on('change','.file_upload',function(event){
    var property = document.getElementById('file_upload'+database_page_codigo).files[0];
    var image_name = property.name;
    var image_extension = image_name.split('.').pop().toLowerCase();
    var form_data = new FormData();
    form_data.append("file",property);

    $.ajax({
        url: ADMIN + "upload_unico.php?id=" + database_page_codigo + "&upload_type=" + database_page_upload,
        method:'POST',
        data:form_data,
        contentType:false,
        cache:false,
        processData:false,
        dataType: 'JSON',
        beforeSend:function(){
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Realizando upload...",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success:function(data){
            $.unblockUI();
            PNotify.removeAll();
            var resposta        = data.resposta;
            var filename        = data.filename;
            var last_id        = data.last_id;
            var link        = data.link;
            
            if(resposta == 'true'){
                new PNotify({   
                    text: "Upload realizado com sucesso!",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                $("#action_image").html("");
                $("#action_image").show();


                var line_image ="<span id='imagem_check'><a target='_blank' href='"+link+"'>Visualizar Imagem ("+filename+")</a></span><span style='cursor: pointer; float: right;' onclick='removeImage("+last_id+")' class='text-danger'><i class='icon-folder-remove'></i> Remover Imagem</span>";
                $("#action_image").append(line_image);

            } else if(resposta == 'dados'){
                new PNotify({   
                    text: "Erro ao realizar upload...",
                    addclass: 'bg-warning',
                    type: 'warning',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            } else if(resposta == 'insert_documento'){
                new PNotify({   
                    text: "Erro ao realizar upload...",
                    addclass: 'bg-warning',
                    type: 'warning',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            } else if(resposta == 'insert_upload'){
                new PNotify({   
                    text: "Erro ao realizar upload...",
                    addclass: 'bg-warning',
                    type: 'warning',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            } else {
                new PNotify({   
                    text: "Erro ao realizar upload!",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
        }
    });

});

$(document).ready(function(){   
    $(".filename").html("Inserir imagem em destaque...");
    $(".uploader > .action").text("Selecionar...");
});
function removeImage(codigo){
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.removerProImagemUnica.php',
        data        : { codigo: codigo },
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Realizando upload...",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Imagem removida com sucesso!",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                $("#action_image").hide();
                $(".filename").html("Inserir nova imagem");
            } else {
                new PNotify({   
                    text: "Não foi possível remover esta imagem!",
                    addclass: 'bg-warning',
                    type: 'warning',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })
}
</script>