<?php 
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        header('Location: error.php');
    } else {
        $path_pagina = pathinfo( __FILE__ )['basename'];
        $acesso->Pagina = $path_pagina;
        $acesso->verificaPermissao();
        $resposta = $acesso->getResposta();
        if(empty($resposta) || $resposta == 'false'){ header('Location: direcionamento.php'); } else {

            $paginas = newsql("SELECT * FROM config_paginas WHERE PAG_ARQUIVO = '{$path_pagina}'")[0];
            $pagina_codigo          = $paginas['PAG_CODIGO'];
            $pagina_titulo          = $paginas['PAG_TITULO'];
            $pagina_singular        = $paginas['PAG_SINGULAR'];
            $pagina_plural          = $paginas['PAG_PLURAL'];
            $pagina_icone           = $paginas['PAG_ICONE'];
            $pagina_text_singular   = $paginas['PAG_TEXT_SINGULAR'];
            $pagina_text_plural     = $paginas['PAG_TEXT_PLURAL'];

            // Variaveis da Página de Consulta
            $pagina_coluna_id       = "USU_CODIGO";
            $pagina_tabela          = "config_usuarios";
            $url_ajax               = "ajax/ajax.configurarUsuariosPermissoes.php";
            $link_back              = "consultar" . maiuscula($pagina_plural). ".php?active=".$pagina_plural."";
            
            $codigo = $_GET['usuario'];
            $sql_consulta = newsql("SELECT * FROM ".$pagina_tabela." WHERE ".$pagina_coluna_id." = ".$codigo."")[0];

?>
<?php require_once("includes/header.php"); ?>
<!-- Theme JS files -->
<script type="text/javascript" src="_template/assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="_template/assets/js/plugins/forms/styling/switch.min.js"></script>
<!-- <script type="text/javascript" src="_template/assets/js/plugins/forms/styling/switchery.min.js"></script> -->
<script type="text/javascript" src="_template/assets/js/pages/form_checkboxes_radios.js"></script>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <!-- Página Título -->
                    <?php require_once("includes/pagina_titulo.php"); ?>
                    <span>Usuário: <strong><?php echo $sql_consulta['USU_NOME']; ?></strong></span>
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <!-- Página Links de Ações -->
                    <?php require_once("includes/pagina_link_alteracao.php"); ?>
                </div>
            </div>
        </div>
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <!-- Página Breadcrumbs -->
                <?php require_once("includes/breadcrumbs_alteracao.php"); ?>
            </ul>
            <!-- Modal Ajuda -->
            <?php require_once("includes/ajuda.php"); ?>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div calss="col-lg-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"><i class="icon-database-add"></i> <strong>Informações para Configuração</strong></h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a id="clean_form" href="header('Refresh:0');" data-action="reload"></a></li>
                            </ul>
                        </div>
                        <p class="content-group" style="margin: 0!important">Siga as etapas e passos até o final.</p>
                    </div> 
                    <div class="panel-body">


                        <form method="post" class="FormPermissoes" onsubmit="return false">
                            <input type="hidden" name="pagina" value="<?php echo $path_pagina; ?>">
                            <input type="hidden" name="tabela" value="<?php echo $pagina_tabela; ?>">
                            <input type="hidden" name="codigo" value="<?php echo $codigo; ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="tabbable tab-content-bordered">
                                        <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
                                            <?php 
                                                $sql_modulos = newsql("SELECT * FROM config_modulos");
                                                foreach ($sql_modulos as $key => $value) {
                                                    $modulo_codigo  = $value['MOD_CODIGO'];
                                                    $modulo_titulo  = $value['MOD_TITULO'];
                                                    if($key == 0){ $active = 'active'; } 
                                                    else { $active = ''; }
                                            ?>
                                            <li class="<?php echo $active; ?>"><a href="#bordered-justified-tab<?php echo $modulo_codigo; ?>" data-toggle="tab"><?php echo $modulo_titulo; ?></a></li>
                                            <?php } ?>
                                        </ul>
                                        <div class="tab-content">
                                            <?php 
                                                $sql_modulos = newsql("SELECT * FROM config_modulos");
                                                foreach ($sql_modulos as $key => $value) {
                                                    $modulo_codigo = $value['MOD_CODIGO'];
                                                    $modulo_titulo = $value['MOD_TITULO'];
                                                    if($key == 0){ $active = 'active'; } 
                                                    else { $active = ''; }
                                            ?>
                                            <div class="tab-pane has-padding <?php echo $active; ?>" id="bordered-justified-tab<?php echo $modulo_codigo; ?>">
                                                <div class="row">
                                                <?php 
                                                    $sql_permissoes = newsql("SELECT * FROM config_permissoes");
                                                    foreach ($sql_permissoes as $key => $value) {
                                                        $permissao_codigo = $value['PERM_CODIGO'];
                                                        $permissao_titulo = $value['PERM_TITULO'];
                                                ?>
                                                    <div class="col-lg-3 col-md-6">
                                                        <div class="panel panel-body">
                                                            <div class="media-header"><h5 style="font-weight: bold;"><?php echo $permissao_titulo; ?></h5></div>
                                                            <div class="media">
                                                                <div class="media-body">
                                                                    <?php 
                                                                        $sql_modulo_permissao = newsql("SELECT * FROM config_modulos_permissoes WHERE MODPERM_PERMISSAO = '{$permissao_codigo}' AND MODPERM_MODULO = '{$modulo_codigo}'");
                                                                        foreach ($sql_modulo_permissao as $key => $value) {
                                                                            $modperm_codigo = $value['MODPERM_CODIGO'];
                                                                            $modperm_titulo = $value['MODPERM_TITULO'];

                                                                            $usuario_permissao = newsql("SELECT * FROM config_usuarios_permissoes WHERE USUPERM_USUARIO = '{$codigo}' AND USUPERM_MODULO_PERMISSAO = '{$modperm_codigo}'");
                                                                    ?>
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            <h6 class="media-heading"><strong><?php echo $modperm_titulo; ?></strong></h6>
                                                                        </div>
                                                                        <div class="col-md-12 text-center">
                                                                            <div class="checkbox checkbox-switch"><label><input name="permissao[<?php echo $modperm_codigo; ?>]" <?php if(!empty($usuario_permissao)) echo 'checked'; ?> type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="Habilitado" data-off-text="Desabilitado" class="switch permissao" data-titulo="<?php echo $modperm_titulo; ?>"></label></div>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                <!-- <?php 
                                                    $sql_permissoes = newsql("SELECT * FROM config_modulos_permissoes as modulopermissao INNER JOIN config_permissoes as permissao ON permissao.PERM_CODIGO = modulopermissao.MODPERM_PERMISSAO WHERE MODPERM_MODULO = '{$modulo_codigo}'");
                                                    foreach ($sql_permissoes as $key => $value) {
                                                        $permicao_codigo = $value['MODPERM_CODIGO'];
                                                        $permicao_titulo = $value['MODPERM_TITULO'];
                                                        $permicao_default = $value['PERM_TITULO'];
                                                ?>
                                                    <div class="col-lg-4 col-md-6">
                                                        <div class="panel panel-body">
                                                            <div class="media-header"><h5 style="font-weight: bold;"><?php echo $permicao_default; ?></h5></div>
                                                            <div class="media">
                                                                <div class="media-body">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <h6 class="media-heading"><?php echo $permicao_titulo; ?></h6>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="checkbox checkbox-switch"><label><input name="permissao[<?php echo $get_permissao_titulo; ?>]" <?php if($consulta[0][$get_permissao_titulo] == 'true') echo 'checked'; ?> type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="Habilitado" data-off-text="Desabilitado" class="switch permissao" data-titulo="<?php echo $get_permissao_titulo; ?>"></label></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?> -->
                                                </div> 
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary pnotify-save">Atualizar Permissões <i class="fa fa-save position-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>


                        
                    </div>
                </div>
            </div>
        </div>
        <?php require_once("includes/copyright.php"); ?>
    </div>
</div>

<script type="text/javascript">
$(".switch").bootstrapSwitch();

$(document).on('submit', 'form.FormPermissoes', function() {  
    var pagina = "<?php echo $path_pagina; ?>";
    var url_ajax = "<?php echo $url_ajax; ?>";
    var usuario = "<?php echo $codigo; ?>";
    var link_back = "<?php echo $link_back; ?>";

    $.blockUI({ 
        message: '<i class="icon-spinner4 spinner"></i>',
        timeout: 8000,
        overlayCSS: {
            backgroundColor: '#1b2024',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            color: '#fff',
            padding: 0,
            backgroundColor: 'transparent'
        }
    });

    var percent = 0;
    var notice = new PNotify({
        text: "Verificando...",
        addclass: 'bg-primary',
        type: 'info',
        icon: 'icon-spinner4 spinner',
        hide: false,
        buttons: {
            closer: false,
            sticker: false
        },
        opacity: .9,
        width: "170px"
    });

    $.ajax({
        type        : 'POST', 
        url         : url_ajax + '?usuario='+usuario,
        data        : $(this).serialize(),
        dataType    : 'json', 
        beforeSend: function() { },
        success: function (data) {
            var resposta = data.resposta;
            setTimeout(function() {
                notice.update({
                    title: false
                });

                var interval = setInterval(function() {
                    percent += 10;
                    var options = { text: percent + "% verificado." };
                    if (percent == 30){ options.title = "Quase Lá"; }
                    if (percent >= 100) {
                        if(resposta == 'true'){
                            window.clearInterval(interval);
                            options.title = "Feito! Atualizando...";
                            options.addclass = "bg-success";
                            options.type = "success";
                            options.hide = true;
                            options.buttons = {
                                closer: true,
                                sticker: true
                            };
                            options.icon = 'icon-checkmark3';
                            options.opacity = 1;
                            options.width = PNotify.prototype.options.width;
                            setTimeout(function() {
                                window.location = link_back;
                            }, 1000);
                        } else {
                            window.clearInterval(interval);
                            options.title = "Não foi possível salvar!";
                            options.addclass = "bg-danger";
                            options.type = "danger";
                            options.hide = true;
                            options.buttons = {
                                closer: true,
                                sticker: true
                            };
                            options.icon = 'icon-cross2';
                            options.opacity = 1;
                            options.width = PNotify.prototype.options.width;
                            setTimeout(function() {
                                $('.pnotify-save').removeAttr('disabled');
                            }, 1000);
                        }
                    }
                    notice.update(options);
                }, 120);
            }, 2000);
        }
    })

});
</script>
<?php 
    require_once("includes/footer.php");
    } }
?>