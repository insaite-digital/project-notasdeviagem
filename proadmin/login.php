<?php
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        newinsert("config_acesso_login","(TENT_HOST, TENT_DATA, TENT_HORA) VALUES ('{$login_host}','{$login_dat}','{$login_hr}')");
?>
<!DOCTYPE html><html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $login_titulo_projeto; ?> - Acesso Restrito</title>
        <link rel="icon" type="image/png" href="imagens/favicon.png" />
        
        <link href="css/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="js/sweetalert/sweetalert.min.js"></script>

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/colors.css" rel="stylesheet" type="text/css">
        
        <script type="text/javascript" src="_template/assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/loaders/blockui.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/forms/styling/uniform.min.js"></script>

        <script type="text/javascript" src="_template/assets/js/core/app.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/login.js"></script>
        
        <!-- <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback" async defer></script>
        <script>
            var onloadCallback = function() { grecaptcha.execute(); };
            function setResponse(response) { document.getElementById('captcha-response').value = response; }
        </script> -->

        <style type="text/css">.rc-anchor-invisible { margin: 0 auto!important; } </style>
        <?php 
            if(empty($imagens_admin_background)){ $background = "_template/assets/images/login_cover.jpg"; } 
            else { $background = $imagem_admin_background; }
        ?>

    </head>
    <body class="login-container login-cover" style="background: url('<?php echo $background; ?>') no-repeat;">
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    <div class="content pb-20">
                        <div class="tabbable panel login-form width-400">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="active"><a href="#basic-tab1" data-toggle="tab"><h6>Área Restrita</h6></a></li>
                                <li><a href="#basic-tab2" data-toggle="tab"><h6>Segurança</h6></a></li>
                            </ul>
                            <div class="tab-content panel-body">
                                <div class="tab-pane fade in active" id="basic-tab1">
                                    <form method="post" action="acesso/login.sql.php">
                                        <div class="text-center">
                                            <?php if(empty($imagens_admin_logo)){ ?>
                                                <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div><br>
                                            <?php } else { ?>
                                                <img src="<?php echo $imagem_admin_logo; ?>" alt="Logo Admin"/>
                                            <?php } ?>
                                            <h5 class="content-group">Acesso Personalizado <br> <small class="display-block">Domínio: <strong><a href="<?php echo $login_website_projeto; ?>" target="_blank"><?php echo $login_dominio_projeto; ?></a></strong></small></h5>
                                        </div>
                                        <div class="form-group has-feedback has-feedback-left">
                                            <input type="text" class="form-control" placeholder="Usuario" name="usuario" required="required">
                                            <div class="form-control-feedback">
                                                <i class="icon-user text-muted"></i>
                                            </div>
                                        </div>
                                        <div class="form-group has-feedback has-feedback-left">
                                            <input type="password" class="form-control" placeholder="Senha" name="senha" required="required">
                                            <div class="form-control-feedback">
                                                <i class="icon-lock2 text-muted"></i>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn bg-blue btn-block">Entrar <i class="icon-arrow-right14 position-right"></i></button>
                                        </div>
                                        <!-- <div style="position: relative; display: block; height: 80px;">
                                            <div class="form-group has-feedback has-feedback-left text-center" style="position: absolute; left: 15%;">
                                                <div class="g-recaptcha" data-sitekey="6LeyZgMbAAAAABlmuGmGKBDWrCcCUQh0jHTL8jaZ" data-badge="inline" data-size="invisible" data-callback="setResponse" style="width: 400px;"></div>
                                                <input type="hidden" id="captcha-response" name="captcha-response" />
                                            </div>
                                        </div> -->
                                        <div class="content-divider text-muted form-group"><span>Direitos Reservados </span></div>
                                    </form>
                                    <span class="help-block text-center no-margin">
                                        <strong><?php echo $titulo_projeto; ?></strong> protegido por <strong><a href="https://www.google.com/recaptcha/intro/v3.html" target="_blank">Recaptcha</a> By <a href="https://www.google.com/recaptcha/intro/v3.html" target="_blank">Google</a></strong>, verifique suas credenciais para realizar o Login.
                                        <br>
                                        <br>
                                        &copy; <?php echo date("Y"); ?>. <a href="<?php echo $login_website_projeto; ?>" target="_blank"><?php echo $login_titulo_projeto; ?></a> by <a href="https://insaitedigital.com.br" target="_blank">Insaite Digital</a>
                                    </span>
                                </div>
                                <div class="tab-pane fade" id="basic-tab2">
                                    Por motivos de <strong>Segurança</strong> já salvamos as seguintes informações referente a este acesso em nossa base de dados.<br>
                                    <strong>Informações:</strong><br>
                                    <ul>
                                        <li>Data: <strong><?php echo $login_dat_config; ?></strong></li> 
                                        <li>Hora: <strong><?php echo $login_hr; ?></strong></li>
                                        <li>HOST: <strong><?php echo $login_host; ?></strong></li> 
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<?php 
        echo (isset($_GET['adv'])) ? "<script> swal('Acesso Negado...', 'Dados Incorretos!', 'error') </script>" : "" ; 
    } else { header('Location: index.php'); } 
?>
