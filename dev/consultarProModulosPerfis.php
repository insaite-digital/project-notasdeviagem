<?php 
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        header('Location: error.php');
    } else {
        $path_pagina = pathinfo( __FILE__ )['basename'];
        $acesso->Pagina = $path_pagina;
        $acesso->verificaPermissao();
        $resposta = $acesso->getResposta();
        if(empty($resposta) || $resposta == 'false'){ header('Location: direcionamento.php'); } else {

            $paginas = newsql("SELECT * FROM config_paginas WHERE PAG_ARQUIVO = '{$path_pagina}'")[0];
            $pagina_codigo          = $paginas['PAG_CODIGO'];
            $pagina_titulo          = $paginas['PAG_TITULO'];
            $pagina_singular        = $paginas['PAG_SINGULAR'];
            $pagina_plural          = $paginas['PAG_PLURAL'];
            $pagina_icone           = $paginas['PAG_ICONE'];
            $pagina_text_singular   = $paginas['PAG_TEXT_SINGULAR'];
            $pagina_text_plural     = $paginas['PAG_TEXT_PLURAL'];
            // Variaveis da Página de Consulta
            
            $codigo = $_GET['modulo'];
            $sql_consulta = newsql("SELECT * FROM config_modulos WHERE MOD_CODIGO = '{$codigo}'")[0];
            $second_title = $sql_consulta['MOD_TITULO'];

?>
<?php require_once("includes/header.php"); ?>

<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <!-- Página Título -->
                    <span class="text-semibold">
                        <a href="index.php">Dashboard</a>
                    </span> 
                    <i class="icon-arrow-right6"></i> 
                    <strong>Configuração de Módulos</strong>
                    <br>
                    <i class="icon-arrow-right6"></i> 
                    Perfil do Módulo: <strong><?php echo $second_title; ?> </strong>
                    <br>
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <!-- Página Links de Ações -->
                    <div class="heading-btn-group">
                        <!-- Página Links de Ações -->
                        
                        <a href="consultarProModulos.php?active=promodulos" class="btn btn-link btn-float has-text">
                            <i class="icon-database-arrow text-default"></i> 
                            <span>Consulta de Módulos</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <!-- Página Breadcrumbs -->
                <li>
                    <a href="index.php">
                        <i class="icon-home2 position-left"></i> 
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <i class="icon-stack3"></i> 
                    <span>Configuração de Módulos</span>
                </li>
                <li>
                    <i class="icon-arrow-right6"></i> 
                    <span><?php echo $second_title; ?></span>
                </li>
            </ul>
            <ul class="breadcrumb-elements">
                <!-- Modal Ajuda -->
                <?php require_once("includes/ajuda.php"); ?>
            </ul>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-lg-9">
                <div class="tabbable">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="activity">

                            <!-- Timeline -->
                            <div class="timeline timeline-left content-group">
                                <div class="timeline-container">

                                    <!-- Invoices -->
                                    <div class="timeline-row">
                                        <div class="timeline-icon">
                                            <div class="bg-primary-400">
                                                <i class="icon-stack3"></i>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <?php 
                                                $sql_permissoes = newsql("SELECT * FROM config_modulos_permissoes as modperm INNER JOIN config_permissoes as perm ON perm.PERM_CODIGO = modperm.MODPERM_PERMISSAO INNER JOIN config_cores as cor ON cor.COR_CODIGO = perm.PERM_COR WHERE modperm.MODPERM_MODULO = '{$codigo}' ORDER BY modperm.MODPERM_CODIGO DESC");
                                                foreach ($sql_permissoes as $key => $value) {
                                                    $modperm_codigo     = $value['MODPERM_CODIGO'];
                                                    $modperm_modulo     = $value['MODPERM_MODULO'];
                                                    $modperm_permissao  = $value['MODPERM_PERMISSAO'];
                                                    $modperm_titulo     = $value['MODPERM_TITULO'];
                                                    $perm_codigo        = $value['PERM_CODIGO'];
                                                    $perm_titulo        = $value['PERM_TITULO'];
                                                    $perm_cor           = $value['PERM_COR'];
                                                    $cor_visibilidade   = $value['COR_VISIBILIDADE'];
                                                    $cor_titulo         = $value['COR_TITULO'];
                                                    $cor_classe         = $value['COR_CLASSE'];
                                                    
                                                    $sql_consulta_paginas = newsql("SELECT * FROM config_paginas as pagina INNER JOIN config_modulos_permissoes as modperm ON modperm.MODPERM_CODIGO = pagina.PAG_MODULO_PERMISSAO INNER JOIN config_permissoes as permissao ON permissao.PERM_CODIGO = modperm.MODPERM_PERMISSAO INNER JOIN config_cores as cor ON cor.COR_CODIGO = permissao.PERM_COR WHERE pagina.PAG_MODULO_PERMISSAO = '{$modperm_codigo}'");
                                            ?>
                                            <div class="col-lg-6">
                                                <div class="panel border-left-lg border-left-<?php echo $cor_classe; ?> invoice-grid timeline-content">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <h6 class="text-semibold no-margin-top"><?php echo $modperm_titulo; ?></h6>
                                                                <ul class="list list-unstyled">
                                                                    <?php $qtd_paginas = newsql("SELECT * FROM config_paginas WHERE PAG_MODULO_PERMISSAO = '{$modperm_codigo}'")[0]; ?>
                                                                    <li><span class="text-semibold">Página:</span> <a href="#" data-toggle="modal" data-target="#edit_pagina<?php echo $modperm_codigo; ?>" class="text-primary"><?php echo $qtd_paginas['PAG_TITULO']; ?></a></li>
                                                                    <?php $logs_permissao = newsql("SELECT COUNT(*) as total FROM config_logs WHERE LOG_MODULO_PERMISSAO = '{$modperm_codigo}'")[0]['total']; ?>
                                                                    <li><a href="consultarProModulosLogs.php?promodulos&permissao=<?php echo $modperm_codigo; ?>" class="text-primary">Quantidade de Logs</a>: <span class="text-semibold"><?php echo $logs_permissao; ?></span></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="panel-footer panel-footer-condensed">
                                                        <div class="heading-elements">
                                                            <span class="heading-text">
                                                                <span class="status-mark border-<?php echo $cor_classe; ?> position-left"></span> <?php echo $perm_titulo; ?></span>
                                                            </span>
                                                            <ul class="list-inline list-inline-condensed heading-text pull-right">
                                                                <li class="dropdown">
                                                                    <a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i> <span class="caret"></span></a>
                                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                                        <li><a href="#" class="text-default" data-toggle="modal" data-target="#update_pagina<?php echo $modperm_codigo; ?>"><i class="icon-database-edit2"></i>Alterar Página</a></li>
                                                                        <li><a href="#" class="text-default" data-toggle="modal" data-target="#edit_permissao<?php echo $modperm_codigo; ?>"><i class="icon-database-edit2"></i>Alterar Permissão</a></li>
                                                                        <li><a href="consultarProModulosAjudas.php?ajuda=<?php echo $modperm_codigo; ?>" class="text-default" ><i class="icon-help"></i>Configurar Ajuda</a></li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="update_pagina<?php echo $modperm_codigo; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header bg-primary">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h5 class="modal-title">
                                                                Deseja alterar a <strong>Página</strong> para <strong>Desenvolvimento</strong>?<br>
                                                            </h5>
                                                        </div>
                                                        <hr style="margin: 0 auto;">
                                                        <form id="FormUpdatePagina<?php echo $modperm_codigo; ?>" method="post" onsubmit="return false">
                                                            <div class="modal-body">
                                                                <input type="hidden" value="<?php echo $codigo; ?>" name="in_codigo<?php echo $modperm_codigo; ?>" id="in_codigo<?php echo $modperm_codigo; ?>">
                                                                <input type="hidden" value="<?php echo $modperm_codigo; ?>" name="in_modperm<?php echo $modperm_codigo; ?>" id="in_modperm<?php echo $modperm_codigo; ?>">
                                                                <input type="hidden" value="<?php echo $sql_consulta_paginas[0]['PAG_CODIGO']; ?>" name="page<?php echo $modperm_codigo; ?>" id="page<?php echo $modperm_codigo; ?>">
                                                                <div class="form-group">
                                                                    <label><strong>Ícone de Referência :</strong></label>
                                                                    <div class="input-group">
                                                                        <input id="in_icone<?php echo $modperm_codigo; ?>" name="in_icone<?php echo $modperm_codigo; ?>" class="form-control" placeholder="Ex: icon-sphere" value="<?php echo $sql_consulta_paginas[0]['PAG_ICONE']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label><strong>Título da Página <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                                                    <div class="input-group">
                                                                        <input id="in_page<?php echo $modperm_codigo; ?>" name="in_page<?php echo $modperm_codigo; ?>" class="form-control" placeholder="Insira um título" value="<?php echo $sql_consulta_paginas[0]['PAG_TITULO']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label><strong>Título (Singular) <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                                                    <div class="input-group">
                                                                        <input id="singular<?php echo $modperm_codigo; ?>" name="in_singular<?php echo $modperm_codigo; ?>" class="form-control" placeholder="Ex: Texto" value="<?php echo $sql_consulta_paginas[0]['PAG_TEXT_SINGULAR']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label><strong>Título (Plural) <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                                                    <div class="input-group">
                                                                        <input id="in_plural<?php echo $modperm_codigo; ?>" name="in_plural<?php echo $modperm_codigo; ?>" class="form-control" placeholder="Ex: Textos" value="<?php echo $sql_consulta_paginas[0]['PAG_TEXT_PLURAL']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label><strong>Arquivo para Desenvolvimento <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                                                    <div class="input-group">
                                                                        <input id="in_arquivo<?php echo $modperm_codigo; ?>" name="in_arquivo<?php echo $modperm_codigo; ?>" class="form-control" placeholder="Insira o nome do arquivo" value="<?php echo $sql_consulta_paginas[0]['PAG_ARQUIVO']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório">.php</span>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                                <a href="consultarProModulosIcones.php?active=icones" target="_blank" class="btn btn-info"> Verificar Icones&nbsp;&nbsp;<i class="icon-laptop"></i></a>
                                                                <button type="submit" class="btn btn-primary update_pagina" data-codigo="<?php echo $modperm_codigo; ?>">Atualizar&nbsp;&nbsp;<i class="icon-database-refresh"></i></button>
                                                            </div>
                                                        </form>                          
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div id="edit_pagina<?php echo $modperm_codigo; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h5 class="modal-title">
                                                                Visualizar <strong>Páginas</strong><br>
                                                            </h5>
                                                        </div>
                                                        <br><hr style="margin: 0 auto;">
                                                        <form id="#" method="post" onsubmit="return false">
                                                            <div class="modal-body">
                                                                <div class="table-responsive">
                                                                    <table class="table text-nowrap">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Página Desenvolvida</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php 
                                                                                
                                                                                foreach ($sql_consulta_paginas as $key => $value) {
                                                                                    $pagina_codigo          = $value['PAG_CODIGO'];
                                                                                    $pagina_titulo          = $value['PAG_TITULO'];
                                                                                    $pagina_arquivo         = $value['PAG_ARQUIVO'];
                                                                                    $pagina_text_singular   = $value['PAG_TEXT_SINGULAR'];
                                                                                    $pagina_text_plural     = $value['PAG_TEXT_PLURAL'];
                                                                                    $pagina_icone           = $value['PAG_ICONE'];
                                                                                    $pagina_singular        = $value['PAG_SINGULAR'];
                                                                                    $pagina_plural          = $value['PAG_PLURAL'];
                                                                                    $cor_classe             = $value['COR_CLASSE'];
                                                                            ?>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="media-left media-middle">
                                                                                        <a href="#" class="btn bg-<?php echo $cor_classe; ?>-400 btn-rounded btn-icon btn-xs">
                                                                                            <span class="letter-icon"></span>
                                                                                        </a>
                                                                                    </div>

                                                                                    <div class="media-body">
                                                                                        <div class="media-heading">
                                                                                            <a href="javascript:void();" class="letter-icon-title"><?php echo $pagina_titulo; ?></a>
                                                                                        </div>

                                                                                        <div class="text-muted text-size-small"><i class="icon-checkmark3 text-size-mini position-left"></i> <?php echo $pagina_arquivo . '.php'; ?></div>
                                                                                        <div class="text-muted text-size-small"><i class="icon-checkmark3 text-size-mini position-left"></i> <?php echo $pagina_text_singular . ' (Singular)'; ?></div>
                                                                                        <div class="text-muted text-size-small"><i class="icon-checkmark3 text-size-mini position-left"></i> <?php echo $pagina_text_plural . ' (Plural)'; ?></div>
                                                                                        <div class="text-muted text-size-small"><i class="icon-checkmark3 text-size-mini position-left"></i> <?php echo $pagina_icone . ' (Ícone)'; ?></div>
                                                                                        <div class="text-muted text-size-small"><i class="icon-checkmark3 text-size-mini position-left"></i> <?php echo $pagina_plural . ' (Links Plural)'; ?></div>
                                                                                        <div class="text-muted text-size-small"><i class="icon-checkmark3 text-size-mini position-left"></i> <?php echo $pagina_singular . ' (Links Singular)'; ?></div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <?php } ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                                            </div>
                                                        </form>                          
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="edit_permissao<?php echo $modperm_codigo; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h5 class="modal-title">
                                                                Deseja atualizar este <strong>Registro</strong>?<br>
                                                                Módulo (Selecionado): <strong class="text-semibold"><?php echo $second_title; ?></strong><br>
                                                                Permissão: <strong class="text-semibold"><?php echo $perm_titulo ?></strong>
                                                            </h5>
                                                        </div>
                                                        <br><hr style="margin: 0 auto;">
                                                        <form id="FormEdit<?php echo $modperm_codigo; ?>" method="post" onsubmit="return false">
                                                            <div class="modal-body">
                                                                <input type="hidden" value="<?php echo $modperm_codigo; ?>" name="edit_codigo<?php echo $modperm_codigo; ?>" id="edit_codigo<?php echo $modperm_codigo; ?>">
                                                                <input type="hidden" value="<?php echo $perm_codigo; ?>" name="edit_permissao<?php echo $modperm_codigo; ?>" id="edit_permissao<?php echo $modperm_codigo; ?>">
                                                                <input type="hidden" value="<?php echo $codigo; ?>" name="edit_modulo<?php echo $modperm_codigo; ?>" id="edit_modulo<?php echo $modperm_codigo; ?>">
                                                                <div class="form-group">
                                                                    <label><strong>Título <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                                                    <div class="input-group">
                                                                        <input id="permissao_titulo<?php echo $modperm_codigo; ?>" name="permissao_titulo<?php echo $modperm_codigo; ?>" class="form-control" placeholder="Insira um título" value="<?php echo $modperm_titulo; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label><strong>Permissão :</strong></label>
                                                                    <div class="input-group">
                                                                        <select data-placeholder="Selecione a permissão" title="Indique a permissão" class="select" name="permissao<?php echo $modperm_codigo; ?>" id="permissao<?php echo $modperm_codigo; ?>">
                                                                            <?php 
                                                                                $sql_perm = newsql("SELECT * FROM config_permissoes AS perm INNER JOIN config_cores AS cor WHERE cor.COR_CODIGO = perm.PERM_COR");
                                                                                foreach ($sql_perm as $chave => $vl) {
                                                                                    $permission_codigo = $vl['PERM_CODIGO'];
                                                                                    $permission_titulo = $vl['PERM_TITULO'];
                                                                            ?>
                                                                            <option <?php if($permission_codigo == $modperm_permissao) echo 'selected'; ?> value="<?php echo $permission_codigo; ?>"><?php echo $permission_titulo; ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                                <button type="submit" class="btn btn-primary update_permissao" data-codigo="<?php echo $modperm_codigo; ?>">Atualizar&nbsp;&nbsp;<i class="icon-database-refresh"></i></button>
                                                            </div>
                                                        </form>                          
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>    
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="thumbnail">
                    <div class="caption text-center">
                        <h6 class="text-semibold no-margin"><strong>Módulo</strong><br><?php echo $second_title; ?> <small class="display-block">Selecionado</small></h6>
                    </div>
                </div>
                
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"><strong>Navegação Importante</strong></h6>
                    </div>

                    <div class="list-group no-border no-padding-top">
                        <a href="#" data-toggle="modal" data-target="#insert_permissao" class="list-group-item"><i class="icon-database-add"></i> Criar Permissão/Página </a>
                        <?php $logs_total = newsql("SELECT COUNT(*) as total FROM config_logs WHERE LOG_MODULO = '{$codigo}'")[0]['total']; ?>
                        <a href="consultarProModulosLogs.php?active=promodulos&modulo=<?php echo $codigo; ?>" class="list-group-item"><i class="icon-calendar3"></i> Logs de Usuários <span class="badge bg-teal-400 pull-right"><?php echo $logs_total; ?></span></a>
                        <div class="list-group-divider"></div>
                        <?php $permissoes_total = newsql("SELECT COUNT(*) as total FROM config_permissoes")[0]['total']; ?>
                        <a href="#" data-toggle="modal" data-target="#permissao_default" class="list-group-item"><i class="icon-tree7"></i> Permissões Padrões <span class="badge bg-danger pull-right"><?php echo $permissoes_total; ?></span></a>
                    </div>

                    <div id="insert_permissao" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">
                                        Deseja cadastrar nova <strong>Permissão</strong>?<br>
                                    </h5>
                                </div>
                                <hr style="margin: 0 auto;">
                                <form id="FormInsert" method="post" onsubmit="return false">
                                    <div class="modal-body">
                                        <input type="hidden" value="<?php echo $codigo; ?>" name="in_codigo" id="in_codigo">
                                        <div class="form-group">
                                            <label><strong>Tipo de Permissão :</strong></label>
                                            <div class="input-group">
                                                <select data-placeholder="Selecione a permissão" title="Indique a permissão" class="select" name="in_permissao" id="in_permissao">
                                                    <?php 
                                                        $sql_perm = newsql("SELECT * FROM config_permissoes AS perm INNER JOIN config_cores AS cor WHERE cor.COR_CODIGO = perm.PERM_COR");
                                                        foreach ($sql_perm as $chave => $vl) {
                                                            $permission_codigo = $vl['PERM_CODIGO'];
                                                            $permission_titulo = $vl['PERM_TITULO'];
                                                    ?>
                                                    <option value="<?php echo $permission_codigo; ?>"><?php echo $permission_titulo; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Título da Permissão <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                            <div class="input-group">
                                                <input id="in_titulo" name="in_titulo" class="form-control" placeholder="Insira o título da permissão" value="">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Ícone de Referência :</strong></label>
                                            <div class="input-group">
                                                <input id="in_icone" name="in_icone" class="form-control" placeholder="Ex: icon-sphere" value="">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Título da Página <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                            <div class="input-group">
                                                <input id="in_page" name="in_page" class="form-control" placeholder="Insira um título" value="">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Título (Singular) <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                            <div class="input-group">
                                                <input id="singular" name="in_singular" class="form-control" placeholder="Ex: Texto" value="">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Título (Plural) <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                            <div class="input-group">
                                                <input id="in_plural" name="in_plural" class="form-control" placeholder="Ex: Textos" value="">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Arquivo para Desenvolvimento <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                            <div class="input-group">
                                                <input id="in_arquivo" name="in_arquivo" class="form-control" placeholder="Insira o nome do arquivo" value="">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório">.php</span>
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <a href="consultarProModulosIcones.php?active=icones" target="_blank" class="btn btn-info"> Verificar Icones&nbsp;&nbsp;<i class="icon-laptop"></i></a>
                                        <button type="submit" class="btn btn-primary insert_permissao" data-codigo="<?php echo $modperm_codigo; ?>">Cadastrar&nbsp;&nbsp;<i class="icon-database-add"></i></button>
                                    </div>
                                </form>                          
                            </div>
                        </div>
                    </div>

                    <div id="permissao_default" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">
                                        Visualizar <strong>Permissões Padrões</strong><br>
                                    </h5>
                                </div>
                                <br><hr style="margin: 0 auto;">
                                <form id="#" method="post" onsubmit="return false">
                                    <div class="modal-body">
                                        <div class="table-responsive">
                                            <table class="table text-nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>Permissões Aceitas</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $sql_consulta_permissoes = newsql("SELECT * FROM config_permissoes as permissao INNER JOIN config_cores as cor ON cor.COR_CODIGO = permissao.PERM_COR");
                                                        foreach ($sql_consulta_permissoes as $key => $value) {
                                                            $conperm_codigo = $value['PERM_CODIGO'];
                                                            $conperm_titulo = $value['PERM_TITULO'];
                                                            $conperm_cor    = $value['PERM_COR'];
                                                            $concor_titulo  = $value['COR_TITULO'];
                                                            $concor_classe  = $value['COR_CLASSE'];
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <div class="media-left media-middle">
                                                                <a href="#" class="btn bg-<?php echo $concor_classe; ?>-400 btn-rounded btn-icon btn-xs">
                                                                    <span class="letter-icon"></span>
                                                                </a>
                                                            </div>

                                                            <div class="media-body">
                                                                <div class="media-heading">
                                                                    <a href="javascript:void();" class="letter-icon-title"><?php echo $conperm_titulo; ?></a>
                                                                </div>

                                                                <div class="text-muted text-size-small"><i class="icon-checkmark3 text-size-mini position-left"></i> <?php echo $concor_titulo; ?></div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"><strong>ALTERAR MÓDULO </strong>dados cadastrais</h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form id="FormDefault" class="stepy-default" method="post" onsubmit="return false">
                            <input type="hidden" name="modulo_codigo" value="<?php echo $codigo; ?>">
                            <div class="form-group">
                                <input type="text" name="modulo_titulo" class="form-control mb-15" placeholder="Inserir Título Principal do Módulo" value="<?php echo $sql_consulta['MOD_TITULO']; ?>">
                            </div>
                            <div class="form-group">
                                <textarea name="modulo_descricao" class="form-control mb-15" rows="3" cols="1" placeholder="Descrição do Módulo:"><?php echo $sql_consulta['MOD_DESCRICAO']; ?></textarea>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-right">
                                    <button id="update_save" type="button" class="btn btn-primary btn-labeled btn-labeled-right">Atualizar <b><i class="icon-circle-right2"></i></b></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Upload -->
        <?php require_once("includes/copyright.php"); ?>
    </div>
</div>

<script type="text/javascript">
$('.insert_permissao').on('click', function () {
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.insertProModulosPermissoes.php',
        data        : $("#FormInsert").serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })


});


$('.update_permissao').on('click', function () {
    var codigo = $(this).attr("data-codigo");
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.updateProModulosPermissoes.php?code=' + codigo,
        data        : $("#FormEdit"+codigo).serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })


});

$('.update_pagina').on('click', function () {
    var codigo = $(this).attr("data-codigo");
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.updateProModulosPaginas.php?code=' + codigo,
        data        : $("#FormUpdatePagina"+codigo).serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })


});


$('#update_save').on('click', function () {
    
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.updateProModulos.php',
        data        : $("#FormDefault").serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })


});

</script>
<?php 
    require_once("includes/footer.php");
    } }
?>