<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Error PRO</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/colors.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="_template/assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/loaders/blockui.min.js"></script>

        <script type="text/javascript" src="_template/assets/js/core/app.js"></script>
    </head>
    <body class="login-container">
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    <div class="content">
                        <div class="text-center content-group">
                            <h1 class="error-title offline-title">Offline</h1>
                            <h5>Desculpe, mas você não pode prosseguir, algo aconteceu para que você não pudesse acessar a página seguinte, desde uma tentativa maliciosa até mesmo uma perde de conexão de internet!</h5>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
                                <form action="#" class="main-search">
                                    
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <a href="javascript:history.back();" class="btn btn-info btn-block content-group"><i class="icon-circle-left2 position-left"></i> Voltar para a Página Anterior</a>
                                        </div>
                                        <div class="col-sm-6">
                                            <a href="login.php" class="btn btn-primary btn-block content-group"><i class="icon-circle-left2 position-left"></i> Realizar Login</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="content">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <?php require_once("includes/copyright.php"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
