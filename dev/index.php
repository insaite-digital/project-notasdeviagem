<?php 
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        header('Location: error.php');
    } else {
        $path_pagina = pathinfo( __FILE__ )['basename'];
        $acesso->Pagina = $path_pagina;
        $acesso->verificaPermissao();
        $resposta = $acesso->getResposta();
        if(empty($resposta) || $resposta == 'false'){ header('Location: direcionamento.php'); } else {
?>

<?php require_once("includes/header.php"); ?>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                </div>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ul>
        </div>
    </div>

    <!-- Content area -->
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <?php 
                        $sql_usuarios_total = newsql("SELECT COUNT(*) as total FROM config_usuarios")[0];
                        $sql_usuarios_logados = newsql("SELECT COUNT(*) as total FROM config_usuarios WHERE USU_LOGADO = 'true' AND USU_BLOQUEADO = 'false'")[0];
                        $sql_usuarios_off = newsql("SELECT COUNT(*) as total FROM config_usuarios WHERE USU_BLOQUEADO = 'true'")[0];
                        $sql_perfis_count = newsql("SELECT COUNT(*) as total FROM config_perfis WHERE PER_CODIGO != '1' AND PER_CODIGO != '2'")[0];
                    ?>
                    <div class="col-lg-3">
                        <div class="panel bg-blue-400">
                            <div class="panel-body">
                                <div class="heading-elements"></div>
                                <h3 class="no-margin"><i class="icon-users" style="margin-right: 8px;"></i><?php echo $sql_usuarios_total['total']; ?></h3>
                                Total Usuários
                                <div class="text-muted text-size-small">Dados Validados</div>
                            </div>
                            <div class="container-fluid">
                                <div id="members-online"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="panel bg-success-400">
                            <div class="panel-body">
                                <div class="heading-elements"></div>
                                <h3 class="no-margin"><i class="icon-user-check" style="margin-right: 8px;"></i><?php echo $sql_usuarios_logados['total']; ?></h3>
                                Usuários Ativos
                                <div class="text-muted text-size-small">Sessão Iniciada</div>
                            </div>
                            <div id="server-load"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="panel bg-danger-400">
                            <div class="panel-body">
                                <div class="heading-elements"></div>
                                <h3 class="no-margin"><i class="icon-user-block" style="margin-right: 8px;"></i><?php echo $sql_usuarios_off['total']; ?></h3>
                                Usuários Bloqueados
                                <div class="text-muted text-size-small">Logs Salvos</div>
                            </div>
                            <div id="today-revenue"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="panel bg-teal-400">
                            <div class="panel-body">
                                <div class="heading-elements"></div>
                                <h3 class="no-margin"><i class="icon-hammer-wrench" style="margin-right: 8px;"></i><?php echo $sql_perfis_count['total']; ?></h3>
                                Perfis
                                <div class="text-muted text-size-small">Quantidade Ativa</div>
                            </div>
                            <div id="today-revenue"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php require_once("includes/copyright.php"); ?>
    </div>
</div>

<?php 
    require_once("includes/footer.php");
    } }
?>