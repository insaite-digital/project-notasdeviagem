<?php 
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        header('Location: error.php');
    } else {
        $path_pagina = pathinfo( __FILE__ )['basename'];
        $acesso->Pagina = $path_pagina;
        $acesso->verificaPermissao();
        $resposta = $acesso->getResposta();
        if(empty($resposta) || $resposta == 'false'){ header('Location: direcionamento.php'); } else {
            date_default_timezone_set('America/Sao_Paulo');
            $cadastro_time      = date("Y-m-d H:i:s");
            $cadastro_usuario   = $_SESSION['USUARIO_CODIGO'];
            insert_logs($path_pagina,"");

            /* Variaveis da Página Database */
            $paginas                    = newsql("SELECT * FROM config_paginas WHERE PAG_ARQUIVO = '{$path_pagina}'")[0];
            $pagina_codigo              = $paginas['PAG_CODIGO'];
            $pagina_titulo              = $paginas['PAG_TITULO'];
            $pagina_modulo_permissao    = $paginas['PAG_MODULO_PERMISSAO'];
            $pagina_modulo              = $paginas['PAG_MODULO'];
            $pagina_text_singular       = $paginas['PAG_TEXT_SINGULAR'];
            $pagina_text_plural         = $paginas['PAG_TEXT_PLURAL'];
            $pagina_icone               = $paginas['PAG_ICONE'];
            $pagina_singular            = $paginas['PAG_SINGULAR'];
            $pagina_plural              = $paginas['PAG_PLURAL'];

            /* Variaveis da Página Principal */
                $primary_database_page_check            = "true";
                $primary_database_page_tabela           = "config_atualizacoes_tipos";
                $primary_database_page_collumn_prefix   = "TIPO";
                $primary_database_page_collumn_id       = "TIPO_CODIGO";
                $primary_database_page_upload           = $pagina_singular;
                $primary_database_page_ajax             = "ajax/ajax.alterarPro".maiuscula($pagina_plural)."tipos.php";
                $primary_database_page_backlink         = "consultarPro" . maiuscula($pagina_plural). "tipos.php?active=".$pagina_plural."";

            /* Variaveis da Página Secundária */
                $secondary_database_page_check            = "false";
            
            /*** Configurações Básicas ***/
                /* Geração de Código Único para cadastro*/
                $database_codigo       = $_GET[$pagina_singular];

            /*** Consulta Padrão/Personalizadas ***/
                $database_consulta = "SELECT * FROM ".$primary_database_page_tabela." ";
                    /***
                     *  Insert Complements Busca Principal  
                    ***/
                    // $database_consulta .= " ";
                $database_consulta .= " WHERE ".$primary_database_page_collumn_id." = ".$database_codigo."";
                $database_consulta = newsql($database_consulta)[0];
?>

<?php require_once("includes/header.php"); ?>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <span class="text-semibold">
                        <a href="index.php">Dashboard</a>
                    </span> 
                    <i class="icon-arrow-right6"></i> 
                    <strong><?php echo $pagina_titulo; ?></strong><br>
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo $primary_database_page_backlink; ?>" class="btn btn-link btn-float has-text">
                        <i class="icon-database-arrow text-default"></i> 
                        <span>Consultar <?php echo maiuscula($pagina_text_plural); ?></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li>
                    <a href="index.php">
                        <i class="icon-home2 position-left"></i> 
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $primary_database_page_backlink; ?>">
                        <i class="icon-database-arrow position-left"></i> 
                        <span>Consultar <?php echo maiuscula($pagina_text_plural); ?></span>
                    </a>
                </li>
                <li>
                    <i class="<?php echo $pagina_icone; ?>"></i> 
                    <span><?php echo $pagina_titulo; ?></span>
                </li>
            </ul>
            <ul class="breadcrumb-elements">
                <!-- Modal Ajuda -->
                <?php require_once("includes/ajuda.php"); ?>
            </ul>
        </div>
    </div>
    <div class="content">

        <div class="row">
            <div calss="col-lg-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"><i class="icon-database-add"></i> <strong>Informações para Cadastro</strong></h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a id="clean_form" href="header('Refresh:0');" data-action="reload"></a></li>
                            </ul>
                        </div>
                        <p class="content-group" style="margin: 0!important">Siga as etapas e passos até o final.</p>
                    </div> 
                    <div class="panel-body"> 
                        <form id="FormDefault" class="stepy-default" method="post" onsubmit="return false">
                            <input 
                                type="hidden" 
                                name="form_page" 
                                value="<?php echo $path_pagina; ?>"
                            >
                            <input 
                                type="hidden" 
                                name="form_table" 
                                value="<?php echo $primary_database_page_tabela; ?>"
                            >
                            <input 
                                type="hidden" 
                                name="form_id" 
                                value="<?php echo $database_codigo; ?>"
                            >
                            <input 
                                type="hidden" 
                                name="form_prefix" 
                                value="<?php echo $primary_database_page_collumn_prefix; ?>"
                            >
                            
                            
                            <fieldset title="1">
                                <legend class="text-semibold">Campos Padrões</legend>
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Configurações Padrões</legend>
                                                    
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Visibilidade</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-database4"></i></span>
                                                                        <select 
                                                                            id="visibilidade<?php echo $database_codigo; ?>"
                                                                            class="select insert_field" 
                                                                            type="" 
                                                                            name="visibilidade<?php echo $database_codigo; ?>"
                                                                            title="Indique a visibilidade"
                                                                            placeholder="Selecione o tipo de visibilidade"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_VISIBILIDADE" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Selecione o tipo de visibilidade" 
                                                                            required="true"
                                                                        >
                                                                            <option value="0">Selecionar...</option> 
                                                                            <option <?php if($database_consulta["TIPO_VISIBILIDADE"] == 'true') echo 'selected'; ?> value="true">Ativo</option> 
                                                                            <option <?php if($database_consulta["TIPO_VISIBILIDADE"] == 'false') echo 'selected'; ?> value="false">Inativo</option> 
                                                                        </select>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Título</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-cube4"></i></span>
                                                                        <input 
                                                                            id=""
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="titulo<?php echo $database_codigo; ?>" 
                                                                            title="Insira o título obrigatório" 
                                                                            placeholder="Informar o título"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_TITULO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Insira o título obrigatório"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['TIPO_TITULO']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <button type="button" class="btn btn-default stepy-finish">Atualização Automática <i class="fa fa-save position-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <script type="text/javascript">
            var ADMIN                   = "<?php echo ADMIN; ?>";
            var database_page_codigo    = "<?php echo $database_codigo; ?>";
            var database_page_upload    = "<?php echo $primary_database_page_upload; ?>";
            var database_page_usuario   = "<?php echo $cadastro_usuario; ?>";
            var database_page_tabela    = "<?php echo $primary_database_page_tabela; ?>";
        </script>
        <?php /* Incluir Upload Unico */ require_once("includes/upload-unico.php"); ?>
        <?php /* Incluir Uploads de Anexos Gerais */ require_once("includes/upload.php"); ?>
        <?php /* Incluir Campo Text Personalizado */ require_once("includes/field-text.php"); ?>
        <?php /* Incluir Notificação Padrão de Anexos Gerais */ require_once("includes/notificacao-padrao.php"); ?>
        <?php /* Incluir Credits */ require_once("includes/copyright.php"); ?>

    </div>
</div>

<script type="text/javascript">
// Clean Data Form
$('#FormDefault')[0].reset();
$('#clean_form').on('click', function () { $('#FormDefault')[0].reset(); });

$(".stepy-default").stepy({
    next: function(index) { 
        if(index == 5){
            getUploads('<?php echo $primary_database_page_upload; ?>', '<?php echo $database_codigo; ?>', '<?php echo $primary_database_page_upload; ?>-uploads', '.arquivos-box');
        }
    },
    back: function(index) { 
        if(index == 5){
            getUploads('<?php echo $primary_database_page_upload; ?>', '<?php echo $database_codigo; ?>', '<?php echo $primary_database_page_upload; ?>-uploads', '.arquivos-box');
        }
    },
    finish: function() {
        return false;
    }
});



</script>
<?php 
    /* Registrar Informações Individualmente */ require_once("includes/form-individual.php");
    /* Incluir Footer */ require_once("includes/footer.php");
    } }
?>