<?php
require_once("config.php");
require_once("PluploadHandler.php");

$type = false;
$cod = intval($_POST['id']);

$allowedTypes = [];
$upload_projeto = newsql("SELECT * FROM config_uploads_projetos");
foreach ($upload_projeto as $key => $value) {
	array_push($allowedTypes, $value['PROJ_TITULO']);
}
if(isset($_POST['type']) && in_array($_POST['type'], $allowedTypes)){$type = $_POST['type']; }
$consulta_tipos_code = newsql("SELECT PROJ_CODIGO FROM config_uploads_projetos WHERE PROJ_TITULO = '{$_POST['type']}' ")[0]['PROJ_CODIGO'];
$consulta_tipos_code = newsql("SELECT TIPO_CODIGO FROM config_uploads_projetos_tipos WHERE TIPO_PROJETO = '{$consulta_tipos_code}' ")[0]['TIPO_CODIGO'];

$allow_Extensions = "";
$upload_extensoes = newsql("SELECT * FROM config_uploads_extensoes");
foreach ($upload_extensoes as $key => $value) {
	if($key == 0){ $allow_Extensions .= $value['EXT_EXTENSAO']; } 
	else { $allow_Extensions .= "," . $value['EXT_EXTENSAO']; }
}


$ph = new PluploadHandler(array(
	'target_dir' => '../uploads/'.$type.'/'.$cod.'/',
	'max_file_age' => '50000',
	'max_execution_time' => '5000',
	'allow_extensions' => $allow_Extensions,
	'debug' => true,
	'log_path' => '../uploads/'.$type.'/'.$cod.'/upload.'.date("Y-m-d").'.log'
));

$ph->sendNoCacheHeaders();
$ph->sendCORSHeaders();

if ($result = $ph->handleUpload()) {
	if(newinsert("config_uploads","(UP_NOME, UP_EXTENSAO, UP_TABELA, UP_COD_REG, UP_CAMINHO, UP_DESTAQUE, UP_TIPO) values ('".$result['name']."', '".$result['extension']."', '{$type}', '{$cod}', '".$result['path']."', 'false', '{$consulta_tipos_code}')")){ 
		die(json_encode(array('resposta' => 'true', 'name' => $result['name'], 'path' => $result['path'], 'extension' => $result['extension'], 'size' => $result['size'], 'error_code' => '', 'error_message' => ''))); 
	} else { die(json_encode(array('resposta' => 'false', 'name' => $result['name'], 'path' => $result['path'], 'extension' => $result['extension'], 'size' => $result['size'], 'error_code' => '666', 'error_message' => 'Erro ao inserir upload na base de dados!'))); }
} else { die(json_encode(array('resposta' => 'false', 'name' => $result['name'], 'path' => $result['path'], 'extension' => $result['extension'], 'size' => $result['size'], 'error_code' => $ph->getErrorCode(), 'error_message' => $ph->getErrorMessage()))); }
