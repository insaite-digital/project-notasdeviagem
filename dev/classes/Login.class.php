<?php
class Login
{
    //nome da tabela para consulta
    var $tabela;
    //campos da tabela para consulta
    var $campos = array(
        'usuario'=>'USU_USUARIO',
        'senha'=>'USU_SENHA'
    );
    //dados para consultas
    var $dados = array('USU_CODIGO');
    //armazenar session?
    var $iniciaSessao = true;
    //prefixo da session
    var $prefixoChaves = 'USUARIO_';
    //armazenar cookie?
    var $cookie = true;
    //armazena mensagens de erro
    var $erro = '';

    //metodo para validar usuario
    function ValidaUsuario($usuario, $senha) {
        $senha = $this->__codificaSenha($senha);

        $sql = "SELECT * FROM {$this->tabela} WHERE {$this->campos['usuario']} = '{$usuario}' AND {$this->campos['senha']} = '{$senha}'";
    
        $query = mysqli_query($GLOBALS["conexao"], $sql);
        $arr = mysqli_fetch_array($query);
        if(!empty($arr)){ $total = mysqli_data_seek($query, 0); } 
        else{ return false; }

        return ($total == 1) ? true : false;
    }

    //loga usuario salvando dados na sessao
    function LogaUsuario($usuario, $senha) {


        if ($this->ValidaUsuario($usuario, $senha)) {
            $senha = $this->__codificaSenha($senha);
            

            //inicia a sessao caso não estivar iniciada
            if ($this->iniciaSessao AND !isset($_SESSION)) {
                session_start();
            }

            
            //traz os dados da tabela
            if ($this->dados != false) {
                
                
                //adiciona o campo usuario(da tabela) no array dados
                if (!in_array($this->campos['usuario'], $this->dados)) {
                    $this->dados[] = 'USU_USUARIO';
                }

                //monta formato SQL da lista de campos
                $dados = '`' . join('`, `', array_unique($this->dados)) . '`';

                //consulta dados
                $sql = "SELECT {$dados}
                        FROM {$this->tabela}
                        WHERE
                        {$this->campos['usuario']} = '{$usuario}' and {$this->campos['senha']} = '{$senha}'";


                $query = mysqli_query($GLOBALS["conexao"], $sql);
                
                //se a query falhou
                if (!$query) {
                    //consulta mal sucedida
                    $this->erro = 'Consulta dos dados é inválida';
                    return false;
                } else {
               
                    $sqlUs = "SELECT * FROM config_usuarios as usuario INNER JOIN config_perfis as perfil ON perfil.PER_CODIGO = usuario.USU_PERFIL WHERE {$this->campos['usuario']} = '{$usuario}' and {$this->campos['senha']} = '{$senha}'";
                    $qr = mysqli_query($GLOBALS["conexao"],$sqlUs);
                    $arr = mysqli_fetch_array($qr);
    
                    if ($arr['USU_BLOQUEADO'] == 'true') {
                        $this->erro = 'Usuário Bloqueado, consulte o suporte técnico.';
                        return false;  
                    } else {
                    
                        //traz dados para um array
                        $dados = mysqli_fetch_assoc($query);
                        //limpa a consulta da memoria
                    
                        if(!$dados) return false;
                        //passa os dados para a sessao
                        foreach ($dados as $chave=>$valor) {
                            $_SESSION[$this->prefixoChaves . 'USERNAME'] = $valor;
                        }
                        $_SESSION[$this->prefixoChaves . 'CODIGO'] = $arr['USU_CODIGO'];
                        $_SESSION[$this->prefixoChaves . 'PERFIL'] = $arr['PER_CODIGO'];
                        $_SESSION[$this->prefixoChaves . 'MASTER'] = $arr['PER_MASTER'];
                    }
                }
            }

            //Usuario Logado
            $_SESSION[$this->prefixoChaves . 'LOGADO'] = 'true';
            
            //define um cookie para maior seguranca do user?
            if ($this->cookie) {
                //Monta uma cookie com informações gerais sobre o usuário: usuario, ip e navegador
                $valor = join('#', array($usuario, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));

                //encripta o valor do cookie
                $valor = sha1($valor);

                setcookie($this->prefixoChaves . 'token', $valor, 0, '/');
            }
            return true;
        } else {
            $this->erro = 'Usuário Inválido';
            return false;
        }
    }//fim do metodo logausuario

    //verifica se ha um usuário logado
    function UsuarioLogado() {

        //inicia sessao caso ainda nao esteja iniciada
        if ($this->iniciaSessao AND !isset($_SESSION)) {
            session_start();
        }

        //verifica se nao existe valor na sessao
        if (!isset($_SESSION[$this->prefixoChaves . 'LOGADO']) OR !$_SESSION[$this->prefixoChaves . 'LOGADO']) {
            return false;
        }
        
        //faz verificação do cookie
        if ($this->cookie) {
            if (!isset($_COOKIE[$this->prefixoChaves . 'token'])) {
                return false;
            } else {
                //monta o valor do cookie
                $valor = join('#', array($_SESSION[$this->prefixoChaves . 'usuario'], $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));

                //encripta o valor do cookie
                $valor = sha1($valor);

                //verifica valor do cookie
                if ($_COOKIE[$this->prefixoChaves . 'token'] != $valor) {
                    return false;
                }
                return true;
            }
        }
        
    }//fim do metodo usuarioLogado

    //metodo para logout
    function Logout() {
        //inicia a sessaio caso nao estiver iniciada
        if ($this->iniciaSessao AND !isset($_SESSION)) {
            session_start();
        }

        //tamanho do prefixo
        $tamanho = strlen($this->prefixoChaves);

        //destroi todos os valores
        foreach ($_SESSION as $chave=>$valor) {
            //remove as chaves que comecem com o prefixo
            if (substr($chave, 0, $tamanho) == $this->prefixoChaves) {
                unset($_SESSION[$chave]);
            }
        }

        //destroi asessao se a estiver vazia
        if (count($_SESSION) == 0) {
            session_destroy();
        }

        //remove o cookie da sessao se ainda existir
        if (isset($_COOKIE['PHPSESSID'])) {
            setcookie('PHPSESSID', false, (time() - 3600));
            unset($_COOKIE['PHPSESSID']);
        }

        //remove o cookie com as informações do vizitante
        if ($this->cookie AND isset($_COOKIE[$this->prefixoChaves . 'token'])) {
            setcookie($this->prefixoChaves . 'token', false, (time - 3600), '/');
            unset($_COOKIE[$this->prefixoChaves . 'token']);
        }
        //retorna se não ha um usuário logado
        return !$this->UsuarioLogado();
    }//fim do metodo logout

    function __codificaSenha($senha) {
        //return sha1($senha);
        $c = '94';
        $salt = 'DiGiTalInsaiTe';
        $hash = md5($salt.$senha.$c);
        return $hash;
    }//fim do metodo de codificar senha

}//fim da class login