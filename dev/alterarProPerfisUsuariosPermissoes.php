<?php 
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        header('Location: error.php');
    } else {
        $path_pagina = pathinfo( __FILE__ )['basename'];
        $acesso->Pagina = $path_pagina;
        $acesso->verificaPermissao();
        $resposta = $acesso->getResposta();
        if(empty($resposta) || $resposta == 'false'){ header('Location: direcionamento.php'); } else {
            date_default_timezone_set('America/Sao_Paulo');
            $cadastro_time      = date("Y-m-d H:i:s");
            $cadastro_usuario   = $_SESSION['USUARIO_CODIGO'];
            insert_logs($path_pagina,"");

            /* Variaveis da Página Database */
            $paginas                    = newsql("SELECT * FROM config_paginas WHERE PAG_ARQUIVO = '{$path_pagina}'")[0];
            $pagina_codigo              = $paginas['PAG_CODIGO'];
            $pagina_titulo              = $paginas['PAG_TITULO'];
            $pagina_modulo_permissao    = $paginas['PAG_MODULO_PERMISSAO'];
            $pagina_modulo              = $paginas['PAG_MODULO'];
            $pagina_text_singular       = $paginas['PAG_TEXT_SINGULAR'];
            $pagina_text_plural         = $paginas['PAG_TEXT_PLURAL'];
            $pagina_icone               = $paginas['PAG_ICONE'];
            $pagina_singular            = $paginas['PAG_SINGULAR'];
            $pagina_plural              = $paginas['PAG_PLURAL'];

            /* Variaveis da Página Secundária */
                $secondary_database_page_check          = "true";
                $secondary_database_page_tabela         = "config_usuarios";
                $secondary_database_page_collumn_prefix = "USU";
                $secondary_database_page_collumn_id     = "USU_CODIGO";
                $secondary_get_id                       = $_GET['usuario'];
                $secondary_database_page_backlink       = "consultarPro" . maiuscula($pagina_plural). ".php?active=".$pagina_plural;
                $secondary_database                     = newsql("SELECT * FROM " . $secondary_database_page_tabela . " WHERE " . $secondary_database_page_collumn_id . " = " . $secondary_get_id)[0];

            /* Variaveis da Página Principal */
                $primary_database_page_check            = "true";
                $primary_database_page_tabela           = "config_usuarios";
                $primary_database_page_collumn_prefix   = "USU";
                $primary_database_page_collumn_id       = "USU_CODIGO";
                $primary_database_page_upload           = $pagina_singular;
                $primary_database_page_ajax             = "ajax/ajax.alterarProPerfis".maiuscula($pagina_plural)."Permissoes.php";
                $primary_database_page_backlink         = "consultar" . maiuscula($pagina_plural). ".php?active=".$pagina_plural."";
            
            /*** Configurações Básicas ***/
                /* Geração de Código Único para cadastro*/
                $database_codigo       = $_GET[$pagina_singular];

            /*** Consulta Padrão/Personalizadas ***/
                $database_consulta = "SELECT * FROM ".$primary_database_page_tabela." ";
                    /***
                     *  Insert Complements Busca Principal  
                    ***/
                    // $database_consulta .= " ";
                $database_consulta .= " WHERE ".$primary_database_page_collumn_id." = ".$database_codigo."";
                $database_consulta = newsql($database_consulta)[0];
?>

<?php require_once("includes/header.php"); ?>

<script type="text/javascript" src="_template/assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="_template/assets/js/plugins/forms/styling/switch.min.js"></script>
<!-- <script type="text/javascript" src="_template/assets/js/plugins/forms/styling/switchery.min.js"></script> -->
<script type="text/javascript" src="_template/assets/js/pages/form_checkboxes_radios.js"></script>

<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <span class="text-semibold">
                        <a href="index.php">Dashboard</a>
                    </span> 
                    <i class="icon-arrow-right6"></i> 
                    <strong><?php echo $pagina_titulo; ?></strong>
                    <br>
                    <i class="icon-circle-left2"></i> 
                    <a href="javascript:history.back()"><span class="text-default">Usuário:</span> <strong><?php echo $secondary_database['USU_NOME']; ?></strong></a>
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="javascript:history.back()" class="btn btn-link btn-float has-text">
                        <i class="icon-database-arrow text-default"></i> 
                        <span>Página Anterior</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li>
                    <a href="index.php">
                        <i class="icon-home2 position-left"></i> 
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <i class="<?php echo $pagina_icone; ?>"></i> 
                    <span><?php echo $pagina_titulo; ?></span>
                </li>
            </ul>
            <ul class="breadcrumb-elements">
                <!-- Modal Ajuda -->
                <?php require_once("includes/ajuda.php"); ?>
            </ul>
        </div>
    </div>
    <div class="content">

        <div class="row">
            <div calss="col-lg-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"><i class="icon-database-add"></i> <strong>Informações para Configuração</strong></h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a id="clean_form" href="header('Refresh:0');" data-action="reload"></a></li>
                            </ul>
                        </div>
                        <p class="content-group" style="margin: 0!important">Siga as etapas e passos até o final.</p>
                    </div> 
                    <div class="panel-body">


                        <form method="post" class="FormPermissoes" onsubmit="return false">
                            <input 
                                type="hidden" 
                                name="form_page" 
                                value="<?php echo $path_pagina; ?>"
                            >
                            <input 
                                type="hidden" 
                                name="form_table" 
                                value="<?php echo $primary_database_page_tabela; ?>"
                            >
                            <input 
                                type="hidden" 
                                name="form_id" 
                                value="<?php echo $database_codigo; ?>"
                            >
                            <input 
                                type="hidden" 
                                name="form_prefix" 
                                value="<?php echo $primary_database_page_collumn_prefix; ?>"
                            >
                            <input type="hidden" name="codigo" value="<?php echo $secondary_get_id; ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="tabbable tab-content-bordered">
                                        <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
                                            <?php 
                                                $sql_modulos = newsql("SELECT * FROM config_modulos");
                                                foreach ($sql_modulos as $key => $value) {
                                                    $modulo_codigo  = $value['MOD_CODIGO'];
                                                    $modulo_titulo  = $value['MOD_TITULO'];
                                                    if($key == 0){ $active = 'active'; } 
                                                    else { $active = ''; }
                                            ?>
                                            <li class="<?php echo $active; ?>"><a href="#bordered-justified-tab<?php echo $modulo_codigo; ?>" data-toggle="tab"><?php echo $modulo_titulo; ?></a></li>
                                            <?php } ?>
                                        </ul>
                                        <div class="tab-content">
                                            <?php 
                                                $sql_modulos = newsql("SELECT * FROM config_modulos");
                                                foreach ($sql_modulos as $key => $value) {
                                                    $modulo_codigo = $value['MOD_CODIGO'];
                                                    $modulo_titulo = $value['MOD_TITULO'];
                                                    if($key == 0){ $active = 'active'; } 
                                                    else { $active = ''; }
                                            ?>
                                            <div class="tab-pane has-padding <?php echo $active; ?>" id="bordered-justified-tab<?php echo $modulo_codigo; ?>">
                                                <div class="row">
                                                <?php 
                                                    $sql_permissoes = newsql("SELECT * FROM config_permissoes");
                                                    foreach ($sql_permissoes as $key => $value) {
                                                        $permissao_codigo = $value['PERM_CODIGO'];
                                                        $permissao_titulo = $value['PERM_TITULO'];
                                                ?>
                                                    <div class="col-lg-3 col-md-6">
                                                        <div class="panel panel-body">
                                                            <div class="media-header"><h5 style="font-weight: bold;"><?php echo $permissao_titulo; ?></h5></div>
                                                            <div class="media">
                                                                <div class="media-body">
                                                                    <?php 
                                                                        $sql_modulo_permissao = newsql("SELECT * FROM config_modulos_permissoes WHERE MODPERM_PERMISSAO = '{$permissao_codigo}' AND MODPERM_MODULO = '{$modulo_codigo}'");
                                                                        foreach ($sql_modulo_permissao as $key => $value) {
                                                                            $modperm_codigo = $value['MODPERM_CODIGO'];
                                                                            $modperm_titulo = $value['MODPERM_TITULO'];

                                                                            $usuario_permissao = newsql("SELECT * FROM config_usuarios_permissoes WHERE USUPERM_USUARIO = '{$codigo}' AND USUPERM_MODULO_PERMISSAO = '{$modperm_codigo}'");
                                                                    ?>
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            <h6 class="media-heading"><strong><?php echo $modperm_titulo; ?></strong></h6>
                                                                        </div>
                                                                        <div class="col-md-12 text-center">
                                                                            <div class="checkbox checkbox-switch"><label><input name="permissao[<?php echo $modperm_codigo; ?>]" <?php if(!empty($usuario_permissao)) echo 'checked'; ?> type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="Habilitado" data-off-text="Desabilitado" class="switch permissao" data-titulo="<?php echo $modperm_titulo; ?>"></label></div>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                </div> 
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary pnotify-save">Atualizar Permissões <i class="fa fa-save position-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>


                        
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var ADMIN                   = "<?php echo ADMIN; ?>";
            var database_page_codigo    = "<?php echo $database_codigo; ?>";
            var database_page_upload    = "<?php echo $primary_database_page_upload; ?>";
            var database_page_usuario   = "<?php echo $cadastro_usuario; ?>";
            var database_page_tabela    = "<?php echo $primary_database_page_tabela; ?>";
        </script>
        <?php /* Incluir Upload Unico */ require_once("includes/upload-unico.php"); ?>
        <?php /* Incluir Uploads de Anexos Gerais */ require_once("includes/upload.php"); ?>
        <?php /* Incluir Campo Text Personalizado */ require_once("includes/field-text.php"); ?>
        <?php /* Incluir Notificação Padrão de Anexos Gerais */ require_once("includes/notificacao-padrao.php"); ?>
        <?php /* Incluir Credits */ require_once("includes/copyright.php"); ?>

    </div>
</div>

<script type="text/javascript">
$(".switch").bootstrapSwitch();

$(document).on('submit', 'form.FormPermissoes', function() {  
    var pagina = "<?php echo $path_pagina; ?>";
    var url_ajax = "<?php echo $primary_database_page_ajax; ?>";
    var usuario = "<?php echo $secondary_get_id; ?>";

    $.blockUI({ 
        message: '<i class="icon-spinner4 spinner"></i>',
        timeout: 8000,
        overlayCSS: {
            backgroundColor: '#1b2024',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            color: '#fff',
            padding: 0,
            backgroundColor: 'transparent'
        }
    });

    var percent = 0;
    var notice = new PNotify({
        text: "Verificando...",
        addclass: 'bg-primary',
        type: 'info',
        icon: 'icon-spinner4 spinner',
        hide: false,
        buttons: {
            closer: false,
            sticker: false
        },
        opacity: .9,
        width: "170px"
    });

    $.ajax({
        type        : 'POST', 
        url         : url_ajax + '?usuario='+usuario,
        data        : $(this).serialize(),
        dataType    : 'json', 
        beforeSend: function() { },
        success: function (data) {
            var resposta = data.resposta;
            setTimeout(function() {
                notice.update({
                    title: false
                });

                var interval = setInterval(function() {
                    percent += 10;
                    var options = { text: percent + "% verificado." };
                    if (percent == 30){ options.title = "Quase Lá"; }
                    if (percent >= 100) {
                        if(resposta == 'true'){
                            window.clearInterval(interval);
                            options.title = "Feito! Atualizando...";
                            options.addclass = "bg-success";
                            options.type = "success";
                            options.hide = true;
                            options.buttons = {
                                closer: true,
                                sticker: true
                            };
                            options.icon = 'icon-checkmark3';
                            options.opacity = 1;
                            options.width = PNotify.prototype.options.width;
                            setTimeout(function() {
                                location.reload();
                            }, 1000);
                        } else {
                            window.clearInterval(interval);
                            options.title = "Não foi possível salvar!";
                            options.addclass = "bg-danger";
                            options.type = "danger";
                            options.hide = true;
                            options.buttons = {
                                closer: true,
                                sticker: true
                            };
                            options.icon = 'icon-cross2';
                            options.opacity = 1;
                            options.width = PNotify.prototype.options.width;
                            setTimeout(function() {
                                $('.pnotify-save').removeAttr('disabled');
                            }, 1000);
                        }
                    }
                    notice.update(options);
                }, 120);
            }, 2000);
        }
    })

});

// Clean Data Form
$('#clean_form').on('click', function () { $('#FormDefault')[0].reset(); });

$(".stepy-default").stepy({
    next: function(index) { 
        if(index == 5){
            getUploads('<?php echo $primary_database_page_upload; ?>', '<?php echo $database_codigo; ?>', '<?php echo $primary_database_page_upload; ?>-uploads', '.arquivos-box');
        }
    },
    back: function(index) { 
        if(index == 5){
            getUploads('<?php echo $primary_database_page_upload; ?>', '<?php echo $database_codigo; ?>', '<?php echo $primary_database_page_upload; ?>-uploads', '.arquivos-box');
        }
    },
    finish: function() {
        return false;
    }
});



</script>
<?php 
    /* Registrar Informações Individualmente */ require_once("includes/form-individual.php");
    /* Incluir Footer */ require_once("includes/footer.php");
    } }
?>