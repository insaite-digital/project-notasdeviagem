<?php 
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        header('Location: error.php');
    } else {
        $path_pagina = pathinfo( __FILE__ )['basename'];
        $acesso->Pagina = $path_pagina;
        $acesso->verificaPermissao();
        $resposta = $acesso->getResposta();
        if(empty($resposta) || $resposta == 'false'){ header('Location: direcionamento.php'); } else {
            date_default_timezone_set('America/Sao_Paulo');
            $cadastro_time      = date("Y-m-d H:i:s");
            $cadastro_usuario   = $_SESSION['USUARIO_CODIGO'];
            insert_logs($path_pagina,"");

            /* Variaveis da Página Database */
            $paginas                    = newsql("SELECT * FROM config_paginas WHERE PAG_ARQUIVO = '{$path_pagina}'")[0];
            $pagina_codigo              = $paginas['PAG_CODIGO'];
            $pagina_titulo              = $paginas['PAG_TITULO'];
            $pagina_modulo_permissao    = $paginas['PAG_MODULO_PERMISSAO'];
            $pagina_modulo              = $paginas['PAG_MODULO'];
            $pagina_text_singular       = $paginas['PAG_TEXT_SINGULAR'];
            $pagina_text_plural         = $paginas['PAG_TEXT_PLURAL'];
            $pagina_icone               = $paginas['PAG_ICONE'];
            $pagina_singular            = $paginas['PAG_SINGULAR'];
            $pagina_plural              = $paginas['PAG_PLURAL'];
            
            /* Variaveis da Página Principal */
                $primary_database_page_delete           = "true";
                $primary_database_page_check            = "true";
                $primary_database_page_tabela           = "config_atualizacoes";
                $primary_database_page_collumn_prefix   = "ATU";
                $primary_database_page_collumn_id       = "ATU_CODIGO";
                $primary_database_page_upload           = $pagina_singular;
                $primary_database_page_ajax             = "ajax/ajax.cadastrarPro".maiuscula($pagina_plural).".php";
                $primary_database_page_backlink         = "consultarPro" . maiuscula($pagina_plural). ".php?active=".$pagina_plural."";

            /* Variaveis da Página Secundária */
                $secondary_database_page_check          = "false";
            
            /*** Configurações Básicas ***/
                /* Geração de Código Único para cadastro*/
                $database_codigo       = newsql("SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '".$primary_database_page_tabela."'")[0]['AUTO_INCREMENT'];
                
            /*** Consulta Padrão/Personalizadas ***/
                $database_consulta = "SELECT * FROM ".$primary_database_page_tabela." ";
                    /***
                     *  Insert Complements Busca Principal  
                    ***/
                    // $database_consulta .= " ";
                $database_consulta .= " WHERE ".$primary_database_page_collumn_prefix."_CHECK = 'true'";
                $database_consulta = newsql($database_consulta);
?>
<?php require_once("includes/header.php"); ?>

<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <!-- Página Título -->
                    <span class="text-semibold">
                        <i class="icon-circle-left2"></i> 
                        <a href="index.php">Dashboard</a>
                    </span> 
                    <br>
                    <i class="icon-database4"></i> 
                    <strong>Listagem de <?php echo $pagina_text_plural; ?></strong><br>
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <!-- Página Links de Ações -->
                    <a href="cadastrarPro<?php echo maiuscula($pagina_plural); ?>.php?active=<?php echo $pagina_plural; ?>" class="btn btn-link btn-float has-text">
                        <i class="icon-database-add text-primary"></i> 
                        <span>Novo(a) <?php echo $pagina_text_singular; ?></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <!-- Página Breadcrumbs -->
                <li>
                    <a href="index.php">
                        <i class="icon-home2 position-left"></i> 
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <i class="icon-database4"></i> 
                    <span>Listagem de <?php echo $pagina_text_plural; ?></span>
                </li>
            </ul>
            <ul class="breadcrumb-elements">
                <!-- Modal Ajuda -->
                <?php require_once("includes/ajuda.php"); ?>
            </ul>
        </div>
    </div>

    <div class="content">
        <?php if(!empty($database_consulta)){ ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-flat" style="padding: 10px 20px;">
                        <table class="table datatable-highlight">
                            <thead>
                                <tr class="bg-slate-400">
                                    <?php if($primary_database_page_delete == 'true'){ ?>
                                    <th><a data-popup="tooltip" title="Remover selecionados" onclick="deleteAll('<?php echo $primary_database_page_tabela; ?>', '<?php echo $primary_database_page_collumn_id; ?>');" id="delete_all" href="javascript:void(0);" class="text-white"><i class="icon-cancel-circle2"></i></a></th>
                                    <?php } else { ?>
                                    <th style="width: 30px;"><i class="icon-blocked"></i></th>
                                    <?php } ?>
                                    
                                    <th style="width: 100px;"><strong>#</strong></th>
                                    <th><strong>Data & Hora</strong></th>
                                    <th><strong>Versão</strong></th>
                                    <th><strong>Descrição</strong></th>
                                    <th><strong>Visibilidade</strong></th>
                                    <th style="width: 200px;" class="text-center text-muted text-white"><i class="text-white icon-checkmark3"></i> Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($database_consulta as $key => $value) {
                                        $base_codigo            = $value['ATU_CODIGO'];
                                        $base_cadastro_usuario  = $value['ATU_CADASTRO_USUARIO'];
                                        $base_user              = newsql("SELECT * FROM config_usuarios WHERE USU_CODIGO = '{$base_cadastro_usuario}'")[0];
                                        $base_cadastro_time     = $value['ATU_CADASTRO_TIME'];
                                        $base_visibilidade      = $value['ATU_VISIBILIDADE'];
                                        $base_versao            = $value['ATU_VERSAO'];
                                        $base_descricao         = $value['ATU_DESCRICAO'];
                                ?>
                                <tr class="registro_line<?php echo $base_codigo; ?>">
                                    <td data-popup="tooltip" title="Marque para remover"><input <?php if($primary_database_page_delete != 'true') echo 'disabled'; ?> type="checkbox" name="delete[]" value="<?php echo $base_codigo; ?>" class="styled input_delete"></td>
                                    
                                    <td style="width: 100px;" data-popup="tooltip" title="Código único"><strong><?php echo $base_codigo; ?></strong></td>
                                    <td style="position: relative;" data-popup="tooltip" title="Cadastro por: <?php echo $base_user['USU_NOME']; ?>">
                                        <strong><?php echo date("d/m/Y H:i", strtotime($base_cadastro_time)); ?></strong></strong>
                                    </td>
                                    <td data-popup="tooltip" title="Versão">
                                        <?php echo $base_versao; ?>
                                    </td>
                                    <td data-popup="tooltip" title="Título identificador">
                                        <?php echo $base_descricao; ?>
                                        <?php 
                                            // Buscar Uploads Padrão - Alterar Código Final
                                            $config_uploads_projetos = newsql("SELECT PROJ_CODIGO FROM config_uploads_projetos WHERE PROJ_TITULO = '{$primary_database_page_upload}'")[0]['PROJ_CODIGO'];
                                            $config_uploads_projetos = newsql("SELECT TIPO_CODIGO FROM config_uploads_projetos_tipos WHERE TIPO_PROJETO = '{$config_uploads_projetos}'")[0]['TIPO_CODIGO'];
                                            $uploads = get_uploads($primary_database_page_upload, $base_codigo, 72);
                                            if(!empty($uploads)){
                                                foreach ($uploads as $chave => $valor) {
                                                    $extensao = $valor['UP_EXTENSAO'];
                                                    
                                                    $sql_ext = newsql("SELECT * FROM config_uploads_extensoes WHERE EXT_EXTENSAO = '{$extensao}'");
                                                    $arquivo = capture_alluploads($valor);
                                                    if(!empty($sql_ext)){
                                                        $up_icon = $sql_ext[0]['EXT_ICONE'];
                                                    } else {
                                                        $up_icon = "icon-file-empty2";
                                                    }
                                                    echo "<a href='" . $arquivo . "' title='Visualizar Arquivo' target='_blank'><i style='float: right; margin-right:3px;' class='" . $up_icon . "'></i></a>";
                                                }
                                            }
                                        ?>
                                    </td>
                                    <td data-popup="tooltip" title="Visibilidade na Plataforma">
                                        <?php if($base_visibilidade == 'true'){ ?>
                                        <span style="margin-bottom: 5px;" class="label bg-success">Ativo</span>
                                        <?php } else if($base_visibilidade == 'false'){ ?>
                                        <span style="margin-bottom: 5px;" class="label bg-danger">Inativo</span>
                                        <?php } ?>
                                    </td>
                                    <td style="width: 200px;" class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right" style="width: 250px;">
                                                    <li class="dropdown-header">Manutenção</li>
                                                    <li>
                                                        <a data-popup="tooltip" title="Visualizar Informações" data-placement="left" data-target="#modal_visualizar<?php echo $base_codigo; ?>" data-toggle="modal" class="" data-pagina="<?php echo $path_pagina; ?>" data-codigo="<?php echo $base_codigo; ?>" data-tabela="<?php echo $primary_database_page_tabela; ?>" data-coluna="<?php echo $primary_database_page_collumn_id; ?>" data-updatelink="alterar<?php echo maiuscula($pagina_plural); ?>.php?active=<?php echo $pagina_plural; ?>&<?php echo $pagina_singular; ?>=<?php echo $base_codigo; ?>">
                                                            <i class="icon-database-menu"></i> 
                                                            <span>Visualizar Informações</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a data-popup="tooltip" title="Alterar Informações" data-placement="left" href="alterarPro<?php echo maiuscula($pagina_plural); ?>.php?active=<?php echo $pagina_plural; ?>&<?php echo $pagina_singular; ?>=<?php echo $base_codigo; ?>">
                                                            <i class="icon-database-edit2"></i> 
                                                            <span>Alterar Informações</span>
                                                        </a>
                                                    </li>
                                                    <li class="dropdown-header">Administrativo</li>
                                                    <li><a style="background: red; color: #FFFFFF;" data-popup="tooltip" title="Remover Registro" data-placement="left" href="javascript:void(0);" class="remover" data-pagina="<?php echo $path_pagina; ?>" data-codigo="<?php echo $base_codigo; ?>" data-tabela="<?php echo $primary_database_page_tabela; ?>" data-coluna="<?php echo $primary_database_page_collumn_id; ?>"><i class="icon-folder-remove"></i> Excluir</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <div id="modal_visualizar<?php echo $base_codigo; ?>" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header bg-primary">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h5 class="modal-title">
                                                    Visualização rápida de <strong>Informações</strong>?<br>
                                                    Código: <strong class="text_visualizar"><?php echo $base_codigo; ?></strong>
                                                </h5>
                                            </div>
                                            <div class="modal-body">
                                                <input type="hidden" value="" name="visualizar_codigo" id="visualizar_codigo">
                                                <input type="hidden" value="" name="visualizar_pagina" id="visualizar_pagina">
                                                <input type="hidden" value="" name="visualizar_tabela" id="visualizar_tabela">
                                                <input type="hidden" value="" name="visualizar_coluna" id="visualizar_coluna">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-lg-3"><strong>Data/Hora</strong></label>
                                                            <div class="col-lg-9">
                                                                <strong><?php echo date("d/m/Y H:i", strtotime($base_cadastro_time)); ?></strong></strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr style="margin: 5px;">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-lg-3"><strong>Versão</strong></label>
                                                            <div class="col-lg-9">
                                                                <span><?php echo $base_versao; ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr style="margin: 5px;">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-lg-3"><strong>Descrição</strong></label>
                                                            <div class="col-lg-9">
                                                                <span><?php echo $base_descricao; ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                                <a id='link_alterar' href="alterarPro<?php echo maiuscula($pagina_plural); ?>.php?active=<?php echo $pagina_plural; ?>&<?php echo $pagina_singular; ?>=<?php echo $base_codigo; ?>" class="btn btn-warning">Ir para página de <strong>Alteração</strong>&nbsp;&nbsp;<i class="icon-database-edit2"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php 
                /**** 
                 * Visualizar Dados 
                ****/ 
            ?>
            
        <?php /* Remover Registro Individualmente */ require_once("includes/remove-single.php"); ?>
        <?php /* Incluir Bloco de Informações Faltando */ } else { require_once("includes/no_info.php"); } ?>
        <?php /* Remover Registros em Massa */ require_once("includes/remove-all.php"); ?>
        <?php /* Incluir Credits */ require_once("includes/copyright.php"); ?>
    </div>
</div>

<?php 
    /* Incluir Footer */ require_once("includes/footer.php");
    } }
?>