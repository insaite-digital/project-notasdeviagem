<?php
/*
*----------------------
* FUNÇÕES DEFAULTS
*----------------------
*/
    /*** Cadastro de Logs Padrões ***/
    function insert_logs($pagina, $motivo){
        $sql_pagina = newsql("SELECT * FROM config_paginas as pagina INNER JOIN config_modulos_permissoes as modulopermissao ON modulopermissao.MODPERM_CODIGO = pagina.PAG_MODULO_PERMISSAO INNER JOIN config_modulos as modulo ON modulo.MOD_CODIGO = modulopermissao.MODPERM_MODULO WHERE pagina.PAG_ARQUIVO = '{$pagina}'")[0];
        if(!empty($sql_pagina)){
            $log_usuario = $_SESSION['USUARIO_CODIGO'];
            $log_modulopermissao_codigo = $sql_pagina['MODPERM_CODIGO'];
            $log_modulo_codigo = $sql_pagina['MOD_CODIGO'];
            $log_modulopermissao_permissao = $sql_pagina['MODPERM_PERMISSAO'];
            $log_pagina_codigo = $sql_pagina['PAG_CODIGO'];
            $log_tabela = 'config_logs';
            $log_dat = date('Y-m-d');
            $log_hr = date('H:i:s');
            $log_ip = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            $log_host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
            $log_motivo = $motivo;
            $log_modulopermissao_titulo = $sql_pagina['MODPERM_TITULO'] . ' - ' . $log_motivo;
            $log_pagina_titulo = $sql_pagina['PAG_TITULO'];
            $log_modulo_titulo = $sql_pagina['MOD_TITULO'];
            newinsert($log_tabela,"(LOG_USUARIO, LOG_MODULO_PERMISSAO, LOG_MODULO, LOG_PERMISSAO, LOG_PAGINA, LOG_DESCRICAO, LOG_DATA, LOG_HORA, LOG_IP, LOG_HOST) VALUES ('{$log_usuario}', '{$log_modulopermissao_codigo}', '{$log_modulo_codigo}', '{$log_modulopermissao_permissao}', '{$log_pagina_codigo}', '{$log_modulopermissao_titulo}', '{$log_dat}', '{$log_hr}', '{$log_ip}', '{$log_host}')");
        }
    }

    function insert_logs_individual($log_tabela, $permission, $table, $id, $field, $value){
        /*
        #1   Consulta de Informações
        #2   Cadastro de Informações
        #3   Alteração de Informações
        #4   Exclusão de Informações
        */
        $cadastro_time      = date("Y-m-d H:i:s");
        $cadastro_usuario   = $_SESSION['USUARIO_CODIGO'];
        $log_ip = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $log_host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
        newinsert($log_tabela,"(LOG_PERMISSAO, LOG_TABLE, LOG_ID, LOG_FIELD, LOG_VALUE, LOG_CADASTRO_TIME, LOG_CADASTRO_USUARIO, LOG_IP, LOG_HOST) VALUES ('{$permission}', '{$table}', '{$id}', '{$field}', '{$value}', '{$cadastro_time}', '{$cadastro_usuario}', '{$log_ip}', '{$log_host}')");
    }

    /*** Validar Força da Senha ***/
    function senhaValida($senha) {
        return preg_match('/[a-z]/', $senha) // tem pelo menos uma letra minúscula
            && preg_match('/[A-Z]/', $senha) // tem pelo menos uma letra maiúscula
            && preg_match('/[0-9]/', $senha) // tem pelo menos um número
            && preg_match('/^[\w$@]{6,}$/', $senha); // tem 6 ou mais caracteres
    }

    /*** Mostrar Resultados Formatados ***/
    function print_($vl){ echo '<pre>'; print_r($vl); echo '</pre>'; }

    /*** SQL Buscar Resultados ***/
    function mysqli_result($res, $row, $field=0) {
        $res->data_seek($row);
        $datarow = $res->fetch_array();
        return $datarow[$field];
    }

    /*** SQL Adicionar Colunas in Table ***/
    function addcolunas($tabela, $field){
        $sql = "ALTER TABLE " . $tabela . " ADD COLUMN " . $field ;
        if(mysqli_query($GLOBALS["conexao"], "$sql")) return true;
        else return false; 
    }

    /*** SQL Consultar/Listar Dados ***/
    function newsql($sql){
        $qry = mysqli_query($GLOBALS["conexao"], "$sql");
        if(empty($qry)) return false;
        $dados = array();
        while($dad = mysqli_fetch_array($qry)){ $dados[] = $dad; }
        return $dados;
    }

    /*** SQL Inserir/Cadastrar Dados ***/
    function newinsert($tabela, $sql){ $sql = "INSERT INTO " . $tabela . " " . $sql ; if(mysqli_query($GLOBALS["conexao"], "$sql")) return true; else return false; }

    /*** SQL Atualizar/Alterar Dados ***/
    function newupdate($tabela, $sql){ $sql = "UPDATE " . $tabela . " SET " . $sql ; if(mysqli_query($GLOBALS["conexao"], "$sql")) return true; else return false; }

    /*** SQL Deletar Dados ***/
    function newdelete($tabela, $sql){ $sql = "DELETE FROM " . $tabela . ' ' . $sql ; if(mysqli_query($GLOBALS["conexao"], "{$sql}")) return true; else return false; }

    /*** Formatar e Validar Senha Usuários ***/
    function isValidMd5($md5 =''){ return strlen($md5) == 32 && ctype_xdigit($md5); }

/*
*----------------------
* FUNÇÕES DE FORMATAÇÕES
*----------------------
*/
    /*** Validar CNPJ ***/
    function validarCNPJ($cnpj){
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);    
        if (strlen($cnpj) != 14) return false; /* Valida CNPJ */
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++){ /* Validar Primeiro Digito Verificador */
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto)) return false;
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++){ /* Validar Segundo Dígito Verificador */
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }

    /*** Validar CPF ***/
    function validaCPF($cpf) {
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf ); /* Extrai somente os números */
        if (strlen($cpf) != 11){ return false; } /* Verifica se foi informado todos os digitos corretamente */
        if (preg_match('/(\d)\1{10}/', $cpf)) { return false; } /* Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11 */
        for ($t = 9; $t < 11; $t++) { /* Faz o calculo para validar o CPF */
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) { return false; }
        }
        return true;
    }

    /*** SQL Formatar CPF & CNPJ ***/
    function limpaCPFCNPJ($valor){ $valor = trim($valor); $valor = str_replace(".", "", $valor); $valor = str_replace(",", "", $valor); $valor = str_replace("-", "", $valor); $valor = str_replace("/", "", $valor); return $valor; }


    /*** SQL Formatar Somente Números ***/
    function limparTexto($str){ 
      return preg_replace("/[^0-9]/", "", $str); 
    }

    /* Limitar tamanho de textos */
    function limit($texto, $qtd){
      if(strlen($texto) == $qtd) $frase = $texto;
      else {
        $ps = explode(' ', $texto);
        $frase = '';
        if($qtd >= 3) {
          foreach($ps as $p){
            $c = strlen($frase);
            if($c + strlen($p) + 3 >= $qtd) {
              $frase .= '...'; // Pra mudar os tres pontos, muda em cima a soma. Se for uma seta tipo -> troca o 3 pra 2
              break;
            }
            $frase .= ' ' . $p;
          }
        }
      }
      return $frase;
    }

    function maiuscula($string) {
        $string = mb_strtolower(trim(preg_replace("/\s+/", " ", $string)));//transformo em minuscula toda a sentença
        $palavras = explode(" ", $string);//explodo a sentença em um array
        $t =  count($palavras);//conto a quantidade de elementos do array
        for ($i=0; $i <$t; $i++){ //entro em um for limitando pela quantidade de elementos do array
            $retorno[$i] = ucfirst($palavras[$i]);//altero a primeira letra de cada palavra para maiuscula
                if($retorno[$i] == "Dos" || $retorno[$i] == "De" || $retorno[$i] == "Do" || $retorno[$i] == "Da" || $retorno[$i] == "E" || $retorno[$i] == "Das"):
                    $retorno[$i] = mb_strtolower($retorno[$i]);//converto em minuscula o elemento do array que contenha preposição de nome próprio
                endif;  
        }
        return implode(" ", $retorno);
    }

    /* Padrão Palavras URL */
    function Remove_caracter($string) {
        $what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','/','=','?','~','^','>','<','ª','º' );
        $by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
        $string = str_replace($what, $by, $string);
        $string = strtolower($string);
        return $string;
    }

/*=======================================================*/
/** Funções utilizadas no projeto padrão **/
/*=======================================================*/
    /* Função enviar mensagem usando PHP Mailer */
    function sendMessage($to, $body, $subject, $from = NULL){
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        // $mail->Mailer = "mail";
        try {
            $mail->SMTPAuth   = true;
            // $mail->SMTPSecure = 'tls';
            $mail->Host       = 'smtp.siscomp.inf.br';
            $mail->Port       = '587';
            $mail->Username   = 'contato@siscomp.inf.br';
            $mail->Password   = 'Siscomp@2021*';
            $mail->AddAddress($to);
            $mail->SetFrom('contato@siscomp.inf.br');
            $mail->CharSet = 'UTF-8';
            $mail->AddReplyTo($from);
    //      $mailer->AddBCC('henriquedegregori@yahoo.com.br');
            $mail->Subject = $subject;
            $mail->MsgHTML($body);
            return $mail->Send();
        } catch (phpmailerException $e) {
            echo $e;
            return false;
        }
    }

    /* Abrir arquivo de email */
    function openFile ($file) {
        $fp = fopen($file,"r"); 
        return fread($fp,filesize($file));
    }

/*=======================================================*/
/** Funções utilizadas para buscar e configurar anexos **/
/*=======================================================*/
    function get_uploads($tipo, $codigo, $type){
        if(empty($type)){
            $sql = "SELECT * FROM config_uploads WHERE UP_TABELA = '{$tipo}' AND UP_COD_REG = '{$codigo}' ORDER BY UP_CODIGO ASC";
        } else {
            $sql = "SELECT * FROM config_uploads WHERE UP_TABELA = '{$tipo}' AND UP_COD_REG = '{$codigo}' AND UP_TIPO = '{$type}' ORDER BY UP_CODIGO ASC";
        }
        $qry = mysqli_query($GLOBALS["conexao"], "$sql") or die ($sql);
        $dados = array();
        while($dad = mysqli_fetch_array($qry)){
            $dados[] = $dad;
        }
        return $dados;
    }

    /*Pegar Imagens*/
    function capture_uploads($midia){
        $mid = SITEROOT . "uploads/".$midia[0]['UP_TABELA']."/".$midia[0]['UP_COD_REG']."/".$midia[0]['UP_NOME'];
        return $mid;
    }

    /*Pegar Imagens*/
    function capture_alluploads($midia){
        $mid = SITEROOT . "uploads/".$midia['UP_TABELA']."/".$midia['UP_COD_REG']."/".$midia['UP_NOME'];
        return $mid;
    }

/*=======================================================*/
/** Funções utilizadas para configurar datas **/
/*=======================================================*/
function datafrombanco($data){
    $part = explode('-', $data);
    $ano = $part[0];
    $mes = $part[1];
    $dia = $part[2];

    $mes_extenso = array(
        '01' => 'Janeiro',
        '02' => 'Fevereiro',
        '03' => 'Março',
        '04' => 'Abril',
        '05' => 'Maio',
        '06' => 'Junho',
        '07' => 'Julho',
        '08' => 'Agosto',
        '09' => 'Setembro',
        '10' => 'Outubro',
        '11' => 'Novembro',
        '12' => 'Dezembro',
    );
    $mes = $mes_extenso["$mes"];

    $return = $dia . ' ' . $mes . ', ' . $ano;
    return $return;   
}

/*Formatação de Data*/
function details_data($data) {
    $explode = (explode('-', $data));
    $ano = $explode[0];
    $mes = $explode[1];
    $dia = $explode[2];

    $mes_abreviado = array(
        '01' => 'Jan',
        '02' => 'Fev',
        '03' => 'Mar',
        '04' => 'Abr',
        '05' => 'Mai',
        '06' => 'Jun',
        '07' => 'Jul',
        '08' => 'Ago',
        '09' => 'Set',
        '10' => 'Out',
        '11' => 'Nov',
        '12' => 'Dez'
    );
    $mes_extenso = array(
        '01' => 'Janeiro',
        '02' => 'Fevereiro',
        '03' => 'Março',
        '04' => 'Abril',
        '05' => 'Maio',
        '06' => 'Junho',
        '07' => 'Julho',
        '08' => 'Agosto',
        '09' => 'Setembro',
        '10' => 'Outubro',
        '11' => 'Novembro',
        '12' => 'Dezembro'
    );

    $arraydata = array();
    $arraydata[0] = $dia;
    $arraydata[1] = $mes_abreviado["$mes"];
    $arraydata[2] = $ano;
    $arraydata[3] = $mes_extenso["$mes"];
    return $arraydata;
}

/*Formatação de Data*/
function format_data_extenso($data) {
    $explode = (explode(' ', $data));
    $explode_mes = (explode(',', $explode[1]));
    $explode_mes = $explode_mes[0];

    $mes_extenso = array(
        'Janeiro' => '01',
        'Fevereiro' => '02',
        'Março' => '03',
        'Abril' => '04',
        'Maio' => '05',
        'Junho' => '06',
        'Julho' => '07',
        'Agosto' => '08',
        'Setembro' => '09',
        'Outubro' => '10',
        'Novembro' => '11',
        'Dezembro' => '12'
    );

    $arraydata = array();
    $arraydata[0] = $explode[0];
    $arraydata[1] = $mes_extenso["$explode_mes"];
    $arraydata[2] = $explode[2];

    $data = $arraydata[2] . '-' . $arraydata[1] . '-' . $arraydata[0];
    return $data;
}

function data_extensa($data) {
    return $data[0] . ' de ' . $data[3] . ' de ' . $data[2];
}