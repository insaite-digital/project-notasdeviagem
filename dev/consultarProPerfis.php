<?php 
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        header('Location: error.php');
    } else {
        $path_pagina = pathinfo( __FILE__ )['basename'];
        $acesso->Pagina = $path_pagina;
        $acesso->verificaPermissao();
        $resposta = $acesso->getResposta();
        if(empty($resposta) || $resposta == 'false'){ header('Location: direcionamento.php'); } else {

            $paginas = newsql("SELECT * FROM config_paginas WHERE PAG_ARQUIVO = '{$path_pagina}'")[0];
            $pagina_codigo          = $paginas['PAG_CODIGO'];
            $pagina_titulo          = $paginas['PAG_TITULO'];
            $pagina_singular        = $paginas['PAG_SINGULAR'];
            $pagina_plural          = $paginas['PAG_PLURAL'];
            $pagina_icone           = $paginas['PAG_ICONE'];
            $pagina_text_singular   = $paginas['PAG_TEXT_SINGULAR'];
            $pagina_text_plural     = $paginas['PAG_TEXT_PLURAL'];
            // Variaveis da Página de Consulta
            $pagina_tabela          = "config_perfis";
            
            $sql_consulta = newsql("SELECT * FROM config_perfis ORDER BY PER_CODIGO DESC");
            $codigo       = newsql("SELECT AUTO_INCREMENT FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME   = '".$pagina_tabela."'")[0]['AUTO_INCREMENT'];

            insert_logs($path_pagina,"");
?>
<?php require_once("includes/header.php"); ?>

<div class="content-wrapper">
    <?php 
        // print_($sql_consulta);
    ?>
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <!-- Página Título -->
                    <span class="text-semibold">
                        <a href="index.php">Dashboard</a>
                    </span> 
                    <i class="icon-arrow-right6"></i> 
                    <strong>Configuração de Perfis</strong><br>
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <!-- Página Links de Ações -->
                    <a href="#" data-toggle="modal" data-target="#insert_perfil" class="btn btn-link btn-float has-text">
                        <i class="icon-database-add text-primary"></i> 
                        <span>Novo(a) Perfil</span>
                    </a>
                </div>

                <div id="insert_perfil" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-primary">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">
                                    Deseja cadastrar novo <strong>Perfil</strong>?<br>
                                </h5>
                            </div>
                            <hr style="margin: 0 auto;">
                            <form id="FormInsert" method="post" onsubmit="return false">
                                <input type="hidden" name="pagina" value="<?php echo $path_pagina; ?>">
                                <input type="hidden" name="tabela" value="<?php echo $pagina_tabela; ?>">
                                <input type="hidden" name="codigo" value="<?php echo $codigo; ?>">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label><strong>Título <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                        <div class="input-group" style="width: 100%;">
                                            <input id="in_titulo" name="in_titulo" class="form-control" placeholder="Insira o título do perfil" value="">
                                            <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label><strong>Descrição:</strong></label>
                                        <textarea name="in_descricao" class="form-control mb-15" rows="3" cols="1" placeholder="Descrição do Perfil:"></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary insert_perfil">Cadastrar&nbsp;&nbsp;<i class="icon-database-add"></i></button>
                                </div>
                            </form>                          
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <!-- Página Breadcrumbs -->
                <li>
                    <a href="index.php">
                        <i class="icon-home2 position-left"></i> 
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <i class="icon-stack3"></i> 
                    <span>Configuração de Módulos</span>
                </li>
            </ul>
            <ul class="breadcrumb-elements">
                <!-- Modal Ajuda -->
                <?php require_once("includes/ajuda.php"); ?>
            </ul>
        </div>
    </div>


    <div class="content">
        <?php if(!empty($sql_consulta)){ ?>
        
        <div class="row">
            <?php 
                foreach ($sql_consulta as $key => $value) {
                    $perfil_codigo      = $value['PER_CODIGO'];
                    $perfil_titulo      = $value['PER_TITULO'];
                    $perfil_master      = $value['PER_MASTER'];
                    $perfil_descricao   = $value['PER_DESCRICAO'];
            ?>

            <div class="col-sm-6 col-lg-4">
                <div class="panel">
                    <div class="panel-body">
                        <h5 class="content-group">
                            <span class="label label-flat label-rounded label-icon border-grey text-grey mr-10">
                                <i class="icon-dice"></i>
                            </span>

                            <a href="#" class="text-default">
                                <strong><?php echo $perfil_titulo; ?></strong>
                            </a>
                            <?php 
                                if($perfil_master == 'true'){
                            ?>
                            <span class="label bg-primary heading-text" style="float: right;">MASTER</span>
                            <?php } ?>
                        </h5>
                        <p class="content-group"><?php echo $perfil_descricao; ?></p>
                    </div>
                    <hr class="no-margin">
                    <div class="panel-body text-center alpha-grey">
                        <a href="consultarProPerfisUsuarios.php?active=properfis&perfil=<?php echo $perfil_codigo; ?>" class="btn bg-primary-400">
                            <i class="icon-users position-left"></i>
                            Usuários
                        </a>
                        <a href="#" data-toggle="modal" data-target="#update_perfil<?php echo $perfil_codigo; ?>" data-codigo="<?php echo $perfil_codigo; ?>" class="btn bg-warning-400">
                            <i class="icon-database-edit2 position-left"></i>
                            Editar Perfil
                        </a>
                    </div>
                </div>
            </div>

            <div id="update_perfil<?php echo $perfil_codigo; ?>" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title">
                                Deseja alterar o <strong>Perfil</strong>?<br>
                            </h5>
                        </div>
                        <hr style="margin: 0 auto;">
                        <form id="FormUpdate<?php echo $perfil_codigo; ?>" method="post" onsubmit="return false">
                            <input type="hidden" name="pagina" value="<?php echo $path_pagina; ?>">
                            <input type="hidden" name="tabela" value="<?php echo $pagina_tabela; ?>">
                            <input type="hidden" name="codigo" value="<?php echo $codigo; ?>">
                            <input type="hidden" name="up_codigo<?php echo $perfil_codigo; ?>" value="<?php echo $perfil_codigo; ?>">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label><strong>Título <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                    <div class="input-group" style="width: 100%;">
                                        <input id="up_titulo<?php echo $perfil_codigo; ?>" name="up_titulo<?php echo $perfil_codigo; ?>" class="form-control" placeholder="Insira o título do perfil" value="<?php echo $perfil_titulo; ?>">
                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label><strong>Descrição:</strong></label>
                                    <textarea name="up_descricao<?php echo $perfil_codigo; ?>" class="form-control mb-15" rows="3" cols="1" placeholder="Descrição do Perfil:"><?php echo $perfil_descricao; ?></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary update_perfil" data-codigo="<?php echo $perfil_codigo; ?>">Alterar&nbsp;&nbsp;<i class="icon-database-edit2"></i></button>
                            </div>
                        </form>                          
                    </div>
                </div>
            </div>
            <?php } ?>

        </div>
        <?php } else { require_once("includes/no_info.php"); } ?>
        <?php require_once("includes/copyright.php"); ?>
    </div>
</div>
<script type="text/javascript">
$('.insert_perfil').on('click', function () {
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.insertProPerfis.php',
        data        : $("#FormInsert").serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Cadastrado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao cadastrar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })
});

$('.update_perfil').on('click', function () {
    var code = $(this).attr("data-codigo");
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.updateProPerfis.php?code='+code,
        data        : $("#FormUpdate" + code).serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })
});
</script>
<?php 
    require_once("includes/footer.php");
    } }
?>