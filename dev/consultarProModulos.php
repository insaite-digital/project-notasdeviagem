<?php 
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        header('Location: error.php');
    } else {
        $path_pagina = pathinfo( __FILE__ )['basename'];
        $acesso->Pagina = $path_pagina;
        $acesso->verificaPermissao();
        $resposta = $acesso->getResposta();
        if(empty($resposta) || $resposta == 'false'){ header('Location: direcionamento.php'); } else {

            $paginas = newsql("SELECT * FROM config_paginas WHERE PAG_ARQUIVO = '{$path_pagina}'")[0];
            $pagina_codigo          = $paginas['PAG_CODIGO'];
            $pagina_titulo          = $paginas['PAG_TITULO'];
            $pagina_singular        = $paginas['PAG_SINGULAR'];
            $pagina_plural          = $paginas['PAG_PLURAL'];
            $pagina_icone           = $paginas['PAG_ICONE'];
            $pagina_text_singular   = $paginas['PAG_TEXT_SINGULAR'];
            $pagina_text_plural     = $paginas['PAG_TEXT_PLURAL'];
            // Variaveis da Página de Consulta
            $pagina_tabela          = "config_modulos";
            
            $sql_consulta = newsql("SELECT * FROM config_modulos ORDER BY MOD_CODIGO DESC");
            $codigo       = newsql("SELECT AUTO_INCREMENT FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME   = '".$pagina_tabela."'")[0]['AUTO_INCREMENT'];
?>
<?php require_once("includes/header.php"); ?>

<div class="content-wrapper">
    <?php 
        // print_($sql_consulta);
    ?>
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <!-- Página Título -->
                    <span class="text-semibold">
                        <a href="index.php">Dashboard</a>
                    </span> 
                    <i class="icon-arrow-right6"></i> 
                    <strong>Configuração de Módulos</strong><br>
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <!-- Página Links de Ações -->
                    <a href="#" data-toggle="modal" data-target="#insert_modulo" class="btn btn-link btn-float has-text">
                        <i class="icon-database-add text-primary"></i> 
                        <span>Novo(a) Módulo</span>
                    </a>
                </div>

                <div id="insert_modulo" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-primary">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">
                                    Deseja cadastrar novo <strong>Módulo</strong>?<br>
                                </h5>
                            </div>
                            <hr style="margin: 0 auto;">
                            <form id="FormInsert" method="post" onsubmit="return false">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label><strong>Título <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                        <div class="input-group" style="width: 100%;">
                                            <input id="in_titulo" name="in_titulo" class="form-control" placeholder="Insira o título da permissão" value="">
                                            <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label><strong>Descrição:</strong></label>
                                        <textarea name="in_descricao" class="form-control mb-15" rows="3" cols="1" placeholder="Descrição do Módulo:"></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary insert_modulo">Cadastrar&nbsp;&nbsp;<i class="icon-database-add"></i></button>
                                </div>
                            </form>                          
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <!-- Página Breadcrumbs -->
                <li>
                    <a href="index.php">
                        <i class="icon-home2 position-left"></i> 
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <i class="icon-stack3"></i> 
                    <span>Configuração de Módulos</span>
                </li>
            </ul>
            <ul class="breadcrumb-elements">
                <!-- Modal Ajuda -->
                <?php require_once("includes/ajuda.php"); ?>
            </ul>
        </div>
    </div>


    <div class="content">
        <?php if(!empty($sql_consulta)){ ?>
        
        <div class="row">
            <?php 
                foreach ($sql_consulta as $key => $value) {
                    $modulo_codigo      = $value['MOD_CODIGO'];
                    $modulo_titulo      = $value['MOD_TITULO'];
                    $modulo_descricao   = $value['MOD_DESCRICAO'];
                    $sql_permissoes = newsql("SELECT COUNT(*) as total FROM config_modulos_permissoes as modperm WHERE modperm.MODPERM_MODULO = '{$modulo_codigo}'");
                    // print_($sql_permissoes);
            ?>

            <div class="col-sm-6 col-lg-4">
                <div class="panel">
                    <div class="panel-body">
                        <h5 class="content-group">
                            <span class="label label-flat label-rounded label-icon border-grey text-grey mr-10">
                                <i class="icon-stack3"></i>
                            </span>

                            <a href="consultarProModulosPerfis.php?active=promodulos&modulo=<?php echo $modulo_codigo; ?>" class="text-primary">
                                <strong><?php echo $modulo_titulo; ?></strong>
                            </a>
                            <span style="font-size: 11px; float: right;" class="label bg-success heading-text" style="float: right;"><?php echo $sql_permissoes[0]['total']; ?> - Páginas</span>
                        </h5>
                        <p class="content-group"><?php echo $modulo_descricao; ?></p>
                    </div>
                    <hr class="no-margin">
                    <div class="panel-body text-center alpha-grey">
                        <a href="consultarProModulosPerfis.php?active=promodulos&modulo=<?php echo $modulo_codigo; ?>" class="btn bg-primary-400">
                            <i class="icon-hammer-wrench position-left"></i>
                            Acessar
                        </a>
                    </div>
                </div>
            </div>
            <?php } ?>

        </div>
        <?php } else { require_once("includes/no_info.php"); } ?>
        <?php require_once("includes/copyright.php"); ?>
    </div>
</div>
<script type="text/javascript">
$('.insert_modulo').on('click', function () {
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.insertProModulos.php',
        data        : $("#FormInsert").serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Cadastrado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao cadastrar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })
});
</script>
<?php 
    require_once("includes/footer.php");
    } }
?>