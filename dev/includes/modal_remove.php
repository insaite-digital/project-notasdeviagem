<div id="modal_remover" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">
                    Porque deseja remover este <strong>Registro</strong>?<br>
                    Código: <strong class="text_remove"></strong>
                </h5>
            </div>
            <br><hr style="margin: 0 auto;">
            <form id="FormRemove" method="post" onsubmit="return false">
                <div class="modal-body">
                    <input type="hidden" value="" name="remove_codigo" id="remove_codigo">
                    <input type="hidden" value="" name="remove_pagina" id="remove_pagina">
                    <input type="hidden" value="" name="remove_tabela" id="remove_tabela">
                    <input type="hidden" value="" name="remove_coluna" id="remove_coluna">
                    <div class="form-group">
                        <label><strong>Motivo <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                        <div class="input-group">
                            <textarea id="remove_motivo" name="remove_motivo" rows="5" cols="5" class="form-control" placeholder="Insira um motivo" style="resize: none;"></textarea>
                            <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger">Remover&nbsp;&nbsp;<i class="icon-database-remove"></i></button>
                </div>
            </form>
            
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.remover').on('click', function() {
        var codigo = $(this).attr("data-codigo");
        var tabela = $(this).attr("data-tabela");
        var pagina = $(this).attr("data-pagina");
        var coluna = $(this).attr("data-coluna");

        $('#click_remove').data('codigo', codigo);
        
        $('#remove_codigo').val(codigo);
        $('#remove_tabela').val(tabela);
        $('#remove_pagina').val(pagina);
        $('#remove_coluna').val(coluna);
        
        $('.text_remove').text(codigo); 
        $('#modal_remover').modal('show'); 
    });

    $(document).on('submit', 'form#FormRemove', function() { 
        $.ajax({
            type        : 'POST', 
            url         : 'ajax/ajax.deletaRegistros.php',
            data        : $(this).serialize(),
            dataType    : 'json', 
            beforeSend: function() {
                $.blockUI({ 
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
                new PNotify({
                    text: "Aguarde um momento",
                    addclass: 'bg-primary',
                    type: 'info',
                    icon: 'icon-spinner4 spinner',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    opacity: .9,
                    width: "250px"
                });
            },
            success: function (data) {
                $.unblockUI();
                PNotify.removeAll();
                var resposta = data.resposta;
                if(resposta == 'true'){
                    new PNotify({   
                        text: "Deletado com sucesso!",
                        addclass: 'bg-success',
                        type: 'success',
                        icon: 'icon-checkmark3',
                        hide: true,
                        buttons: {
                            closer: true,
                            sticker: false
                        },
                        opacity: 1,
                        width: "200px"
                    });
                    setTimeout(function(){ location.reload(); }, 3000);
                } else {
                    new PNotify({   
                        text: "Não foi possível deletar!",
                        addclass: 'bg-success',
                        type: 'danger',
                        icon: 'icon-blocked',
                        hide: true,
                        buttons: {
                            closer: true,
                            sticker: false
                        },
                        opacity: 1,
                        width: "200px"
                    });
                }
            }
        })
    });
</script>