<div id="modal_upload" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">
                    <strong>Configuração de Upload</strong><br>
                    Código: <strong class="text_upload"></strong><br>
                    Nome do Arquivo: <strong class="nome_upload"></strong>
                </h5>
            </div>
            <br><hr style="margin: 0 auto;">
            <form id="FormUpload" method="post" onsubmit="return false">
                <div class="modal-body">
                    <input type="hidden" value="" name="upload_codigo" id="upload_codigo">
                    <input type="hidden" value="" name="upload_tipo" id="upload_tipo">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><strong>Título em Destaque:</strong></label>
                            <input id="upload_titulo" type="text" class="form-control" name="upload_titulo" placeholder="Título Principal do Upload" value="">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label><strong>Legenda/Descrição (Imagem):</strong></label>
                            <input id="upload_descricao" type="text" class="form-control" name="upload_descricao" placeholder="Descrição e Legenda do Upload" value="">
                        </div>
                    </div>
                    <?php 
                        $consulta = newsql("SELECT * FROM config_uploads_projetos WHERE PROJ_TITULO = '{$upload_name}'")[0];
                        $coprojeto = $consulta['PROJ_CODIGO'];
                        $consulta_tipos = newsql("SELECT * FROM config_uploads_projetos_tipos WHERE TIPO_PROJETO = '{$coprojeto}'");
                        if(!empty($consulta_tipos)){
                    ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><strong>Tipo de Upload:</strong></label>
                            <select data-placeholder="Selecione o tipo de upload" class="select" name="upload_tipo" id="upload_tipo">
                                <option value="0">Selecionar/Selecionado</option> 
                                <?php foreach ($consulta_tipos as $key => $value) { ?>
                                <option value="<?php echo $value['TIPO_CODIGO']; ?>"><?php echo $value['TIPO_TITULO']; ?></option> 
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <?php } ?>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label><strong>Upload em Destaque:</strong></label>
                            <select data-placeholder="Selecione o upload em Destaque" class="select" name="upload_destaque" id="upload_destaque">
                                <!-- <option value="none">Sem destaque</option>  -->
                                <option value="none">Selecionar/Selecionado</option> 
                                <option value="true">Este registro é destaque</option> 
                                <option value="false">Não é destaque</option> 
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Atualizar&nbsp;&nbsp;<i class="icon-database-edit2"></i></button>
                </div>
            </form>
            
        </div>
    </div>
</div>
<script type="text/javascript">
    $("body").on("click", ".alterar_upload", function(event){
        var codigo      = $(this).attr("data-codigo");
        var nome        = $(this).attr("data-nome");
        var titulo      = $(this).attr("data-titulo");
        var descricao   = $(this).attr("data-descricao");
        var tipo        = $(this).attr("data-tipo");
        var destaque    = $(this).attr("data-destaque");
        var tipo_proj   = $(this).attr("data-tipo_proj");

        $('#upload_codigo').val(codigo);
        $('#upload_titulo').val(titulo);
        $('#upload_descricao').val(descricao);
        $('#upload_tipo').val(tipo);

        $('#upload_destaque option[value="'+destaque+'"]').prop('selected', true);
        $('#upload_tipo option[value="'+tipo_proj+'"]').prop('selected', true);

        $('.text_upload').text(codigo); 
        $('.nome_upload').text(nome); 
        $('#modal_upload').modal('show'); 
    });

    $(document).on('submit', 'form#FormUpload', function() { 
        var tipo    = $('#upload_tipo').val();
        var codigo  = "<?php echo $codigo; ?>";
        $.ajax({
            type        : 'POST', 
            url         : 'ajax/ajax.updateProUploads.php',
            data        : $(this).serialize(),
            dataType    : 'json', 
            beforeSend: function() {
                $.blockUI({ 
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
                new PNotify({
                    text: "Aguarde um momento",
                    addclass: 'bg-primary',
                    type: 'info',
                    icon: 'icon-spinner4 spinner',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    opacity: .9,
                    width: "250px"
                });
            },
            success: function (data) {
                $.unblockUI();
                PNotify.removeAll();
                var resposta = data.resposta;
                if(resposta == 'true'){
                    new PNotify({   
                        text: "Atualizado com sucesso!",
                        addclass: 'bg-success',
                        type: 'success',
                        icon: 'icon-checkmark3',
                        hide: true,
                        buttons: {
                            closer: true,
                            sticker: false
                        },
                        opacity: 1,
                        width: "200px"
                    });
                    getUploads(tipo, codigo, tipo + '-uploads', '.arquivos-box');
                    $('#modal_upload').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                } else {
                    new PNotify({   
                        text: "Não foi possível atualizar!",
                        addclass: 'bg-danger',
                        type: 'danger',
                        icon: 'icon-blocked',
                        hide: true,
                        buttons: {
                            closer: true,
                            sticker: false
                        },
                        opacity: 1,
                        width: "200px"
                    });
                }
            }
        })
    });

    $("body").on("click", ".delaction", function(event){
        var codigo      = $(this).attr("data-codigo");
        var tipo        = $(this).attr("data-tipo");

        var formData = {
            'codigo' : codigo,
        };
        $.ajax({
            type        : 'POST', 
            url         : 'ajax/ajax.removerProUploads.php',
            data        : formData,
            dataType    : 'json', 
            beforeSend: function() { 
                $.blockUI({ 
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
                new PNotify({
                    text: "Aguarde um momento",
                    addclass: 'bg-primary',
                    type: 'info',
                    icon: 'icon-spinner4 spinner',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    opacity: .9,
                    width: "250px"
                });
            },
            success: function (data) {
                $.unblockUI();
                PNotify.removeAll();
                var resposta = data.resposta;
                if(resposta == 'true'){
                    new PNotify({   
                        text: "Deletado com sucesso com sucesso!",
                        addclass: 'bg-success',
                        type: 'success',
                        icon: 'icon-checkmark3',
                        hide: true,
                        buttons: {
                            closer: true,
                            sticker: false
                        },
                        opacity: 1,
                        width: "200px"
                    });
                    // getUploads(tipo, codigo, tipo + '-uploads', '.arquivos-box');
                        $("#upreg" + codigo).remove();
                } else {
                    new PNotify({   
                        text: "Não foi possível atualizar!",
                        addclass: 'bg-danger',
                        type: 'danger',
                        icon: 'icon-blocked',
                        hide: true,
                        buttons: {
                            closer: true,
                            sticker: false
                        },
                        opacity: 1,
                        width: "200px"
                    });
                }
            }
        });
    });
</script>