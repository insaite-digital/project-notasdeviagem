<li>
    <a href="#" data-toggle="modal" data-target="#modal_help">
        <i class="icon-help position-left"></i> 
        <span>Ajuda</span>
    </a>
</li>
<div id="modal_help" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">
                    <strong style="color: #FFFFFF">Central de Ajuda</strong><br>
                </h5>
            </div>
            <hr style="margin: 0 auto;">
            <div class="modal-body">
                <div class="panel-group panel-group-control panel-group-control-right content-group-lg" id="accordion-control-right">
                    <?php 
                        $sql_ajudas = newsql("SELECT * FROM config_paginas_ajudas WHERE AJU_PAGINA = '{$pagina_codigo}' AND AJU_VISIBILIDADE = 'true' AND AJU_CHECK = 'true'");
                        foreach ($sql_ajudas as $key => $value) {
                            $ajuda_codigo = $value['AJU_CODIGO'];
                            $ajuda_titulo = $value['AJU_TITULO'];
                            $ajuda_pergunta = $value['AJU_PERGUNTA'];
                            $ajuda_resposta = $value['AJU_RESPOSTA'];
                    ?>
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h6 class="panel-title" style="height: 25px;">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion-control-right" href="#question<?php echo $ajuda_codigo; ?>"><?php echo $ajuda_pergunta; ?></a>
                            </h6>
                        </div>
                        <div id="question<?php echo $ajuda_codigo; ?>" class="panel-collapse collapse" style="font-size: 13px; text-align: right;">
                            <div class="panel-body">
                                <article>
                                <?php echo strip_tags(htmlspecialchars_decode($ajuda_resposta),"<span><strong><b><u><i><ul><li>"); ?>
                                </article>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <hr style="margin: 10px auto;">
            <div class="modal-footer">
                <br>
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Encontrei o que precisava!</button>
            </div>
        </div>
    </div>
</div>
