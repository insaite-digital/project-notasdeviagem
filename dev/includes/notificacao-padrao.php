<div id="modal_iconified<?php echo $cadastro_usuario . '_' . $primary_database_page_tabela; ?>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title"><i class="icon-menu7"></i> &nbsp;Sistema de Notificações</h5>
            </div>

            <div class="modal-body">
                <h6 class="text-semibold"><i class="icon-notification2 position-left"></i> Cadastro & Alteração de Informações</h6>
                <p>
                    Desenvolvemos esta <strong>Plataforma</strong> para melhor lhe atender! <br>
                    Utilizamos as mais atualizadas tecnologias, para que suas informações permaneçam seguras e estamos sempre ativos para buscar melhorias!<br><br>
                    Ao carregar esta página geramos um novo registro em seu banco de dados, desta maneira toda e qualquer informação inserida nos campos requisitados desta página serão salvas automáticamente - <strong>como backup temporário</strong>...<br><br>
                    Para finalizar seu cadastro ou alteração, basta seguir os passos e clicar em <strong>Comfirmar/Finalizar</strong> no último passo. Fazendo isso, nossa plataforma entenderá que estas informações são verídicas e importantes e um novo registro será criado para que possa fazer parte das informações da plataforma de forma integral e confiavél. 
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Fechar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    // Modal de Notify
    $('.closedrop' + database_page_usuario + '_' + database_page_tabela).click(function(event){ $('#modal_iconified' + database_page_usuario + '_' + database_page_tabela).fadeOut(); event.preventDefault(); });
    $(document).ready(function() { var ls = localStorage.getItem("modal" + database_page_usuario + '_' + database_page_tabela); if(!ls){ $('#modal_iconified' + database_page_usuario + '_' + database_page_tabela).modal('show'); } })
    $('#modal_iconified' + database_page_usuario + '_' + database_page_tabela).on('shown.bs.modal', function(){ localStorage.setItem("modal" + database_page_usuario + '_' + database_page_tabela, false); });
</script>