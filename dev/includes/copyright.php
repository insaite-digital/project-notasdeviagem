        <!-- Footer -->
        <div class="footer text-muted">
            &copy; <?php echo date("Y"); ?> <?php echo $inprojeto_titulo; ?> <br>Desenvolvido por <a href="https://insaitedigital.com.br" target="_blank">Insaite Digital</a>
        </div>
        <!-- /footer -->