<div id="modal_upload" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">
                    <strong>Configuração de Upload</strong><br>
                    Código: <strong class="text_upload"></strong><br>
                    Nome do Arquivo: <strong class="nome_upload"></strong>
                </h5>
            </div>
            <br><hr style="margin: 0 auto;">
            <form id="FormUpload" method="post" onsubmit="return false">
                <div class="modal-body">
                    <input type="hidden" value="" name="upload_codigo" id="upload_codigo">
                    <input type="hidden" value="" name="upload_tipo" id="upload_tipo">
                    <input type="hidden" value="" name="upload_destaque" id="upload_destaque">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><strong>Título em Destaque:</strong></label>
                            <input id="upload_titulo" type="text" class="form-control" name="upload_titulo" placeholder="Título Principal do Upload" value="">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label><strong>Legenda/Descrição:</strong></label>
                            <input id="upload_descricao" type="text" class="form-control" name="upload_descricao" placeholder="Descrição e Legenda do Upload" value="">
                        </div>
                    </div>
                    <?php 
                        $consulta = newsql("SELECT * FROM config_uploads_projetos WHERE PROJ_TITULO = '{$primary_database_page_upload}'")[0];
                        $coprojeto = $consulta['PROJ_CODIGO'];
                        $consulta_tipos = newsql("SELECT * FROM config_uploads_projetos_tipos WHERE TIPO_PROJETO = '{$coprojeto}'");
                        if(!empty($consulta_tipos)){
                    ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><strong>Tipo de Upload:</strong></label>
                            <select data-placeholder="Selecione o tipo de upload" class="select" name="upload_tipo_select" id="upload_tipo_select">
                                <?php foreach ($consulta_tipos as $key => $value) { ?>
                                <option value="<?php echo $value['TIPO_CODIGO']; ?>"><?php echo $value['TIPO_TITULO']; ?></option> 
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <?php } ?>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label><strong>Upload em Destaque:</strong></label>
                            <select data-placeholder="Selecione o upload em Destaque" class="select" name="upload_destaque_select" id="upload_destaque_select">
                                <option value="true">Este registro é destaque</option> 
                                <option value="false">Não é destaque</option> 
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Atualizar&nbsp;&nbsp;<i class="icon-database-edit2"></i></button>
                </div>
            </form>
            
        </div>
    </div>
</div>
<script type="text/javascript">
function getUploads(type, cod, wrapElm, box){

    $.blockUI({ 
        message: '<i class="icon-spinner4 spinner"></i>',
        timeout: 2000,
        overlayCSS: {
            backgroundColor: '#1b2024',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            color: '#fff',
            padding: 0,
            backgroundColor: 'transparent'
        }
    });

    $.ajax({
        type        : 'POST', 
        url         : ADMIN + 'ajax/proadmin/ajax.searchProUploads.php',
        data        : {"type" :type,"id" :cod},
        dataType    : 'json', 
        success: function (data) {
            $(box).empty();
            if(data.uploads.length > 0){
                for (var i in data.uploads){
                    if(data.uploads[i].UP_DESTAQUE == 'true'){
                        var destaque = 'Destaque';
                    } else {
                        var destaque = '';
                    }
                    if(data.uploads[i].UP_TIPO == 0){
                        var titulo_tipo = '';
                        var codigo_tipo = '';
                    } else {
                        var titulo_tipo = data.uploads[i].TIPO_TITULO;
                        var codigo_tipo = '(' + data.uploads[i].TIPO_CODIGO + ')';
                    }
                    var up = "<tr id='upreg"+data.uploads[i].UP_CODIGO+"'>"+
                                "<td class='col-md-4 col-sm-3'><a target='_blank' href='"+data.uploads[i].UP_CAMINHO+"'>" + data.uploads[i].UP_NOME + "</a></td>"+
                                "<td class='col-md-3 col-sm-2'>"+
                                    "<ul class='icons-list'>"+
                                        "<li><a class='alterar_upload' data-tipo_proj='" + data.uploads[i].UP_TIPO + "' data-destaque='" + data.uploads[i].UP_DESTAQUE + "' data-codigo='" + data.uploads[i].UP_CODIGO + "' data-nome='" + data.uploads[i].UP_NOME + "' data-titulo='" + data.uploads[i].UP_TITULO + "' data-descricao='" + data.uploads[i].UP_DESCRICAO + "'data-tipo='" + type.trim() + "' href='javascript:void(0);'><i class='icon-cog7'></i></a></li>"+
                                        "<li><a class='delaction' href='javascript:void(0);' data-tipo=" + type.trim() + " data-codigo='" + data.uploads[i].UP_CODIGO + "'><i class='icon-trash'></i></a></li>"+
                                    "</ul>"+
                                "</td>"+
                                "<td><span style='margin-bottom: 5px;' class='label bg-success'>"+destaque+"</span>&nbsp;&nbsp;<span style='margin-bottom: 5px;' class='label bg-teal'>" + titulo_tipo + "" + codigo_tipo + "</span>&nbsp;&nbsp;" + data.uploads[i].UP_TITULO + "</td>"+
                            "</tr>";
                            console.log(up);
                    $(box).append(up);
                }
            }
        }
    });
}

$("body").on("click", ".alterar_upload", function(event){
    var codigo      = $(this).attr("data-codigo");
    var nome        = $(this).attr("data-nome");
    var titulo      = $(this).attr("data-titulo");
    var descricao   = $(this).attr("data-descricao");
    var tipo        = $(this).attr("data-tipo");
    var destaque    = $(this).attr("data-destaque");
    var tipo_proj   = $(this).attr("data-tipo_proj");
    
    $('#upload_codigo').val(codigo);
    $('#upload_titulo').val(titulo);
    $('#upload_descricao').val(descricao);
    $('#upload_tipo').val(tipo);
    $('#upload_destaque').val(destaque);

    $('#upload_destaque_select').val(destaque).trigger('change');
    $('#upload_tipo_select').val(tipo_proj).trigger('change');

    $('.text_upload').text(codigo); 
    $('.nome_upload').text(nome); 
    $('#modal_upload').modal('show'); 
});

$(document).on('submit', 'form#FormUpload', function() { 
    var tipo    = $('#upload_tipo').val();
    var codigo  = "<?php echo $codigo; ?>";
    $.ajax({
        type        : 'POST', 
        url         : ADMIN + 'ajax/proadmin/ajax.updateProUploads.php',
        data        : $(this).serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso!",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // getUploads(tipo, codigo, tipo + '-uploads', '.arquivos-box');
                getUploads('<?php echo $primary_database_page_upload; ?>', '<?php echo $database_codigo; ?>', '<?php echo $primary_database_page_upload; ?>-uploads', '.arquivos-box');
                $('#modal_upload').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            } else {
                new PNotify({   
                    text: "Não foi possível atualizar!",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-blocked',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
        }
    })
});

$("body").on("click", ".delaction", function(event){
    var codigo      = $(this).attr("data-codigo");
    var tipo        = $(this).attr("data-tipo");

    var formData = {
        'codigo' : codigo,
    };
    $.ajax({
        type        : 'POST', 
        url         : ADMIN + 'ajax/proadmin/ajax.removerProUploads.php',
        data        : formData,
        dataType    : 'json', 
        beforeSend: function() { 
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Deletado com sucesso com sucesso!",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // getUploads(tipo, codigo, tipo + '-uploads', '.arquivos-box');
                    $("#upreg" + codigo).remove();
            } else {
                new PNotify({   
                    text: "Não foi possível atualizar!",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-blocked',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
        }
    });
});

</script>