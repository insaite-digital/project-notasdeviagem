<script type="text/javascript">
// Insert Data Individual Form Database
$(document).on('apply.daterangepicker', '.daterange-insaite', insertFunction);
$(document).on('change.daterangepicker', '.daterange-insaite', insertFunction);
$(document).on('change', '.insert_field', insertFunction);
$('.summernote_update').on("summernote.change", insertFunction);
function insertFunction(){
    var table   = $(this).attr("data-table"); 
    var primary = $(this).attr("data-field-primary"); 
    var field   = $(this).attr("data-field"); 
    var id      = $(this).attr("data-id"); 
    var type    = $(this).attr("data-type");
    var value   = ""; 

    if(type == 'varchar'){          value = $(this).val(); }
    else if(type == 'text'){        value = $(this).val(); }
    else if(type == 'longtext'){    value = $(this).summernote('code'); }
    else if(type == 'data'){        value = $(this).val(); }
    else if(type == 'cnpj'){        value = $(this).val(); }
    else if(type == 'cpf'){         value = $(this).val(); }
    else if(type == 'valor'){       value = $(this).val(); }
    else if(type == 'quantidade'){  value = $(this).val(); }
    else { value = $(this).val(); }

    $.ajax({
        type        : 'POST', 
        url         : ADMIN + 'ajax/ajax.cadastrarDados.php',
        data        : { table: table, primary: primary, field: field, id: id, type: type, value: value },
        dataType    : 'json', 
        beforeSend: function() { },
        success: function (data) {
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Tudo OK!",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px",
                    delay: 500
                });
            } else if(resposta == 'false'){
                new PNotify({   
                    text: "Não foi possível cadastrar esta informação!",
                    addclass: 'bg-warning',
                    type: 'warning',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            } else {
                new PNotify({   
                    text: "O campo parece estar vazio, ou faltando informações!",
                    addclass: 'bg-warning',
                    type: 'warning',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
        }
    })
}
</script>