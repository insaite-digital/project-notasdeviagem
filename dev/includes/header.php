<?php 
    $sql_user = newsql("SELECT * FROM config_usuarios as usuario INNER JOIN config_perfis as perfil ON perfil.PER_CODIGO = usuario.USU_PERFIL WHERE usuario.USU_CODIGO = '{$_SESSION["USUARIO_CODIGO"]}'");
?>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $inprojeto_titulo; ?></title>
        <link rel="icon" type="image/png" href="imagens/favicon.png" />
        
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="_template/assets/css/colors.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="_template/assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/loaders/blockui.min.js"></script>

        <script type="text/javascript" src="js/insaite/fresco.js"></script>
        <link href="css/fresco.css" rel="stylesheet" type="text/css">
        
        <script type="text/javascript" src="_template/assets/js/plugins/editors/summernote/summernote.min.js"></script>

        <!-- /core JS files -->
        <script type="text/javascript" src="js/mask/jquery.mask.js"></script>
        <script type="text/javascript" src="js/insaite/insaite.js"></script>

        <!-- Notificação -->
        <script type="text/javascript" src="_template/assets/js/plugins/notifications/pnotify.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/notifications/noty.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/notifications/jgrowl.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/notifications/bootbox.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/notifications/sweet_alert.min.js"></script>

        <!-- Formulários -->
        <script type="text/javascript" src="_template/assets/js/plugins/forms/wizards/stepy.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/core/libraries/jasny_bootstrap.min.js"></script>
        <!-- <script type="text/javascript" src="_template/assets/js/plugins/forms/validation/validate.min.js"></script> -->
        <script type="text/javascript" src="_template/assets/js/plugins/forms/tags/tagsinput.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/forms/tags/tokenfield.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/ui/prism.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
        
        <!-- Data Table -->
        <script type="text/javascript" src="_template/assets/js/core/libraries/jquery_ui/widgets.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/tables/datatables/extensions/natural_sort.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/datatables_advanced.js"></script>
        
        <!-- Data Picker -->
        <script type="text/javascript" src="_template/assets/js/plugins/ui/moment/moment.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/ui/moment/moment_locales.min.js"></script>

        <!-- Uploads de Arquivos -->
        <script type="text/javascript" src="_template/assets/js/plugins/uploaders/plupload/plupload.full.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/uploaders/plupload/plupload.queue.min.js"></script>

        <!-- Data Picker -->
        <script type="text/javascript" src="_template/assets/js/plugins/pickers/daterangepicker.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/pickers/anytime.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/pickers/pickadate/picker.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/pickers/pickadate/legacy.js"></script>


        <!-- Editores de Textos -->
        <script type="text/javascript" src="_template/assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/editors/wysihtml5/wysihtml5.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/editors/wysihtml5/toolbar.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/editors/wysihtml5/parsers.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/notifications/jgrowl.min.js"></script>

        <script type="text/javascript" src="_template/assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/pickers/daterangepicker.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/ui/headroom/headroom.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/ui/headroom/headroom_jquery.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/ui/nicescroll.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

        <!---->
        <script type="text/javascript" src="_template/assets/js/core/app.js"></script>
        <!---->
        <script type="text/javascript" src="_template/assets/js/pages/editor_summernote.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/tasks_list.js"></script>

        <script type="text/javascript" src="_template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/datatables_api.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/components_notifications_pnotify.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/components_notifications_other.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/wizard_stepy.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/components_popups.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/form_tags_input.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/picker_date.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/components_modals.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/editor_wysihtml5.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/layout_fixed_custom.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/layout_navbar_hideable_sidebar.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/datatables_responsive.js"></script>
        <script type="text/javascript" src="_template/assets/js/pages/datatables_extension_buttons_html5.js"></script>
        <script type="text/javascript" src="_template/assets/js/plugins/uploaders/plupload/langs/pt_BR.js"></script>

        <script type="text/javascript">
            var SITE =  '<?php echo SITE; ?>';
            var SYSTEM =  '<?php echo SYSTEM; ?>';
            var ADMIN =  '<?php echo ADMIN; ?>';
            var AJAX =  '<?php echo AJAX; ?>';

            moment.locale('pt-br');
            $(window).load(function(){ $('.blockUI').remove(); });      
            $(document).ready(function(){
                $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
                $('.cpf').mask('000.000.000-00', {reverse: true});
                $('.qtd').mask('000.000.000.000.000', {reverse: true});
                $('.money').mask('000.000.000.000.000,00', {reverse: true});
                $('.money4').mask('000.000.000.000.000,0000', {reverse: true});
                $('.empenho').mask('000000/0000', {reverse: true});
                $('.qtd_text').mask('000.000.000.000.000');
                $('.data').mask('00/00/0000');
                $('.hora').mask('00:00');
                $('.hora_segundos').mask('00:00:00');
                $('.cep').mask('00000-000');
                $('.fone').mask('(00) 0000-00009');
                $('.fone').blur(function(event) { if($(this).val().length == 15){ $('.fone').mask('(00) 00000-0009'); } else { $('.fone').mask('(00) 0000-00009'); } });
                $('.onlynumber').mask('0#');
            });
        </script>

        <style type="text/css">
            .wysihtml5-sandbox { height: 250px!important; }
            .wysihtml5-init { height: 250px!important; }
            .note-editor { margin-bottom: 0; }
            .dataTable thead .sorting::before, .dataTable thead .sorting::after {
                color: white!important;
            }
        </style>
    </head>
    <body class="navbar-top">
        <div style="z-index: 1000; border: medium none; margin: 0pt; padding: 0pt; width: 100%; height: 100%; top: 0pt; left: 0pt; background-color: rgb(0, 0, 0); opacity: 0.6; cursor: wait; position: fixed;" class="blockUI blockOverlay"></div>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-header">
                <h4 style="margin: 10px;"><?php echo $inprojeto_titulo; ?></h4>

                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-earth"></i>
                            <span class="visible-xs-inline-block position-right">Atualizações de Sistema</span>
                        </a>
                        
                        <div class="dropdown-menu dropdown-content">
                            <div class="dropdown-content-heading">
                                Atualizações do Sistema
                                <ul class="icons-list">
                                    <li><a href="javascript:window.location.reload(true)"><i class="icon-sync"></i></a></li>
                                </ul>
                            </div>

                            <ul class="media-list dropdown-content-body width-350">
                                <?php 
                                    $sql_atualizacoes = newsql("SELECT * FROM config_atualizacoes WHERE ATU_VISIBILIDADE = 'true' ORDER BY ATU_CADASTRO_TIME DESC LIMIT 0,5");
                                    foreach ($sql_atualizacoes as $key => $value) {
                                        $atu_versao = $value['ATU_VERSAO'];
                                        $atu_tipo = $value['ATU_TIPO'];
                                        $atu_time = $value['ATU_CADASTRO_TIME'];
                                        $atu_descricao = $value['ATU_DESCRICAO'];
                                ?>
                                
                                <li class="media">
                                    <div class="media-left">
                                        <?php if($atu_tipo == '1'){ ?>
                                        <a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
                                        <?php } ?> 
                                        <?php if($atu_tipo == '2'){ ?>
                                        <a href="#" class="btn border-info text-info btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-branch"></i></a>
                                        <?php } ?> 
                                        <?php if($atu_tipo == '3'){ ?>
                                        <a href="#" class="btn border-success text-success btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-merge"></i></a>
                                        <?php } ?>
                                    </div>

                                    <div class="media-body">
                                        <strong>Versão: <?php echo $atu_versao; ?></strong>&nbsp;<br><?php echo $atu_descricao; ?>
                                        <div class="media-annotation"><?php echo date("d/m/Y H:i", strtotime($atu_time)); ?></div>
                                    </div>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </li>
                </ul>

                <p class="navbar-text"><?php if($projeto_online == 'true'){ echo "<span class='label bg-success'>Online</span>";  } else { echo "<span class='label bg-warning'>Offline</span>";  } ?></span></p>

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="_template/assets/images/placeholder.jpg" alt="">
                            <span><?php echo $sql_user[0]['USU_NOME']; ?></span>
                            <i class="caret"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right" style="width: 220px;">
                            <li><a href="<?php echo SITE; ?>" style="background: #2196F3; color: #FFFFFF;" target="_blank"><i style="color: #FFFFFF!important;" class="icon-laptop text-default"></i> Acessar Website</a></li>
                            <!-- <li><a href="alterarDadosgerais.php?active=dados"><i class="icon-equalizer text-default"></i> Dados Gerais</a></li> -->
                            <li><a href="consultarUsuarios.php?active=usuarios"><i class="icon-collaboration text-default"></i> Usuários</a></li>
                            <li class="divider"></li>
                            <li><a href="acesso/logout.sql.php" style="background: red; color: #FFFFFF;"><i class="icon-switch2"></i> Logout/Sair</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>


        <div class="page-container">
            <div class="page-content">
            <?php require_once("sidebar.php"); ?>
