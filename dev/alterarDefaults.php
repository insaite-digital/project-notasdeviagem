<?php 
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        header('Location: error.php');
    } else {
        $path_pagina = pathinfo( __FILE__ )['basename'];
        $acesso->Pagina = $path_pagina;
        $acesso->verificaPermissao();
        $resposta = $acesso->getResposta();
        if(empty($resposta) || $resposta == 'false'){ header('Location: direcionamento.php'); } else {
            date_default_timezone_set('America/Sao_Paulo');
            $cadastro_time      = date("Y-m-d H:i:s");
            $cadastro_usuario   = $_SESSION['USUARIO_CODIGO'];
            insert_logs($path_pagina,"");

            /* Variaveis da Página Database */
            $paginas                    = newsql("SELECT * FROM config_paginas WHERE PAG_ARQUIVO = '{$path_pagina}'")[0];
            $pagina_codigo              = $paginas['PAG_CODIGO'];
            $pagina_titulo              = $paginas['PAG_TITULO'];
            $pagina_modulo_permissao    = $paginas['PAG_MODULO_PERMISSAO'];
            $pagina_modulo              = $paginas['PAG_MODULO'];
            $pagina_text_singular       = $paginas['PAG_TEXT_SINGULAR'];
            $pagina_text_plural         = $paginas['PAG_TEXT_PLURAL'];
            $pagina_icone               = $paginas['PAG_ICONE'];
            $pagina_singular            = $paginas['PAG_SINGULAR'];
            $pagina_plural              = $paginas['PAG_PLURAL'];

            /* Variaveis da Página Principal */
                $primary_database_page_check            = "true";
                $primary_database_page_tabela           = "tbl_defaults";
                $primary_database_page_collumn_prefix   = "DEF";
                $primary_database_page_collumn_id       = "DEF_CODIGO";
                $primary_database_page_upload           = $pagina_singular;
                $primary_database_page_ajax             = "ajax/ajax.alterar".maiuscula($pagina_plural).".php";
                $primary_database_page_backlink         = "consultar" . maiuscula($pagina_plural). ".php?active=".$pagina_plural."";

            /* Variaveis da Página Secundária */
                $secondary_database_page_check            = "false";
            
            /*** Configurações Básicas ***/
                /* Geração de Código Único para cadastro*/
                $database_codigo       = $_GET[$pagina_singular];

            /*** Consulta Padrão/Personalizadas ***/
                $database_consulta = "SELECT * FROM ".$primary_database_page_tabela." ";
                    /***
                     *  Insert Complements Busca Principal  
                    ***/
                    // $database_consulta .= " ";
                $database_consulta .= " WHERE ".$primary_database_page_collumn_id." = ".$database_codigo."";
                $database_consulta = newsql($database_consulta)[0];
?>

<?php require_once("includes/header.php"); ?>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <span class="text-semibold">
                        <a href="index.php">Dashboard</a>
                    </span> 
                    <i class="icon-arrow-right6"></i> 
                    <strong><?php echo $pagina_titulo; ?></strong><br>
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo $primary_database_page_backlink; ?>" class="btn btn-link btn-float has-text">
                        <i class="icon-database-arrow text-default"></i> 
                        <span>Consultar <?php echo maiuscula($pagina_text_plural); ?></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li>
                    <a href="index.php">
                        <i class="icon-home2 position-left"></i> 
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $primary_database_page_backlink; ?>">
                        <i class="icon-database-arrow position-left"></i> 
                        <span>Consultar <?php echo maiuscula($pagina_text_plural); ?></span>
                    </a>
                </li>
                <li>
                    <i class="<?php echo $pagina_icone; ?>"></i> 
                    <span><?php echo $pagina_titulo; ?></span>
                </li>
            </ul>
            <ul class="breadcrumb-elements">
                <!-- Modal Ajuda -->
                <?php require_once("includes/ajuda.php"); ?>
            </ul>
        </div>
    </div>
    <div class="content">

        <div class="row">
            <div calss="col-lg-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"><i class="icon-database-add"></i> <strong>Informações para Cadastro</strong></h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a id="clean_form" href="header('Refresh:0');" data-action="reload"></a></li>
                            </ul>
                        </div>
                        <p class="content-group" style="margin: 0!important">Siga as etapas e passos até o final.</p>
                    </div> 
                    <div class="panel-body"> 
                        <form id="FormDefault" class="stepy-default" method="post" onsubmit="return false">
                            <input 
                                type="hidden" 
                                name="form_page" 
                                value="<?php echo $path_pagina; ?>"
                            >
                            <input 
                                type="hidden" 
                                name="form_table" 
                                value="<?php echo $primary_database_page_tabela; ?>"
                            >
                            <input 
                                type="hidden" 
                                name="form_id" 
                                value="<?php echo $database_codigo; ?>"
                            >
                            <input 
                                type="hidden" 
                                name="form_prefix" 
                                value="<?php echo $primary_database_page_collumn_prefix; ?>"
                            >
                            
                            
                            <fieldset title="1">
                                <legend class="text-semibold">Campos Padrões</legend>
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Configurações Padrões</legend>
                                                    
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Visibilidade</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-database4"></i></span>
                                                                        <select 
                                                                            id="visibilidade<?php echo $database_codigo; ?>"
                                                                            class="select insert_field" 
                                                                            type="" 
                                                                            name="visibilidade<?php echo $database_codigo; ?>"
                                                                            title="Indique a visibilidade"
                                                                            placeholder="Selecione o tipo de visibilidade"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_VISIBILIDADE" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Selecione o tipo de visibilidade" 
                                                                            required="true"
                                                                        >
                                                                            <option value="0">Selecionar...</option> 
                                                                            <option <?php if($database_consulta["DEF_VISIBILIDADE"] == 'true') echo 'selected'; ?> value="true">Ativo</option> 
                                                                            <option <?php if($database_consulta["DEF_VISIBILIDADE"] == 'false') echo 'selected'; ?> value="false">Inativo</option> 
                                                                        </select>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Imagem:</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="row">
                                                                        <div class="col-lg-6">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon"><i class="icon-image2"></i></span>
                                                                                <input 
                                                                                    id="file_upload<?php echo $database_codigo; ?>"
                                                                                    class="file-styled inputs file_upload"
                                                                                    style="cursor: pointer;" 
                                                                                    type="file" 
                                                                                    name="file_upload<?php echo $database_codigo; ?>" 
                                                                                    data-codigo="<?php echo $database_codigo; ?>" 
                                                                                    data-upload="<?php echo $primary_database_page_upload; ?>" 
                                                                                    data-show-preview="false" 
                                                                                    title="Indique a Imagem"
                                                                                    placeholder="Selecione a Imagem"
                                                                                    data-trigger="focus" 
                                                                                    data-placeholder="Selecione a imagem" 
                                                                                >
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon"><i class="icon-highlight"></i></span>
                                                                                <input 
                                                                                    id=""
                                                                                    class="form-control insert_field" 
                                                                                    type="varchar" 
                                                                                    name="legenda<?php echo $database_codigo; ?>" 
                                                                                    title="Insira a legenda" 
                                                                                    placeholder="Informar a legenda"
                                                                                    data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                                    data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                                    data-field="<?php echo $primary_database_page_collumn_prefix; ?>_LEGENDA" 
                                                                                    data-id="<?php echo $database_codigo; ?>" 
                                                                                    data-type="varchar" 
                                                                                    data-trigger="focus" 
                                                                                    data-placeholder="Insira a legenda"
                                                                                    required="true"
                                                                                    value="<?php echo $database_consulta['DEF_LEGENDA']; ?>"
                                                                                >
                                                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div id="action_image" style="margin-top: 10px;">
                                                                                <!-- Imagem Ajax -->
                                                                                <?php 
                                                                                    $imagens_unico = get_uploads($primary_database_page_upload . "_unico", $database_codigo, "");
                                                                                    $imagem_unico = capture_uploads($imagens_unico); 
                                                                                ?>
                                                                                <?php if(!empty($imagens_unico)){ ?>
                                                                                    <span id="imagem_check"><a target="_blank" href="<?php echo $imagem_unico; ?>">Visualizar Imagem (<?php echo $imagens_unico[0]['UP_NOME']; ?>)</a></span>
                                                                                    <span style="cursor: pointer; float: right;" onclick="removeImage(<?php echo $imagens_unico[0]['UP_CODIGO']; ?>)" class="text-danger"><i class="icon-folder-remove"></i> Remover Imagem</span>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Varchar 255 Obrigatório</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-cube4"></i></span>
                                                                        <input 
                                                                            id=""
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="titulo<?php echo $database_codigo; ?>" 
                                                                            title="Insira o título obrigatório" 
                                                                            placeholder="Informar o título"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_TITULO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Insira o título obrigatório"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_TITULO']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Varchar 255 Opcional</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-cube4"></i></span>
                                                                        <input 
                                                                            id=""
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="titulo_opcional<?php echo $database_codigo; ?>" 
                                                                            title="Insira o título opcional" 
                                                                            placeholder="Informar o título"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_TITULO_OPCIONAL" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Informar o título" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_TITULO_OPCIONAL']; ?>"
                                                                        >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Somente Números (Varchar 255)</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-sort-numeric-asc"></i></span>
                                                                        <input 
                                                                            id=""
                                                                            class="form-control onlynumber insert_field" 
                                                                            type="varchar" 
                                                                            name="email<?php echo $database_codigo; ?>" 
                                                                            title="Informar somente números"
                                                                            placeholder="Inserir somente números" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_NUMERO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir somente números"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_NUMERO']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Tempo</legend>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Data</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-calendar3"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control data insert_field" 
                                                                            type="varchar" 
                                                                            name="data<?php echo $database_codigo; ?>" 
                                                                            title="Informar a data" 
                                                                            placeholder="Inserir a data" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_DATA" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="data"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir a data" 
                                                                            required="true"
                                                                            value="<?php echo implode("/",array_reverse(explode("-", $database_consulta['DEF_DATA']))); ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Datas - Calendário</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-calendar3"></i></span>
                                                                        <input 
                                                                            id="data_calendario<?php echo $database_codigo; ?>" 
                                                                            class="form-control data daterange-insaite insert_field" 
                                                                            type="varchar" 
                                                                            name="data_calendario<?php echo $database_codigo; ?>" 
                                                                            title="Informar a data"
                                                                            placeholder="Inserir data" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_CALENDARIO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="calendario"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir a data" 
                                                                            required="true"
                                                                            value="<?php echo implode("/",array_reverse(explode("-", $database_consulta['DEF_CALENDARIO']))); ?>" 
                                                                            >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                    <?php $calendario_data_value = implode("/",array_reverse(explode("-", $database_consulta['DEF_CALENDARIO']))); ?>
                                                                    <script type="text/javascript">
                                                                        // Basic Initialization
                                                                        $('.daterange-insaite').daterangepicker({
                                                                            singleDatePicker: true,
                                                                            // autoUpdateInput: true,
                                                                            // emptyInitUpdate: false,
                                                                            applyClass: 'bg-slate-600',
                                                                            cancelClass: 'btn-default',
                                                                            "locale": {
                                                                                "format": "DD/MM/YYYY",
                                                                                "separator": " - ",
                                                                                "applyLabel": "Aplicar",
                                                                                "cancelLabel": "Cancelar",
                                                                                "fromLabel": "De",
                                                                                "toLabel": "Para",
                                                                                "customRangeLabel": "Customizar",
                                                                                "daysOfWeek": [
                                                                                    "Dom",
                                                                                    "Seg",
                                                                                    "Ter",
                                                                                    "Qua",
                                                                                    "Qui",
                                                                                    "Sex",
                                                                                    "Sab"
                                                                                ],
                                                                                "monthNames": [
                                                                                    "Janeiro",
                                                                                    "Fevereiro",
                                                                                    "Março",
                                                                                    "Abril",
                                                                                    "Maio",
                                                                                    "Junho",
                                                                                    "Julho",
                                                                                    "Agosto",
                                                                                    "Setembro",
                                                                                    "Outubro",
                                                                                    "Novembro",
                                                                                    "Dezembro"
                                                                                ]
                                                                            }
                                                                        });

                                                                        // Resetar Data de Calendario (Empty);
                                                                        $(".daterange-insaite-cadastro").val(null);
                                                                        $("#data_calendario<?php echo $primary_database_page_collumn_id; ?>").data('daterangepicker').setStartDate(<?php echo $calendario_data_value; ?>);
                                                                    </script>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Hora</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-database-time2"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control hora insert_field" 
                                                                            type="varchar" 
                                                                            name="hora<?php echo $database_codigo; ?>" 
                                                                            title="Informar a hora"
                                                                            placeholder="Inserir hora" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_HORA" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir a hora"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_HORA']; ?>" 
                                                                            >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Hora com Segundos</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-database-time2"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control hora_segundos insert_field" 
                                                                            type="varchar" 
                                                                            name="hora_segundos<?php echo $database_codigo; ?>" 
                                                                            title="Informar a hora"
                                                                            placeholder="Inserir hora" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_SEGUNDO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir a hora"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_SEGUNDO']; ?>" 
                                                                            >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Textos</legend>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Descrição (Text)</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-file-text2"></i></span>
                                                                        <textarea 
                                                                            id=""
                                                                            class="form-control insert_field" 
                                                                            type=""
                                                                            name="descricao_text<?php echo $database_codigo; ?>" 
                                                                            title="Informar a descrição"
                                                                            placeholder="Inserir a descrição"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_DESCRICAO_TEXT" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="text" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir a descrição"
                                                                            required="true"
                                                                        ><?php echo $database_consulta['DEF_DESCRICAO_TEXT']; ?></textarea>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Descrição (Long Text)</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <textarea 
                                                                            id="summernote_update<?php echo $database_codigo; ?>" 
                                                                            class="summernote_update insert_field" 
                                                                            type="" 
                                                                            name="descricao<?php echo $database_codigo; ?>" 
                                                                            title="Informar a descrição"
                                                                            placeholder="Inserir a descrição"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_DESCRICAO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="longtext" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir a descrição"
                                                                            required="true"
                                                                        ><?php echo $database_consulta['DEF_DESCRICAO']; ?></textarea>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset title="2">
                                <legend class="text-semibold">Campos Únicos</legend>
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Identificações</legend>
                            
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>CPF</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-vcard"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control cpf insert_field" 
                                                                            type="cpf" 
                                                                            name="cpf<?php echo $database_codigo; ?>" 
                                                                            title="Informar o CPF" 
                                                                            placeholder="Inserir o CPF"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_CPF" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="cpf" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o CPF"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_CPF']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>CNPJ</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-vcard"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control cnpj insert_field" 
                                                                            type="cnpj" 
                                                                            name="cnpj<?php echo $database_codigo; ?>" 
                                                                            title="Informar o CNPJ" 
                                                                            placeholder="Inserir o CNPJ"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_CNPJ" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="cnpj" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o CNPJ"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_CNPJ']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Comunicação</legend>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Telefone</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-phone2"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control fone insert_field" 
                                                                            type="varchar" 
                                                                            name="telefone<?php echo $database_codigo; ?>" 
                                                                            title="Informar o telefone"
                                                                            placeholder="Inserir o telefone" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_TELEFONE" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o telefone"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_TELEFONE']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>E-mail</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-mail5"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            name="email<?php echo $database_codigo; ?>" 
                                                                            type="varchar" 
                                                                            title="Informar o e-mail"
                                                                            placeholder="Inserir o e-mail" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_EMAIL" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o e-mail"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_EMAIL']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Monetização</legend>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Valor Monetário R$</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Item"><strong style="color: #000000;">R$</strong></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control money insert_field" 
                                                                            type="varchar" 
                                                                            name="money<?php echo $database_codigo; ?>" 
                                                                            title="Informar o valor monetário" 
                                                                            placeholder="Inserir o valor monetário"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_VALOR" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="valor"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o valor monetário"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_VALOR']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Valor Monetário R$ (Decimal 4)</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Item"><strong style="color: #000000;">R$</strong></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control money4 insert_field" 
                                                                            type="varchar" 
                                                                            name="money4<?php echo $database_codigo; ?>" 
                                                                            title="Informar o valor monetário" 
                                                                            placeholder="Inserir o valor monetário"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_VALOR4" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="valor"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o valor monetário"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_VALOR4']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Empenhos</legend>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Empenho de Licitação</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-briefcase3"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control empenho insert_field" 
                                                                            type="varchar" 
                                                                            name="empenho<?php echo $database_codigo; ?>" 
                                                                            title="Informar o número de empenho" 
                                                                            placeholder="Inserir o número de empenho"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_EMPENHO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus"
                                                                            data-placeholder="Inserir o número de empenho" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_EMPENHO']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Quantidade</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-footprint"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control qtd_text insert_field" 
                                                                            type="quantidade" 
                                                                            name="quantidade<?php echo $database_codigo; ?>" 
                                                                            title="Informar a quantidade" 
                                                                            placeholder="Inserir a quantidade"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_QUANTIDADE" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir a quantidade"
                                                                            required="true" 
                                                                            value="<?php echo $database_consulta['DEF_QUANTIDADE']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Localização</legend>
                            

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>CEP</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-mailbox"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control cep insert_field" 
                                                                            type="varchar" 
                                                                            name="cep<?php echo $database_codigo; ?>" 
                                                                            title="Informar o CEP"
                                                                            placeholder="Inserir o CEP" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_CEP" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o CEP"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_CEP']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Cidade</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-city"></i></span>
                                                                        <select 
                                                                            id="" 
                                                                            class="select insert_field" 
                                                                            type="" 
                                                                            name="cidade<?php echo $database_codigo; ?>" 
                                                                            title="Informar a cidade"
                                                                            placeholder="Inserir a cidade" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_CIDADE" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir a cidade" 
                                                                            required="true"
                                                                        >
                                                                            <optgroup label="Padrão">
                                                                                <option value="" selected="true">Selecione...</option> 
                                                                            </optgroup>
                                                                            <?php 
                                                                                $sql_ufs = newsql("SELECT DISTINCT CID_UF FROM config_cidades ORDER BY CID_UF ASC");
                                                                                foreach ($sql_ufs as $key => $value) {
                                                                                    $ufs_uf = $value['CID_UF'];
                                                                            ?>
                                                                            <optgroup label="<?php echo $ufs_uf; ?>">
                                                                                <?php 
                                                                                    $sql_cidades = newsql("SELECT * FROM config_cidades WHERE CID_UF = '{$ufs_uf}' ORDER BY CID_NOME ASC");
                                                                                    foreach ($sql_cidades as $key => $value) {
                                                                                        $cidade_codigo     = $value['CID_CODIGO'];
                                                                                        $cidade_nome       = $value['CID_NOME'];
                                                                                        $cidade_uf         = $value['CID_UF'];
                                                                                ?>
                                                                                <option <?php if($database_consulta['DEF_CIDADE'] == $cidade_codigo) echo 'selected'; ?> value="<?php echo $cidade_codigo; ?>"><?php echo $cidade_nome . " - " . $cidade_uf; ?></option> 
                                                                                <?php } ?>
                                                                            </optgroup>
                                                                            <?php } ?>
                                                                        </select>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Widget Google Maps (Iframe)</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-map5"></i></span>
                                                                        <textarea 
                                                                            id=""
                                                                            class="form-control insert_field" 
                                                                            type="text"
                                                                            name="widgetgooglemaps<?php echo $database_codigo; ?>" 
                                                                            title="Informar o widget google maps"
                                                                            placeholder="Inserir o widget google maps"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_WIDGET_GOOGLE_MAPS" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="text" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o google maps"
                                                                            data-popup="tooltip"
                                                                            required="true"
                                                                        >
                                                                            <?php echo $database_consulta['DEF_WIDGET_GOOGLE_MAPS']; ?>
                                                                        </textarea>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    

                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            
                             <fieldset title="3">
                                <legend class="text-semibold">Redes Sociais</legend>
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Widgets</legend>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Widget Youtube (Iframe)</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-youtube"></i></span>
                                                                        <textarea 
                                                                            id=""
                                                                            class="form-control insert_field" 
                                                                            type="text"
                                                                            name="widgetyoutube<?php echo $database_codigo; ?>" 
                                                                            title="Informar o widget youtube"
                                                                            placeholder="Inserir o widget youtube"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_WIDGET_YOUTUBE" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="text" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o widget youtube"
                                                                            data-popup="tooltip"
                                                                            required="true"
                                                                        ><?php echo $database_consulta['DEF_WIDGET_YOUTUBE']; ?></textarea>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Comunicação</legend>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>WhatsApp</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="fa fa-whatsapp" style="font-size: 20px;"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control fone insert_field" 
                                                                            type="varchar" 
                                                                            name="whatsapp<?php echo $database_codigo; ?>" 
                                                                            title="Informar o WhatsApp"
                                                                            placeholder="Inserir o WhatsApp" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_WHATSAPP" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o WhatsApp" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_WHATSAPP']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Skype</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-skype"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="skype<?php echo $database_codigo; ?>" 
                                                                            title="Informar o skype"
                                                                            placeholder="Inserir o skype" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_SKYPE" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o skype" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_SKYPE']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Midias</legend>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Instagram</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-instagram"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="instagram<?php echo $database_codigo ?>" 
                                                                            title="Informar o instagram"
                                                                            placeholder="Inserir o instagram" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_INSTAGRAM" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o instagram" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_INSTAGRAM']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Facebook</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-facebook"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="facebook<?php echo $database_codigo; ?>" 
                                                                            title="Informar o facebook"
                                                                            placeholder="Inserir o facebook" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_FACEBOOK" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o instagram" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_FACEBOOK']; ?>"
                                                                            >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Twitter</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-twitter"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="twitter<?php echo $database_codigo; ?>"
                                                                            title="Informar o Twitter"
                                                                            placeholder="Inserir o twitter" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_TWITTER" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o twitter" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_TWITTER']; ?>"
                                                                            >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Pinterest</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-pinterest2"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="pinterest<?php echo $database_codigo; ?>" 
                                                                            title="Informar o pinterest"
                                                                            placeholder="Inserir o pinterest" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_PINTREST" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o pinterest" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_PINTREST']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Reddit</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-reddit"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="reddit<?php echo $database_codigo; ?>" 
                                                                            title="Informar o reddit"
                                                                            placeholder="Inserir o reddit" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_REDDIT" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o reddit" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_REDDIT']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Streamings</legend>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Youtube</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-youtube"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="youtube<?php echo $database_codigo; ?>" 
                                                                            title="Informar o youtube"
                                                                            placeholder="Inserir o youtube" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_YOUTUBE" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o youtube" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_YOUTUBE']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Vimeo</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-vimeo"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="vimeo<?php echo $database_codigo; ?>" 
                                                                            title="Informar o vimeo"
                                                                            placeholder="Inserir o vimeo" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_VIMEO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o vimeo" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_VIMEO']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>SoundCloud</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-soundcloud"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="soundcloud<?php echo $database_codigo; ?>" 
                                                                            title="Informar o soundcloud"
                                                                            placeholder="Inserir o soundcloud" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_SOUNDCLOUD" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o soundcloud" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_SOUNDCLOUD']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Armazenamento</legend>
                                                    
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Dropbox</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-dropbox"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="dropbox<?php echo $database_codigo; ?>" 
                                                                            title="Informar o dropbox"
                                                                            placeholder="Inserir o dropbox" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_DROPBOX" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o dropbox" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_DROPBOX']; ?>"
                                                                            >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>One Drive</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-onedrive"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="onde_drive<?php echo $database_codigo; ?>" 
                                                                            title="Informar o one drive"
                                                                            placeholder="Inserir o one drive" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_ONEDRIVE" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o one drive" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_ONEDRIVE']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Google Drive</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-google-drive"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="google_drive<?php echo $database_codigo; ?>" 
                                                                            title="Informar o google drive"
                                                                            placeholder="Inserir o google drive" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_GOOGLEDRIVE" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o google drive" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_GOOGLEDRIVE']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Github</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-github"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="github<?php echo $database_codigo; ?>" 
                                                                            title="Informar o github"
                                                                            placeholder="Inserir o github" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_GITHUB" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o github" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_GITHUB']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Website/Links</legend>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Wordpress</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-wordpress"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="wordpress<?php echo $database_codigo; ?>" 
                                                                            title="Informar o wordpress"
                                                                            placeholder="Inserir o wordpress" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_WORDPRESS" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o wordpress" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_WORDPRESS']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    


                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Google Plus</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-google-plus"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="google_plus<?php echo $database_codigo; ?>" 
                                                                            placeholder="Informar o google glus" 
                                                                            title="Inserir o google plus"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_GOOGLEPLUS" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o google plus" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_GOOGLEPLUS']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Hiperlink</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-anchor"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="hiperlink<?php echo $database_codigo; ?>" 
                                                                            title="Informar o hiperlink"
                                                                            placeholder="Inserir o hiperlink" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_HIPERLINK" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o hiperlink" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_HIPERLINK']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Pagamentos</legend>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>PayPal</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-paypal2"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="paypal<?php echo $database_codigo; ?>" 
                                                                            title="Informar o paypal"
                                                                            placeholder="Inserir o paypal" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_PAYPAL" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o paypal" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_PAYPAL']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>PagSeguro</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-credit-card2"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="pagseguro<?php echo $database_codigo; ?>" 
                                                                            title="Informar o pagseguro"
                                                                            placeholder="Inserir o pagseguro" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_PAGSEGURO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o pagseguro" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_PAGSEGURO']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Mercado Pago</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-credit-card"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="mercadopago<?php echo $database_codigo; ?>" 
                                                                            title="Informar o mercado pago"
                                                                            placeholder="Inserir o mercado pago" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_MERCADOPAGO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o mercado pago" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_MERCADOPAGO']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Game/Conta</legend>
                                                    

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Steam</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-steam"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="steam<?php echo $database_codigo; ?>" 
                                                                            title="Informar o steam"
                                                                            placeholder="Inserir o steam" 
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_STEAM" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o steam" 
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_STEAM']; ?>">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    
                                                    


                                                    
                                                    
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>


                            <fieldset title="4">
                                <legend class="text-semibold">Campos Personalizados</legend>
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Blog/Notícias</legend>
                                                    
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Categoria</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-database4"></i></span>
                                                                        <select 
                                                                            id="categoria<?php echo $database_codigo; ?>"
                                                                            class="select insert_field" 
                                                                            type="" 
                                                                            name="categoria<?php echo $database_codigo; ?>"
                                                                            title="Indique a categoria"
                                                                            placeholder="Selecione a categoria"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_COR" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-popup="tooltip"
                                                                            data-placeholder="Selecione a categoria" 
                                                                            required="true"
                                                                            value=""
                                                                        >
                                                                            <option data-popup="tooltip" selected title="Selecionar" value="">Selecione...</option> 
                                                                            <?php 
                                                                                $sql_cores = newsql("SELECT * FROM config_cores WHERE COR_VISIBILIDADE = 'true' ORDER BY COR_TITULO ASC");
                                                                                foreach ($sql_cores as $key => $value) {
                                                                                    $categoria_codigo = $value['COR_CODIGO'];
                                                                                    $categoria_titulo = $value['COR_TITULO'];
                                                                            ?>
                                                                            <option <?php if($categoria_codigo == $database_consulta['DEF_COR']) echo 'selected'; ?> data-popup="tooltip" title="<?php echo $categoria_titulo; ?>" value="<?php echo $categoria_codigo; ?>"><?php echo $categoria_titulo; ?></option> 
                                                                            <?php } ?>
                                                                        </select>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Slug (Endereço da Página)</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><?php echo SITE . 'publicacao/'; ?></span>
                                                                        <input 
                                                                            id="slug<?php echo $database_codigo; ?>"
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="slug<?php echo $database_codigo; ?>"
                                                                            title="Informar o slug"
                                                                            placeholder="Inserir o slug"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_SLUG" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="varchar" 
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o slug" 
                                                                            data-popup="tooltip"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_SLUG']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>    
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Imóveis</legend>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Metragem</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-rulers"></i></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control money insert_field" 
                                                                            type="varchar" 
                                                                            name="metragem<?php echo $database_codigo; ?>" 
                                                                            title="Informar a metragem" 
                                                                            placeholder="Inserir a metragem"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_METRAGEM" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="valor"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir a metragem"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_METRAGEM']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Indicação de Metragem"><strong style="color: #000000;">m²</strong></span>
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-lg-2"><strong>Registro Único</strong></label>
                                                                <div class="col-lg-10">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Registro Único"><strong style="color: #000000; font-size: 16px; padding: 0 3px;">#</strong></span>
                                                                        <input 
                                                                            id="" 
                                                                            class="form-control insert_field" 
                                                                            type="varchar" 
                                                                            name="registro<?php echo $database_codigo; ?>" 
                                                                            title="Informar o registro" 
                                                                            placeholder="Inserir o registro"
                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                            data-field="<?php echo $primary_database_page_collumn_prefix; ?>_REGISTRO" 
                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                            data-type="valor"
                                                                            data-trigger="focus" 
                                                                            data-placeholder="Inserir o registro"
                                                                            required="true"
                                                                            value="<?php echo $database_consulta['DEF_REGISTRO']; ?>"
                                                                        >
                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            
                            <fieldset title="5">
                                <legend class="text-semibold">Anexos</legend>
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Galeria de Anexos</legend>
                                                    <div class="alert alert-info alert-styled-left">
                                                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                                        <span class="text-semibold">Aviso!</span> <br>
                                                        <strong>Uploads:</strong> Por favor, não insira documentos, imagens ou qualquer outro tipo de upload com o mesmo nome dos arquivos já anexados!
                                                    </div> 
                                                    <div class="panel panel-flat">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <fieldset>
                                                                        <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Upload de Anexos</legend>
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="<?php echo $primary_database_page_upload; ?>-uploads"></div>
                                                                                <div class="file-uploader"><p>Atualize seu browser para ter acesso ao upload de arquivos! </p></div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="row">
                                                                                    <table class='table table-bordered table-lg'>
                                                                                        <thead>
                                                                                            <tr class='active'>
                                                                                                <th colspan='3'>Anexados</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody class="arquivos-box">

                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            
                                                                            </div>
                                                                        </div>
                                                                    </fieldset>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset title="5">
                                <legend class="text-semibold">Itens</legend>
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Configuração de Itens</legend>
                                                    <div class="panel panel-flat">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <fieldset>
                                                                        <legend class="text-semibold"><i class="fa fa-unlock-alt position-left"></i> Itens</legend>
                                                                    
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label>Título do Item:</label>
                                                                                    <div class="input-group">
                                                                                        <input 
                                                                                            id="titulo_item<?php echo $database_codigo; ?>" 
                                                                                            class="form-control" 
                                                                                            type="varchar" 
                                                                                            name="titulo_item<?php echo $database_codigo; ?>" 
                                                                                            title="Informar o título do item" 
                                                                                            placeholder="Inserir o título do item"
                                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>"  
                                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                                            data-type="varchar"
                                                                                            data-trigger="focus" 
                                                                                            data-placeholder="Inserir o título do item"
                                                                                            required="true"
                                                                                            value=""
                                                                                        >
                                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label>Cor do Item:</label>
                                                                                    <div class="input-group">
                                                                                        <select 
                                                                                            id="cor_item<?php echo $database_codigo; ?>"
                                                                                            class="select" 
                                                                                            type="" 
                                                                                            name="cor_item<?php echo $database_codigo; ?>"
                                                                                            title="Indique a cor do item"
                                                                                            placeholder="Selecione a cor do item"
                                                                                            data-table="<?php echo $primary_database_page_tabela; ?>" 
                                                                                            data-field-primary="<?php echo $primary_database_page_collumn_id; ?>" 
                                                                                            data-id="<?php echo $database_codigo; ?>" 
                                                                                            data-type="varchar" 
                                                                                            data-trigger="focus" 
                                                                                            data-popup="tooltip"
                                                                                            data-placeholder="Selecione a cor do item" 
                                                                                            required="true"
                                                                                            value=""
                                                                                        >
                                                                                            <option data-popup="tooltip" selected title="Selecionar" value="">Selecione...</option> 
                                                                                            <?php 
                                                                                                $sql_cores = newsql("SELECT * FROM config_cores WHERE COR_VISIBILIDADE = 'true' ORDER BY COR_TITULO ASC");
                                                                                                foreach ($sql_cores as $key => $value) {
                                                                                                    $categoria_codigo = $value['COR_CODIGO'];
                                                                                                    $categoria_titulo = $value['COR_TITULO'];
                                                                                            ?>
                                                                                            <option data-popup="tooltip" title="" value="<?php echo $categoria_codigo; ?>"><?php echo $categoria_titulo; ?></option> 
                                                                                            <?php } ?>
                                                                                        </select>
                                                                                        <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                        <a id="add_item" style="width: 100%; text-align: center;" href="javascript:void(0);" class="btn btn-info">Adicionar Item <i class="icon-hour-glass2 position-right"></i></a>
                                                                        
                                                                    </fieldset>
                                                                    <legend style="margin-top: 15px;" class="text-semibold"><i class="icon-hour-glass2 position-left"></i> Itens Adicionados</legend>
                                                                    <div calss="col-lg-12">
                                                                        <table class="table table-bordered table-hover " id="table_itens">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Título</th>
                                                                                    <th>Cor</th>
                                                                                    <th class="text-center">Ações</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody id="itens">
                                                                                <?php 
                                                                                    $sql_itens = newsql("SELECT * FROM tbl_defaults_itens as item INNER JOIN config_cores as cor ON cor.COR_CODIGO = item.ITEM_COR WHERE ITEM_REGISTRO = '{$database_codigo}'");
                                                                                    foreach ($sql_itens as $key => $value) {
                                                                                        $item_codigo        = $value['ITEM_CODIGO'];
                                                                                        $item_titulo        = $value['ITEM_TITULO'];
                                                                                        $item_cor           = $value['ITEM_COR'];
                                                                                        $item_titulo_cor    = $value['COR_TITULO'];
                                                                                ?>
                                                                                <tr class='line<?php echo $item_codigo; ?>'>
                                                                                    <td><?php echo $item_codigo; ?></td>
                                                                                    <td><?php echo $item_titulo; ?></td>
                                                                                    <td><?php echo $item_titulo_cor; ?></td>
                                                                                    <td class='text-center'>
                                                                                        <a onclick="confirmDelItem(<?php echo $item_codigo; ?>)" class='remove_button' style='color:red;'><i class='icon-folder-remove'></i></a>
                                                                                    </td>
                                                                                </tr>

                                                                                <?php } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            
                            <button type="button" class="btn btn-default stepy-finish">Atualização Automática <i class="fa fa-save position-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <script type="text/javascript">
            var ADMIN                   = "<?php echo ADMIN; ?>";
            var database_page_codigo    = "<?php echo $database_codigo; ?>";
            var database_page_upload    = "<?php echo $primary_database_page_upload; ?>";
            var database_page_usuario   = "<?php echo $cadastro_usuario; ?>";
            var database_page_tabela    = "<?php echo $primary_database_page_tabela; ?>";
        </script>
        <?php /* Incluir Upload Unico */ require_once("includes/upload-unico.php"); ?>
        <?php /* Incluir Uploads de Anexos Gerais */ require_once("includes/upload.php"); ?>
        <?php /* Incluir Campo Text Personalizado */ require_once("includes/field-text.php"); ?>
        <?php /* Incluir Notificação Padrão de Anexos Gerais */ require_once("includes/notificacao-padrao.php"); ?>
        <?php /* Incluir Credits */ require_once("includes/copyright.php"); ?>

    </div>
</div>

<script type="text/javascript">
$('#add_item').click(function () {
    var item_codigo = '<?php echo $database_codigo; ?>';
    var item_titulo = $('#titulo_item' + item_codigo).val();
    var item_cor    = $('#cor_item' +item_codigo).val();
    $.ajax({
        type: 'POST',
        url: 'ajax/ajax.searchDefaultsitens.php',
        dataType: 'JSON',
        data: {'item_codigo':item_codigo, 'item_titulo':item_titulo, 'item_cor':item_cor },
        beforeSend: function() {},
        success: function (data) {
            var resposta            = data.resposta;
            var search_titulo       = data.search_titulo;
            var search_cor          = search_cor;
            var search_cor_titulo   = data.search_cor_titulo;
            var last_id             = data.last_id;

            if(resposta == 'true'){
                var line = "<tr class='line"+last_id+"'>"+
                    "<td>"+last_id+"</td>"+
                    "<td>"+search_titulo+"</td>"+
                    "<td>"+search_cor_titulo+"</td>"+
                    "<td class='text-center'>"+
                        "<a onclick='confirmDelItem("+last_id+")' class='remove_button' style='color:red;'><i class='icon-folder-remove'></i></a>"+
                    "</td>"+
                "</tr>";
                $('#titulo_item' +item_codigo).val("");
                $("#itens").append(line);

                new PNotify({
                    text: "Adicionado com sucesso.",
                    addclass: 'bg-success',
                    type: 'warning',
                    icon: 'icon-database',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    opacity: .9,
                    width: "250px"
                });

                setTimeout(function(){ PNotify.removeAll(); }, 3000);

            } else {
                new PNotify({
                    text: "Erro ao procurar informações, tente novamente...",
                    addclass: 'bg-warning',
                    type: 'warning',
                    icon: 'icon-checkmark3',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    opacity: .9,
                    width: "250px"
                });
                setTimeout(function(){ PNotify.removeAll(); }, 3000);
            }
        }
    });
});


function confirmDelItem(codigo){

    $.ajax({
        type: 'POST',
        url: 'ajax/ajax.removerDefaultsitens.php',
        dataType: 'JSON',
        data: {'item':codigo },
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento...",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        }, 
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta         = data.resposta;
            
            if(resposta == 'true'){
                new PNotify({
                    text: "Removido com sucesso.",
                    addclass: 'bg-success',
                    type: 'warning',
                    icon: 'icon-database',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    opacity: .9,
                    width: "250px"
                });
                $(".line"+codigo).remove();
                setTimeout(function(){ PNotify.removeAll(); }, 3000);

            } else {
                new PNotify({
                    text: "Erro ao remover item, tente novamente mais tarde...",
                    addclass: 'bg-warning',
                    type: 'warning',
                    icon: 'icon-checkmark3',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    opacity: .9,
                    width: "250px"
                });
                setTimeout(function(){ PNotify.removeAll(); }, 3000);
            }
        }
    });
}

// Clean Data Form
$('#FormDefault')[0].reset();
$('#clean_form').on('click', function () { $('#FormDefault')[0].reset(); });

$(".stepy-default").stepy({
    next: function(index) { 
        if(index == 5){
            getUploads('<?php echo $primary_database_page_upload; ?>', '<?php echo $database_codigo; ?>', '<?php echo $primary_database_page_upload; ?>-uploads', '.arquivos-box');
        }
    },
    back: function(index) { 
        if(index == 5){
            getUploads('<?php echo $primary_database_page_upload; ?>', '<?php echo $database_codigo; ?>', '<?php echo $primary_database_page_upload; ?>-uploads', '.arquivos-box');
        }
    },
    finish: function() {
        return false;
    }
});



</script>
<?php 
    /* Registrar Informações Individualmente */ require_once("includes/form-individual.php");
    /* Incluir Footer */ require_once("includes/footer.php");
    } }
?>