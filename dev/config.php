<?php
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
session_start();
//Definição das constantes
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));
define('ROOTSIS', dirname(dirname(__FILE__)));
define('LIB_JS', ROOT . DS . 'js' . DS);
define('LIB_CSS', ROOT . DS . 'css' . DS);
define('LIB_IMG',  ROOT . DS . 'images' . DS);
define('LIB_SQL',  ROOT . DS . 'sql' . DS);
define('HOST', 'https://' . $_SERVER['HTTP_HOST']); 

require_once('funcoes/funcoes.php');
require_once('funcoes/phpmailer/class.phpmailer.php');
require_once('acesso/verificaAcesso.php');
require_once('classes/Conexao.class.php');
date_default_timezone_set('America/Sao_Paulo');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$acesso = new VerificaAcesso();
$acesso->conecta_bd();
$acesso->verificaLogin();
$check_logado = $acesso->getCheck();

$sql_insaite = newsql("SELECT * FROM config_insaite WHERE INS_CODIGO = 0")[0];

/*** Login ***/
/** Váriaveis Importantes **/
$login_titulo_projeto 	= $sql_insaite['INS_TITULO'];
$login_dominio_projeto 	= $sql_insaite['INS_DOMINIO'];
$login_website_projeto 	= SITE;
$projeto_online 		= $sql_insaite['INS_DOMINIO'];
$projeto_repositorio 	= $sql_insaite['INS_REPOSITORIO'];

if($projeto_online == 'true'){ define('SITEROOT', HOST . '/');  } 
else { define('SITEROOT', HOST . '/' . $projeto_repositorio . '/');  }

// define('SITEROOT', HOST . '/'); 
define('SYSTEM', HOST . '/system/');
define('ADMIN', HOST . '/dev/');
define('SITE', HOST . '/site/');
define('IMG', HOST  . '/admin/imagens/');
define('AJAX', HOST  . '/admin/ajax/');


/** Váriaveis Segurança **/
$login_dat 				= date('Y-m-d');
$login_dat_config 		= date('d-m-Y');
$login_hr 				= date('H:i:s');
$login_host 			= gethostbyaddr($_SERVER['REMOTE_ADDR']);

/*** Projeto ***/
$inprojeto_titulo 		= $sql_insaite['INS_TITULO'];

$imagens_admin_logo = get_uploads("insaite", 0, "33");
$imagem_admin_logo = capture_uploads($imagens_admin_logo);

$imagens_admin_background = get_uploads("insaite", 0, "34");
$imagem_admin_background = capture_uploads($imagens_admin_background);  
