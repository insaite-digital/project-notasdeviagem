<?php 
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        header('Location: error.php');
    } else {
        $path_pagina = pathinfo( __FILE__ )['basename'];
        $acesso->Pagina = $path_pagina;
        $acesso->verificaPermissao();
        $resposta = $acesso->getResposta();
        if(empty($resposta) || $resposta == 'false'){ header('Location: direcionamento.php'); } else {

            $paginas = newsql("SELECT * FROM config_paginas WHERE PAG_ARQUIVO = '{$path_pagina}'")[0];
            $pagina_codigo          = $paginas['PAG_CODIGO'];
            $pagina_titulo          = $paginas['PAG_TITULO'];
            $pagina_singular        = $paginas['PAG_SINGULAR'];
            $pagina_plural          = $paginas['PAG_PLURAL'];
            $pagina_icone           = $paginas['PAG_ICONE'];
            $pagina_text_singular   = $paginas['PAG_TEXT_SINGULAR'];
            $pagina_text_plural     = $paginas['PAG_TEXT_PLURAL'];
            // Variaveis da Página de Consulta
            $pagina_tabela          = "config_perfis";
            
            $codigo = $_GET['perfil'];
            $sql_consulta = newsql("SELECT * FROM config_perfis WHERE PER_CODIGO = '{$codigo}'")[0];
            $second_title = $sql_consulta['PER_TITULO'];

?>
<?php require_once("includes/header.php"); ?>
<script type="text/javascript" src="_template/assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="_template/assets/js/plugins/forms/styling/switch.min.js"></script>
<!-- <script type="text/javascript" src="_template/assets/js/plugins/forms/styling/switchery.min.js"></script> -->
<script type="text/javascript" src="_template/assets/js/pages/form_checkboxes_radios.js"></script>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <!-- Página Título -->
                    <span class="text-semibold">
                        <a href="index.php">Dashboard</a>
                    </span> 
                    <i class="icon-arrow-right6"></i> 
                    <strong>Configuração de Usuários</strong>
                    <br>
                    <i class="icon-arrow-right6"></i> 
                    Perfil: <strong><?php echo $second_title; ?> </strong>
                    <br>
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <!-- Página Links de Ações -->
                    <div class="heading-btn-group">
                        <!-- Página Links de Ações -->
                        <a href="consultarProPerfis.php?active=properfis" class="btn btn-link btn-float has-text">
                            <i class="icon-database-arrow text-default"></i> 
                            <span>Consulta de Perfis</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <!-- Página Breadcrumbs -->
                <li>
                    <a href="index.php">
                        <i class="icon-home2 position-left"></i> 
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <i class="icon-stack3"></i> 
                    <span>Configuração de Usuários</span>
                </li>
                <li>
                    <i class="icon-arrow-right6"></i> 
                    <span>Perfi: <?php echo $second_title; ?></span>
                </li>
            </ul>
            <ul class="breadcrumb-elements">
                <!-- Modal Ajuda -->
                <?php require_once("includes/ajuda.php"); ?>
            </ul>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-lg-9">
                <h6 class="content-group text-semibold">
                    Lista de Usuários
                    <small class="display-block">Configure Permissões e Alterações de dados conforme necessitar!</small>
                    
                </h6>

                <div class="row">
                    <?php 
                        $sql_users = newsql("SELECT * FROM config_usuarios as usuario INNER JOIN config_perfis as perfil ON perfil.PER_CODIGO = usuario.USU_PERFIL WHERE usuario.USU_PERFIL = '{$codigo}'");
                        foreach ($sql_users as $key => $value) {
                            // print_($value);
                            $user_codigo     = $value['USU_CODIGO'];
                            $user_nome       = $value['USU_NOME'];
                            $user_perfil     = $value['PER_TITULO'];
                            $user_usuario    = $value['USU_USUARIO'];
                            $user_senha      = $value['USU_SENHA'];
                            $user_email      = $value['USU_EMAIL'];
                            $user_bloqueado  = $value['USU_BLOQUEADO'];
                            $hora_login      = $value['USU_HORA_LOGIN'];                             
                            $hora_logout     = $value['USU_HORA_LOGOUT'];

                            if ($value['USU_DATA_LOGIN'] === '0000-00-00' || $value['USU_DATA_LOGIN'] === '30/11/-0001'){ $data_login = 'Não identificado'; } else { $data_login = date('d/m/Y', strtotime($value['USU_DATA_LOGIN'])); }
                            if ($value['USU_DATA_LOGOUT'] === '0000-00-00' || $value['USU_DATA_LOGOUT'] === '30/11/-0001'){ $data_logout = 'Não identificado'; } else { $data_logout = date('d/m/Y', strtotime($value['USU_DATA_LOGIN'])); }
                            // echo $user_bloqueado;
                            if($user_bloqueado == 'true'){
                                $cla = "bg-danger-400";
                            } else if($user_bloqueado == 'false') {
                                $cla = "bg-success-400";
                            }
                    ?>
                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-body">
                            <div class="media">
                                <div class="media-left">
                                    <a class="btn <?php echo $cla; ?> btn-rounded btn-icon btn-xs">
                                        <span class="letter-icon"></span>
                                    </a>
                                </div>

                                <div class="media-body">
                                    <h6 class="media-heading"><?php echo $user_nome; ?></h6>
                                    <span class="text-muted"><?php echo $user_perfil; ?></span><br>
                                    <span class="text-muted"><strong>Login:</strong> <?php echo $user_usuario; ?></span><br>
                                    <span class="text-muted"><strong>E-mail:</strong> <?php echo $user_email; ?></span><br>
                                    <hr>
                                    <span class="text-muted"><strong>Login:</strong> <?php echo $data_login; ?> - <?php echo $hora_login; ?><br>
                                    <span class="text-muted"><strong>Logout:</strong> <?php echo $data_logout; ?> - <?php echo $hora_logout; ?></span><br>
                                    
                                </div>

                                <div class="media-right media-middle">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right" style="width: 300px;">
                                                <li><a data-toggle="modal" data-target="#update_nome<?php echo $user_codigo; ?>" href="#"><i class="icon-mouse-left pull-right"></i> Alterar Nome & E-mail </a></li>
                                                <li><a data-toggle="modal" data-target="#update_user<?php echo $user_codigo; ?>" href="#"><i class="icon-server pull-right"></i> Alterar Usuário</a></li>
                                                <li><a data-toggle="modal" data-target="#update_senha<?php echo $user_codigo; ?>" href="#"><i class="icon-drawer pull-right"></i> Alterar Senha</a></li>
                                                <li><a data-toggle="modal" data-target="#bloqueado_user<?php echo $user_codigo; ?>" href="#"><i class="icon-user-lock pull-right"></i> Habilitar/Desabilitar Acesso</a></li>
                                                <li><a href="alterarProPerfisUsuariosPermissoes.php?usuario=<?php echo $user_codigo; ?>"> <i class="icon-hammer-wrench pull-right"></i> Permissões Gerais </a></li>
                                                <li class="divider"></li>
                                                <?php $sql_count_logs = newsql("SELECT COUNT(*) as total FROM config_logs WHERE LOG_USUARIO = '{$codigo}'")[0]['total']; ?>
                                                <li><a href="consultarProPerfisUsuariosLogs.php?usuario=<?php echo $user_codigo; ?>"> Logs/Histórico <span class="badge bg-teal-400 pull-right"><?php echo $sql_count_logs; ?></span></a></li>
                                                <li><a data-toggle="modal" data-target="#remove_user<?php echo $user_codigo; ?>" href="#" style="background: red; color: #FFFFFF;"><i class="icon-database-remove pull-right"></i> Remover Usuário</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="bloqueado_user<?php echo $user_codigo; ?>" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">
                                        Deseja alterar estas <strong>Informações</strong>?<br>
                                    </h5>
                                </div>
                                <hr style="margin: 0 auto;">
                                <form id="FormUpdateBloqueado<?php echo $user_codigo; ?>" method="post" onsubmit="return false">
                                    <div class="modal-body">
                                        <input type="hidden" name="upbloqueado_pagina<?php echo $user_codigo; ?>" value="alterarProPerfisUsuarios.php">
                                        <input type="hidden" name="upbloqueado_tabela<?php echo $user_codigo; ?>" value="config_usuarios">
                                        <input type="hidden" name="upbloqueado_codigo<?php echo $user_codigo; ?>" value="<?php echo $user_codigo; ?>">
                                        <div class="form-group">
                                            <label>Permissão de Acesso:</label>
                                            <div class="input-group">
                                                <select data-placeholder="Selecione a Permissão" class="select" name="permissao<?php echo $user_codigo; ?>">
                                                    <option value="0" selected="true">Selecionar...</option> 
                                                    <option <?php if($user_bloqueado == 'true') echo 'selected'; ?> value="true">Bloqueado</option> 
                                                    <option <?php if($user_bloqueado == 'false') echo 'selected'; ?> value="false">Ativo</option> 
                                                </select>
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-primary bloqueado_cadastro" data-codigo="<?php echo $user_codigo; ?>">Atualizar&nbsp;&nbsp;<i class="icon-database-refresh"></i></button>
                                    </div>
                                </form>                          
                            </div>
                        </div>
                    </div>    

                    <div id="update_user<?php echo $user_codigo; ?>" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">
                                        Deseja alterar estas <strong>Informações</strong>?<br>
                                    </h5>
                                </div>
                                <hr style="margin: 0 auto;">
                                <form id="FormUpdateUser<?php echo $user_codigo; ?>" method="post" onsubmit="return false">
                                    <div class="modal-body">
                                        <input type="hidden" name="upuser_pagina<?php echo $user_codigo; ?>" value="alterarProPerfisUsuarios.php">
                                        <input type="hidden" name="upuser_tabela<?php echo $user_codigo; ?>" value="config_usuarios">
                                        <input type="hidden" name="upuser_codigo<?php echo $user_codigo; ?>" value="<?php echo $user_codigo; ?>">
                                        <div class="form-group">
                                            <label><strong>Usuário <small style="color: #777;">(Login)</small>:</strong></label>
                                            <div class="input-group">
                                                <input id="upuser_usuario<?php echo $user_codigo; ?>" name="upuser_usuario<?php echo $user_codigo; ?>" class="form-control" placeholder="Insira um usuario" value="<?php echo $user_usuario; ?>">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-primary upuser_cadastro" data-codigo="<?php echo $user_codigo; ?>">Atualizar&nbsp;&nbsp;<i class="icon-database-refresh"></i></button>
                                    </div>
                                </form>                          
                            </div>
                        </div>
                    </div>

                    <div id="update_senha<?php echo $user_codigo; ?>" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">
                                        Deseja alterar estas <strong>Informações</strong>?<br>
                                    </h5>
                                </div>
                                <hr style="margin: 0 auto;">
                                <form id="FormUpdateSenha<?php echo $user_codigo; ?>" method="post" onsubmit="return false">
                                    <div class="modal-body">
                                        <input type="hidden" name="upsenha_pagina<?php echo $user_codigo; ?>" value="alterarProPerfisUsuarios.php">
                                        <input type="hidden" name="upsenha_tabela<?php echo $user_codigo; ?>" value="config_usuarios">
                                        <input type="hidden" name="upsenha_codigo<?php echo $user_codigo; ?>" value="<?php echo $user_codigo; ?>">
                                        <div class="form-group">
                                            <label><strong>Senha <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                            <div class="input-group">
                                                <input id="upsenha_senha<?php echo $user_codigo; ?>" name="upsenha_senha<?php echo $user_codigo; ?>" class="form-control" placeholder="Insira sua Senha" value="" type="password">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="panel panel-info" style="margin-top: 20px;">
                                                <div class="panel-heading ">
                                                    <h6 class="panel-title"><i class="icon-warning position-left"></i> Requisitos - Senhas Fortes</h6>
                                                </div>
                                                
                                                <div class="panel-body">
                                                    <ul>
                                                        <li>Ter pelo menos <strong>1 Letra Minúscula</strong></li>    
                                                        <li>Ter pelo menos <strong>1 Letra Maiúscula</strong></li>    
                                                        <li>Ter pelo menos <strong>1 Número</strong></li>    
                                                        <li>Ter <strong>6 ou mais Caracteres</strong></li>    
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-primary upsenha_cadastro" data-codigo="<?php echo $user_codigo; ?>">Atualizar&nbsp;&nbsp;<i class="icon-database-refresh"></i></button>
                                    </div>
                                </form>                          
                            </div>
                        </div>
                    </div>

                    <div id="update_nome<?php echo $user_codigo; ?>" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">
                                        Deseja alterar estas <strong>Informações</strong>?<br>
                                    </h5>
                                </div>
                                <hr style="margin: 0 auto;">
                                <form id="FormUpdateNome<?php echo $user_codigo; ?>" method="post" onsubmit="return false">
                                    <div class="modal-body">
                                        <input type="hidden" name="upnome_pagina<?php echo $user_codigo; ?>" value="alterarProPerfisUsuarios.php">
                                        <input type="hidden" name="upnome_tabela<?php echo $user_codigo; ?>" value="config_usuarios">
                                        <input type="hidden" name="upnome_codigo<?php echo $user_codigo; ?>" value="<?php echo $user_codigo; ?>">
                                        <div class="form-group">
                                            <label><strong>Nome Completo <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                            <div class="input-group">
                                                <input id="upnome_nome<?php echo $user_codigo; ?>" name="upnome_nome<?php echo $user_codigo; ?>" class="form-control" placeholder="Insira o nome completo" value="<?php echo $user_nome; ?>">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><strong>E-mail :</strong></label>
                                            <div class="input-group">
                                                <input id="upnome_email<?php echo $user_codigo; ?>" name="upnome_email<?php echo $user_codigo; ?>" class="form-control" placeholder="Insira um e-mail para validação" value="<?php echo $user_email; ?>">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-primary upnome_cadastro" data-codigo="<?php echo $user_codigo; ?>">Atualizar&nbsp;&nbsp;<i class="icon-database-refresh"></i></button>
                                    </div>
                                </form>                          
                            </div>
                        </div>
                    </div>

                    <div id="remove_user<?php echo $user_codigo; ?>" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">
                                        Porque deseja remover este <strong>Registro</strong>?<br>
                                        Código: <strong class="text_remove"><?php echo $user_codigo; ?></strong>
                                    </h5>
                                </div>
                                <br><hr style="margin: 0 auto;">
                                <form id="FormRemoverUser<?php echo $user_codigo; ?>" method="post" onsubmit="return false">
                                    <div class="modal-body">
                                        <input type="hidden" name="removeuser_pagina<?php echo $user_codigo; ?>" value="#consultarProPerfisUsuarios.php">
                                        <input type="hidden" name="removeuser_tabela<?php echo $user_codigo; ?>" value="config_usuarios">
                                        <input type="hidden" name="removeuser_codigo<?php echo $user_codigo; ?>" value="<?php echo $user_codigo; ?>">
                                        <input type="hidden" value="USU_CODIGO" name="remove_coluna<?php echo $user_codigo; ?>" id="remove_coluna">
                                        <div class="form-group">
                                            <label><strong>Motivo <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                            <div class="input-group">
                                                <textarea id="remove_motivo<?php echo $user_codigo; ?>" name="remove_motivo<?php echo $user_codigo; ?>" rows="5" cols="5" class="form-control" placeholder="Insira um motivo" style="resize: none;"></textarea>
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-danger remover_user" data-codigo="<?php echo $user_codigo; ?>">Remover&nbsp;&nbsp;<i class="icon-database-remove"></i></button>
                                    </div>
                                </form>
                                
                            </div>
                        </div>
                    </div>


                    <?php } ?>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="thumbnail">
                    <div class="caption text-center">
                        <h6 class="text-semibold no-margin"><strong>Perfil</strong><br><?php echo $second_title; ?> <small class="display-block">Selecionado</small></h6>
                    </div>
                </div>
                
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"><strong>Navegação Importante</strong></h6>
                    </div>

                    <div class="list-group no-border no-padding-top">
                        <a href="#" data-toggle="modal" data-target="#insert_user" class="list-group-item"><i class="icon-database-add"></i> Criar Novo Usuário </a>
                    </div>

                    <div id="insert_user" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">
                                        Deseja cadastrar nova <strong>Permissão</strong>?<br>
                                    </h5>
                                </div>
                                <hr style="margin: 0 auto;">
                                <form id="FormInsert" method="post" onsubmit="return false">
                                    <div class="modal-body">
                                        <input type="hidden" name="pagina" value="<?php echo $path_pagina; ?>">
                                        <input type="hidden" name="tabela" value="<?php echo $pagina_tabela; ?>">
                                        <input type="hidden" name="codigo" value="<?php echo $codigo; ?>">
                                        <input type="hidden" value="<?php echo $codigo; ?>" name="in_perfil" id="in_perfil">
                                        <div class="form-group">
                                            <label><strong>Nome Completo <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                            <div class="input-group">
                                                <input id="in_nome" name="in_nome" class="form-control" placeholder="Insira o nome completo" value="">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><strong>E-mail :</strong></label>
                                            <div class="input-group">
                                                <input id="in_email" name="in_email" class="form-control" placeholder="Insira um e-mail para validação" value="">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label><strong>Usuário <small style="color: #777;">(Login)</small>:</strong></label>
                                            <div class="input-group">
                                                <input id="in_usuario" name="in_usuario" class="form-control" placeholder="Insira um título" value="">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="panel panel-info" style="margin-top: 20px;">
                                                <div class="panel-heading ">
                                                    <h6 class="panel-title"><i class="icon-warning position-left"></i> Requisitos - Senhas Fortes</h6>
                                                </div>
                                                
                                                <div class="panel-body">
                                                    <ul>
                                                        <li>Ter pelo menos <strong>1 Letra Minúscula</strong></li>    
                                                        <li>Ter pelo menos <strong>1 Letra Maiúscula</strong></li>    
                                                        <li>Ter pelo menos <strong>1 Número</strong></li>    
                                                        <li>Ter <strong>6 ou mais Caracteres</strong></li>    
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Senha <small style="color: #777;">(250 caracteres)</small>:</strong></label>
                                            <div class="input-group">
                                                <span id="see_pass" class="input-group-addon" data-popup="tooltip" title="Clique para visualizar/esconder a senha"><strong style="color: #2196F3;"><i class="icon-eye"></i></strong></span>
                                                <input id="in_senha" name="in_senha" class="form-control" placeholder="Insira sua Senha" value="" type="password">
                                                <span class="input-group-addon" data-popup="tooltip" title="Obrigatório"><strong style="color: red;">*</strong></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-primary insert_user">Cadastrar&nbsp;&nbsp;<i class="icon-database-add"></i></button>
                                    </div>
                                </form>                          
                            </div>
                        </div>
                    </div>        
                </div>
            </div>
        </div>
        <!-- Modal Upload -->
        <?php require_once("includes/copyright.php"); ?>
    </div>
</div>

<script type="text/javascript">
$('.remover_user').on('click', function () {
    var code = $(this).attr("data-codigo");
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.removerProUsuariosUser.php?code='+code,
        data        : $("#FormRemoverUser" + code).serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })
});

$('.bloqueado_cadastro').on('click', function () {
    var code = $(this).attr("data-codigo");
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.updateProUsuariosBloqueado.php?code='+code,
        data        : $("#FormUpdateBloqueado" + code).serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })
});

$('.upsenha_cadastro').on('click', function () {
    var code = $(this).attr("data-codigo");
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.updateProUsuariosSenha.php?code='+code,
        data        : $("#FormUpdateSenha" + code).serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })
});

$('.upuser_cadastro').on('click', function () {
    var code = $(this).attr("data-codigo");
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.updateProUsuariosUser.php?code='+code,
        data        : $("#FormUpdateUser" + code).serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })
});

$('.upnome_cadastro').on('click', function () {
    var code = $(this).attr("data-codigo");
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.updateProUsuariosNome.php?code='+code,
        data        : $("#FormUpdateNome" + code).serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })
});

$('#see_pass').on('click', function(){     
    var passwordField = $('#in_senha');
    var passwordFieldType = passwordField.attr('type');
    if(passwordFieldType == 'password') {
        passwordField.attr('type', 'text');
        $(this).val('Hide');
    } else {
        passwordField.attr('type', 'password');
        $(this).val('Show');
    }
});


$('.insert_user').on('click', function () {
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.insertProPerfisUsuarios.php',
        data        : $("#FormInsert").serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Cadastrado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                location.reload();
            } else if(resposta == 'senha'){
                new PNotify({   
                    text: "Senha Fraca...",
                    addclass: 'bg-warning',
                    type: 'warning',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })


});


$('.update_permissao').on('click', function () {
    var codigo = $(this).attr("data-codigo");
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.updateProModulosPermissoes.php?code=' + codigo,
        data        : $("#FormEdit"+codigo).serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })


});

$('.update_pagina').on('click', function () {
    var codigo = $(this).attr("data-codigo");
    $.ajax({
        type        : 'POST', 
        url         : 'ajax/proadmin/ajax.updateProModulosPaginas.php?code=' + codigo,
        data        : $("#FormUpdatePagina"+codigo).serialize(),
        dataType    : 'json', 
        beforeSend: function() {
            $.blockUI({ 
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            new PNotify({
                text: "Aguarde um momento",
                addclass: 'bg-primary',
                type: 'info',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                },
                opacity: .9,
                width: "250px"
            });
        },
        success: function (data) {
            $.unblockUI();
            PNotify.removeAll();
            var resposta = data.resposta;
            if(resposta == 'true'){
                new PNotify({   
                    text: "Atualizado com sucesso...",
                    addclass: 'bg-success',
                    type: 'success',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
                // setTimeout(function(){ 
                    location.reload();
                // }, 2000);
            } else {
                new PNotify({   
                    text: "Erro ao atualizar...",
                    addclass: 'bg-danger',
                    type: 'danger',
                    icon: 'icon-checkmark3',
                    hide: true,
                    buttons: {
                        closer: true,
                        sticker: false
                    },
                    opacity: 1,
                    width: "200px"
                });
            }
            
        }
    })


});
</script>
<?php 
    require_once("includes/footer.php");
    } }
?>