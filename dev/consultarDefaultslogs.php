<?php 
    require_once("config.php");
    if(empty($check_logado) || $check_logado == 'false'){
        header('Location: error.php');
    } else {
        $path_pagina = pathinfo( __FILE__ )['basename'];
        $acesso->Pagina = $path_pagina;
        $acesso->verificaPermissao();
        $resposta = $acesso->getResposta();
        if(empty($resposta) || $resposta == 'false'){ header('Location: direcionamento.php'); } else {

            $paginas = newsql("SELECT * FROM config_paginas WHERE PAG_ARQUIVO = '{$path_pagina}'")[0];
            $pagina_codigo          = $paginas['PAG_CODIGO'];
            $pagina_titulo          = $paginas['PAG_TITULO'];
            $pagina_singular        = $paginas['PAG_SINGULAR'];
            $pagina_plural          = $paginas['PAG_PLURAL'];
            $pagina_icone           = $paginas['PAG_ICONE'];
            $pagina_text_singular   = $paginas['PAG_TEXT_SINGULAR'];
            $pagina_text_plural     = $paginas['PAG_TEXT_PLURAL'];

            $primary_database_page_tabela           = "tbl_defaults";
            $primary_database_page_collumn_prefix   = "DEF";
            $primary_database_page_collumn_id       = "DEF_CODIGO";
            
            $codigo = $_GET['default'];
            $sql_consulta = newsql("SELECT * FROM ".$primary_database_page_tabela." WHERE ".$primary_database_page_collumn_id." = '{$codigo}'")[0];
?>
<?php require_once("includes/header.php"); ?>

<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <!-- Página Título -->
                    <span class="text-semibold">
                        <a href="index.php">Dashboard</a>
                    </span> 
                    <i class="icon-arrow-right6"></i> 
                    <strong>Logs de Alterações</strong>
                    <br>
                    <i class="icon-arrow-right6"></i> 
                    <strong><?php echo $sql_consulta['DEF_TITULO']; ?> </strong>
                    <br>
                </h4>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <!-- Página Links de Ações -->
                    <div class="heading-btn-group">
                        <!-- Página Links de Ações -->
                        
                        <a href="javascript: history.go(-1)" class="btn btn-link btn-float has-text">
                            <i class="icon-database-arrow text-default"></i> 
                            <span>Voltar Página Anterior</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <!-- Página Breadcrumbs -->
                <li>
                    <a href="index.php">
                        <i class="icon-home2 position-left"></i> 
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <i class="icon-stack3"></i> 
                    <span>Logs de Alterações</span>
                </li>
                <li>
                    <i class="icon-arrow-right6"></i> 
                    <span><?php echo $sql_consulta['DEF_TITULO']; ?></span>
                </li>
            </ul>
            <ul class="breadcrumb-elements">
                <!-- Modal Ajuda -->
                <?php require_once("includes/ajuda.php"); ?>
            </ul>
        </div>
    </div>
    <div class="content">
        <!-- Detached content -->
        <div class="container-detached">
            <div class="content-detached">

                <!-- Version 1.5 -->
                <div class="panel panel-flat" id="v_1_5">
                    <div class="panel-heading">
                        <h5 class="panel-title"><strong>Logs de Alterações:  <?php echo $sql_consulta['DEF_TITULO']; ?></strong></h5>
                        <div class="heading-elements">
                            <?php 
                                $data_extenso = details_data(date('Y-m-d'));
                                $data_extenso = data_extensa($data_extenso);
                            ?>
                            <span class="text-muted heading-text"><?php echo $data_extenso; ?></span>
                            <span class="label bg-blue heading-text">Atualizado</span>
                        </div>
                    </div>

                    <div class="panel-body">
                        <!-- <p class="content-group">Version 1.5 is a second part of version 1.4, which includes 22 new pages from 7 categories, various improvements and bug fixes. Please check documentation for a full list of changes and files that need to be updated.</p> -->

                        <?php 
                        ?>

                        <pre class="language-php"><code>// # Lista de Logs // ------------------------------
                        <?php 

                            $sql_logs = newsql("SELECT * FROM ".$primary_database_page_tabela."_logs as log INNER JOIN config_usuarios as usuario ON usuario.USU_CODIGO = log.LOG_CADASTRO_USUARIO WHERE log.LOG_TABLE = '{$primary_database_page_tabela}' AND log.LOG_ID = '{$codigo}'");
                            foreach ($sql_logs as $key => $value) {
                                // print_($value);
                                $log_codigo = $value['LOG_CODIGO'];
                                $log_permissao = $value['LOG_PERMISSAO'];
                                $log_table = $value['LOG_TABLE'];
                                $log_id = $value['LOG_ID'];
                                $log_field = $value['LOG_FIELD'];
                                $log_value = $value['LOG_VALUE'];
                                $log_cadastro_time = $value['LOG_CADASTRO_TIME'];
                                $log_cadastro_usuario = $value['LOG_CADASTRO_USUARIO'];
                                $log_ip = $value['LOG_IP'];
                                $log_host = $value['LOG_HOST'];
                                $usuario_nome = $value['USU_NOME'];
                                $data_extenso_log = date("d/m/Y H:i", strtotime($log_cadastro_time));;
echo '
[#'.$log_codigo.'] ' . $data_extenso_log . '
[Usuário] ' . $usuario_nome .  '
[Campo] ' . $log_field . '
[Valor] ' . $log_value . '
';
                            }
                        ?>
                        
                        </code></pre>
                    </div>
                </div>
            </div>
        </div>
                    
        <!-- Modal Upload -->
        <?php require_once("includes/copyright.php"); ?>
    </div>
</div>

<?php 
    require_once("includes/footer.php");
    } }
?>