<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../classes/Conexao.class.php');
require_once('../funcoes/funcoes.php');
require_once('../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];

$database_table    	= mysqli_real_escape_string($conexao, $_POST['table']);
$database_primary   = mysqli_real_escape_string($conexao, $_POST['primary']);
$database_field    	= mysqli_real_escape_string($conexao, $_POST['field']);
$database_id    	= mysqli_real_escape_string($conexao, $_POST['id']);
$database_type    	= mysqli_real_escape_string($conexao, $_POST['type']);

if($database_type == 'varchar'){ 			$database_value    	= mysqli_real_escape_string($conexao, $_POST['value']); } 
else if($database_type == 'text'){ 			$database_value    	= htmlspecialchars($_POST['value']); }
else if($database_type == 'longtext'){ 		$database_value 	= htmlspecialchars($_POST['value']); }
else if($database_type == 'data'){ 			$database_value    	= mysqli_real_escape_string($conexao, implode("-",array_reverse(explode("/", $_POST['value'])))); }
else if($database_type == 'calendario'){ 	$database_value    	= mysqli_real_escape_string($conexao, implode("-",array_reverse(explode("/", $_POST['value'])))); }
else if($database_type == 'cnpj'){  		$database_value    	= limpaCPFCNPJ(mysqli_real_escape_string($conexao, $_POST['value'])); }
else if($database_type == 'cpf'){  			$database_value    	= limpaCPFCNPJ(mysqli_real_escape_string($conexao, $_POST['value'])); }
else if($database_type == 'valor'){ 		$database_value    	= mysqli_real_escape_string($conexao, $_POST['value']); $database_value = str_replace("." , "" , $database_value ); $database_value = str_replace(',','.', $database_value); }
else if($database_type == 'quantidade'){  	$database_value    	= limparTexto(mysqli_real_escape_string($conexao, $_POST['value'])); }
else { $database_value    	= mysqli_real_escape_string($conexao, $_POST['value']); }

if(!empty($database_table) && !empty($database_primary) && !empty($database_field) && !empty($database_id) && !empty($database_value)){
	if(newupdate($database_table, "".$database_field." = '{$database_value}' WHERE ".$database_primary." = '{$database_id}'")){
		
		/* #1 Consulta de Informações - #2 Cadastro de Informações - #3 Alteração de Informações - #4 Exclusão de Informações */
		insert_logs_individual($database_table . '_logs', '2', $database_table, $database_id, $database_field, $database_value);
		
		$resp = array('resposta' => 'true');
	} else { $resp = array('resposta' => 'false'); }
} else { $resp = array('resposta' => 'dados'); }
echo json_encode($resp);