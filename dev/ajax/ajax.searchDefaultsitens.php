      <?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../classes/Conexao.class.php');
require_once('../funcoes/funcoes.php');
require_once('../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];

$search_codigo    	= mysqli_real_escape_string($conexao, $_POST['item_codigo']);
$search_titulo    	= mysqli_real_escape_string($conexao, $_POST['item_titulo']);
$search_cor   		= mysqli_real_escape_string($conexao, $_POST['item_cor']);

$sql_consulta = newsql("SELECT * FROM config_cores WHERE COR_CODIGO = '{$search_cor}'")[0];
$consulta_titulo = $sql_consulta['COR_TITULO'];

if(!empty($search_codigo) && !empty($search_titulo) && !empty($search_cor)){
	if(!empty($consulta_titulo)){
		if(newinsert("tbl_defaults_itens","(ITEM_CHECK, ITEM_CADASTRO_USUARIO, ITEM_CADASTRO_TIME, ITEM_VISIBILIDADE, ITEM_REGISTRO, ITEM_COR, ITEM_TITULO) VALUES ('true', '{$cadastro_usuario}', '{$cadastro_time}', 'true', '{$search_codigo}', '{$search_cor}', '{$search_titulo}')")){
			$last_id = mysqli_insert_id($conexao);
			$resp = array('resposta' => 'true', 'search_titulo' => $search_titulo, 'search_cor' => $search_cor, 'search_cor_titulo' => $consulta_titulo, 'last_id' => $last_id);
		} else { $resp = array('resposta' => 'false'); }
	} else { $resp = array('resposta' => 'dados'); }
} else { $resp = array('resposta' => 'dados'); }

echo json_encode($resp);
