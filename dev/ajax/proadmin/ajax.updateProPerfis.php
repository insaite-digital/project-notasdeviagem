<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../../classes/Conexao.class.php');
require_once('../../funcoes/funcoes.php');
require_once('../../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];

$path_pagina    	= "alterarProPerfis.php";
$tabela         	= mysqli_real_escape_string($conexao, $_POST['tabela']);
$code 		        = $_GET['code'];
$codigo		    	= mysqli_real_escape_string($conexao, $_POST['up_codigo' . $code]);
$titulo         	= mysqli_real_escape_string($conexao, $_POST['up_titulo' . $code]);
$descricao          = htmlspecialchars($_POST['up_descricao' . $code]);

if(!empty($codigo) && !empty($titulo)){
	if(newupdate("config_perfis", "PER_TITULO = '{$titulo}', PER_DESCRICAO = '{$descricao}' WHERE PER_CODIGO = '{$codigo}'")){
		// Logs
        insert_logs($path_pagina);
		
		$resp = array('resposta' => 'true');
	
	} else { $resp = array('resposta' => 'false'); }
} else { $resp = array('resposta' => 'dados'); }
echo json_encode($resp);
