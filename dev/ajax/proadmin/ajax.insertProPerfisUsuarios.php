<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../../classes/Conexao.class.php');
require_once('../../funcoes/funcoes.php');
require_once('../../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];


$path_pagina    	= "cadastrarProPerfisUsuarios.php";
$tabela         	= "config_usuarios";
$codigo       		= newsql("SELECT AUTO_INCREMENT FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME   = '".$tabela."'")[0]['AUTO_INCREMENT'];
$perfil     		= mysqli_real_escape_string($conexao, $_POST['in_perfil']);
$nome     			= mysqli_real_escape_string($conexao, $_POST['in_nome']);
$email     			= mysqli_real_escape_string($conexao, $_POST['in_email']);
$usuario			= mysqli_real_escape_string($conexao, $_POST['in_usuario']);
$senha				= mysqli_real_escape_string($conexao, $_POST['in_senha']);

$c = '94';
$salt = 'DiGiTalInsaiTe';
$hash = md5($salt.$senha.$c);

$forca = senhaValida($senha);

if($forca == 1){
	if(!empty($nome) && !empty($email) && !empty($usuario) && !empty($senha)){
		$consulta = newsql("SELECT * FROM config_usuarios WHERE USU_USUARIO = '{$usuario}'");
		if(empty($consulta)){
			if(newinsert("config_usuarios","(USU_NOME, USU_EMAIL, USU_USUARIO, USU_SENHA, USU_PERFIL, USU_BLOQUEADO) VALUES ('{$nome}', '{$email}', '{$usuario}', '{$hash}', '{$perfil}', 'false')")){

				// Logs
		        insert_logs($path_pagina);
				$resp = array('resposta' => 'true');
			} else { $resp = array('resposta' => 'false'); }

		} else { $resp = array('resposta' => 'false'); }
	} else { $resp = array('resposta' => 'dados'); }
} else { $resp = array('resposta' => 'senha'); }
echo json_encode($resp);
