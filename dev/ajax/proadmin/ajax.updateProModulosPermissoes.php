<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../../classes/Conexao.class.php');
require_once('../../funcoes/funcoes.php');
require_once('../../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];

// $path_pagina    = mysqli_real_escape_string($conexao, $_POST['pagina']);
// $tabela         = mysqli_real_escape_string($conexao, $_POST['tabela']);
$code				= mysqli_real_escape_string($conexao, $_GET['code']);
$edit_codigo		= mysqli_real_escape_string($conexao, $_POST['edit_codigo' . $code]);
$edit_permissao     = mysqli_real_escape_string($conexao, $_POST['edit_permissao' . $code]);
$edit_modulo     	= mysqli_real_escape_string($conexao, $_POST['edit_modulo' . $code]);
$permissao_titulo   = mysqli_real_escape_string($conexao, $_POST['permissao_titulo' . $code]);
$permissao         	= mysqli_real_escape_string($conexao, $_POST['permissao' . $code]);

if(!empty($code) && !empty($permissao_titulo) && !empty($permissao)){
	if(newupdate("config_modulos_permissoes", "MODPERM_TITULO = '{$permissao_titulo}', MODPERM_PERMISSAO = '{$permissao}' WHERE MODPERM_CODIGO = '{$code}'")){
		// Logs
        // insert_logs($path_pagina);
		
		$resp = array('resposta' => 'true');
	
	} else { $resp = array('resposta' => 'false'); }
} else { $resp = array('resposta' => 'dados'); }
echo json_encode($resp);
