<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
error_reporting(0);
require_once('../../classes/Conexao.class.php');
require_once('../../funcoes/funcoes.php');
require_once('../../funcoes/phpmailer/class.phpmailer.php');
$conexao = new Conexao;
$conexao = $conexao->Conecta_bd();
$cadastro_time 		= date("Y-m-d H:i:s");
$cadastro_usuario 	= $_SESSION['USUARIO_CODIGO'];

$titulo     		= mysqli_real_escape_string($conexao, $_POST['in_titulo']);
$descricao			= mysqli_real_escape_string($conexao, $_POST['in_descricao']);

if(!empty($titulo)){
	if(newinsert("config_modulos","(MOD_TITULO, MOD_DESCRICAO) VALUES ('{$titulo}', '{$descricao}')")){
		
		$format = Remove_caracter($titulo);
		$format_unico = $format . '_unico';
		mkdir('../../../uploads/'. $format, 0777, true);
		mkdir('../../../uploads/' . $format_unico, 0777, true);
		if(newinsert("config_uploads_projetos","(PROJ_TITULO) VALUES ('{$format}')")){
			$id_pass = mysqli_insert_id($conexao);
			if(newinsert("config_uploads_projetos_tipos","(TIPO_PROJETO, TIPO_TITULO, TIPO_CADASTRO_TIME, TIPO_CADASTRO_USUARIO, TIPO_CHECK, TIPO_VISIBILIDADE) VALUES ('{$id_pass}', 'Padrão', '{$cadastro_time}', '{$cadastro_usuario}', 'true', 'true')")){
				newinsert("config_uploads_projetos","(PROJ_TITULO, PROJ_CADASTRO_TIME, PROJ_CADASTRO_USUARIO, PROJ_CHECK, PROJ_VISIBILIDADE) VALUES ('{$format_unico}', '{$cadastro_time}', '{$cadastro_usuario}', 'true', 'true')");
				// Logs
		        // insert_logs($path_pagina);
				$resp = array('resposta' => 'true');
			} else { $resp = array('resposta' => 'error_up_projetos'); }
		} else { $resp = array('resposta' => 'error'); }
	} else { $resp = array('resposta' => 'false'); }
} else { $resp = array('resposta' => 'dados'); }
echo json_encode($resp);
